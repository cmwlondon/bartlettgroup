var loginOpen = false,
	shroud,
	overlay,
	shroudOpacity = '0.5',
	duration = 500;

function setCookie(cookieName, cookieValue) {
	document.cookie = encodeURIComponent(cookieName) + '=' + encodeURIComponent(cookieValue) + '; expires=Fri, 31 Dec 9999 23:59:59 GMT' + '; path=/';
}
function getCookie(cookieName) {
	return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(cookieName).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
}
function userSignon() {
	// only checks for empty strings for username and password
	if (document.portalSignon.Username.value > '' && document.portalSignon.Password.value > '') {
		/*
		if (document.portalSignon.RememberMe.checked == true) {
			setCookie('Username', document.portalSignon.Username.value);
		} else {
			setCookie('Username', '');
		}
		*/
		jQuery('.bcnOverlay .form').removeClass('error');
		document.portalSignon.submit();
	} else {
		// add error messages to form

		jQuery('.bcnOverlay .form').addClass('error');
		alert('Please enter a user name and password');
		document.portalSignon.Username.focus();
	}
}
function altLink(linkName) {
	// if (linkName == 'signup') sURL = 'https://portal.csr24.com/mvc/Portal/SignMeUp';
	// https://portal.csr24.com/mvc/Account/ForgotPassword/
	//if (linkName == 'forgot') sURL = 'https://services.csr24.com/Portal/Reset/' + document.portalSignon.AgencyKey.value + '/Password?language=EN&urlReferrer=' + encodeURIComponent(parent.location.href);
	if (linkName == 'forgot') sURL = 'https://portal.csr24.com/mvc/Account/ForgotPassword/' + document.portalSignon.AgencyKey.value;
	document.portalSignon.action = sURL;
	document.portalSignon.submit();
}
function documentReady() {
	var savedUserID = getCookie('Username');
	if (savedUserID > '') {
		document.portalSignon.Username.value = savedUserID;
		document.portalSignon.RememberMe.checked = true;
	}
}

jQuery( document ).ready( function() {
	documentReady();

	shroud = jQuery('.bcnShroud');
	overlay = jQuery('.bcnOverlay');

	jQuery('nav.secnav a.login').on('click',function(e){
		e.preventDefault();

		shroud
		.css({"display" : "block"})
		.animate({"opacity" : shroudOpacity}, duration, function(){
		});

		overlay
		.css({"display" : "block"})
		.animate({"opacity" : "1"}, duration, function(){
			loginOpen = true;
		});
	});

	jQuery('.bcnOverlay a.closer').on('click',function(e){
		e.preventDefault();

		overlay
		.animate({"opacity" : "0"}, duration, function(){
			overlay
			.css({"display" : "none"});
		});
		shroud
		.animate({"opacity" : "0"}, duration, function(){
			shroud
			.css({"display" : "none"});
			loginOpen = false;
		});
	});

});

