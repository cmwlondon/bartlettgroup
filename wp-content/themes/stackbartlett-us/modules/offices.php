<?php
/*
office list module
/custom_posts/offices/
custom post type: 'office'
functions.
php -> get_offices();
*/
$syncedLocations = synchronise_offices(explode(',', $locations), $postid);
$assigned = (count($syncedLocations['assigned']) > 0 ) ? get_offices_in_order($syncedLocations['assigned']) : [];
$theme_root = get_template_directory_uri();
?>
		<article class="module offices"  id="contentAnchor">
			<div class="singleColumn intro">
				<h2>Where to find us</h2>
			</div>			
			<section class="singleColumn officeList">

				<?php
				foreach($assigned AS $key => $office) :
					$office_id = $office->ID;
					$metadata = $metadata = get_post_meta($office_id);
					$office_title = $office->post_title;
				    $map_image = $metadata['o_map'][0];
				    $address = $metadata['o_address'][0];
				    $email = $metadata['o_email'][0];
				    $phone = $metadata['o_phone'][0];
				    $latitude = $metadata['o_lat'][0];
				    $longitude = $metadata['o_long'][0];

				    $stateClass = ( $key == 2 ) ? ' open' : '';
				?>
				<article class="officeItem clearfix<?php echo $stateClass; ?>">

					<header class="singleColumn"><h3><?php echo $office_title; ?></h3><div class="state"><img alt="" src="<?php echo $theme_root; ?>/img/office_opener.png"></div></header>
					<div class="window">
						<div class="info doubleColumn clearfix">
							<div class="mapWrapper column">
								<div class="map" id="map<?php echo $office_id; ?>" data-latitude="<?php echo $latitude; ?>" data-longitude="<?php echo $longitude; ?>"><img alt="" src="<?php echo $map_image; ?>"></div>
							</div>
							<div class="text column">
								<h4>Address</h4>
								<address><?php echo nl2br(html_entity_decode($address)); ?></address>
								<p class="mobile"><?php echo $phone; ?></p>
								<p class="email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
							</div>
						</div>
					</div>
				</article>
				<?php
					endforeach;
				?>
			</section>
		</article>
