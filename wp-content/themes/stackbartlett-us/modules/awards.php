<?php 
$theme_root = get_template_directory_uri();
?>
		<article class="module awards">
			<div class="singleColumn">
				<h2>Our Global Reach</h2>
			</div>

			<div class="singleColumn">
				<img alt="" src="<?php echo $theme_root; ?>/img/bartlett-global-map.png" style="width:100%;">
			</div>
		</article>
