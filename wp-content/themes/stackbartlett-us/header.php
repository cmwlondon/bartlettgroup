<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
global $current_blog;
global $theme_root;
global $defaultFooter;

$theme_root = get_template_directory_uri();
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="<?php echo esc_url( get_template_directory_uri() ); ?>/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo esc_url( get_template_directory_uri() ); ?>/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo esc_url( get_template_directory_uri() ); ?>/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site waitForFontsToLoad">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyfifteen' ); ?></a>

	<div class="pageHeader">
		<header>
			<h1>PAGE</h1>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="bartlett"><img alt="bartlett" src="<?php echo $theme_root; ?>/img/logo01.png" class="logo"></a>
			
			<div class="menuWrapper clearfix">
				<a href="" id="hamburgermenu"><span>toggle menu</span></a>
				<div class="menuMover">
					<a href="/" title="bartlett"><img alt="bartlett" src="<?php echo $theme_root; ?>/img/logo01.png" class="mobilelogo"></a>
					<a href="" class="closeMenu">CLOSE</a>
					<?php if ( has_nav_menu( 'primary' ) ) : ?>
						<nav id="site-navigation" class="main-navigation" role="navigation">
							<?php
								// Primary navigation menu.
								wp_nav_menu( array(
									'menu_class'     => 'nav-menu',
									'theme_location' => 'primary',
								) );
							?>
						</nav><!-- .main-navigation -->
					<?php endif; ?>
					<nav class="footerLinks">
						<?php
							// footer links
							wp_nav_menu( array(
								'menu_class'     => 'nav-menu',
								'theme_location' => 'footer',
							) );
						?>
					</nav>

					<nav class="secnav"><a href="https://bartlett.clientportal.acturis.com/AWE/Container.aspx" class="login">client login</a><a href="<?php echo network_home_url(); ?>" class="uk<?php if ($current_blog->blog_id == 1) {echo ' active';} ?>">UK</a><a href="/us/" class="us<?php if ($current_blog->blog_id == 3) {echo ' active';} ?>">US</a><a href="http://www.linkedin.com/company/bartlett-group-ltd?trk=hb_tab_compy_id_537965" target="_blank" class="linkedin inlinedpiSelect"  data-normal="<?php echo $theme_root; ?>/img/icon_linkedin_white.png" data-retina="<?php echo $theme_root; ?>/img/icon_linkedin_whitex2.png"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="52" height="52" viewBox="0 0 52 52" class="linkedinwhite"><filter id="filter" x="9" y="9" width="34" height="34" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#fff"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="_" data-name="" class="li1" d="M14.252,22.1h5.11V37.457h-5.11V22.1ZM18.9,19.261a2.911,2.911,0,0,1-2.1.763H16.774a2.812,2.812,0,0,1-2.046-.763,2.531,2.531,0,0,1-.785-1.892,2.488,2.488,0,0,1,.807-1.9,2.945,2.945,0,0,1,2.091-.752,2.828,2.828,0,0,1,2.057.752,2.615,2.615,0,0,1,.8,1.9A2.514,2.514,0,0,1,18.9,19.261Zm13.738,9.988q0-3.473-2.566-3.473a2.5,2.5,0,0,0-1.637.542,3.417,3.417,0,0,0-1,1.316,3.655,3.655,0,0,0-.155,1.239v8.584h-5.11q0.066-13.893,0-15.353h5.11v2.234H27.238a5.143,5.143,0,0,1,4.624-2.588,5.538,5.538,0,0,1,4.27,1.748,7.349,7.349,0,0,1,1.615,5.155v8.8h-5.11V29.249ZM41.12,10.876a6.138,6.138,0,0,0-4.5-1.869H15.381a6.382,6.382,0,0,0-6.371,6.371V36.616a6.382,6.382,0,0,0,6.371,6.371H36.618a6.383,6.383,0,0,0,6.371-6.371V15.378A6.138,6.138,0,0,0,41.12,10.876Z"/></svg></a></nav>
				</div>
			</div>
		</header>


