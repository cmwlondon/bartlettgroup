<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
global $current_blog;
global $defaultFooter;

$theme_root = get_template_directory_uri();
?>

	<?php // if ( $defaultFooter ) :  get_template_part( 'modules/advisor' ); endif ?>

<!-- closure point for div#content in all templates -->
	</div>
<!--  -->

	<div class="bcnShroud"></div>
	<div class="bcnOverlay">
		<div class="form">
			<form name="portalSignon" id="portalSignon" method="post" action="https://portal.csr24.com/mvc">
				<input type="hidden" name="AgencyKey" value="959635840" />
				<h2>Bartlett Client Extranet</h2>
				<a href="" class="closer"><img alt="close" src="<?php echo get_stylesheet_directory_uri(); ?>/img/bluecross.png"></a>
				<hr>
				<div class="field"><label for="username">Email address</label><input type="text" name="Username" id="username" size="99" placeholder="Email address" /></div>
				<div class="field"><label for="password">Password</label><input type="password" name="Password" id="password" size="18" placeholder="Password" /></div>
				<a href="javascript:altLink('forgot');" class="misc">Forgot your password?</a>
				<span class="nb2"><input type="button" name="signon" value="LOG ON" onclick="userSignon();" /></span>
			</form>
		</div>
	</div>

	<footer id="colophon" class="site-footer">
		<div class="singleColumn">
			<div class="row firstrow clearfix">
				<a href="<?php echo $current_blog->path; ?>" class="logo" title="bartlett group"><img alt="" src="<?php echo get_template_directory_uri(); ?>/img/footer_logo.png"></a>
				<nav class="secnav"><a href="https://bartlett.clientportal.acturis.com/AWE/Container.aspx" class="login">client login</a><a href="/" class="uk<?php if ($current_blog->blog_id == 1) {echo ' active';} ?>">UK</a><a href="/us/" class="us<?php if ($current_blog->blog_id == 3) {echo ' active';} ?>">US</a><a href="http://www.linkedin.com/company/bartlett-group-ltd?trk=hb_tab_compy_id_537965" target="_blank" class="linkedin inlinedpiSelect"  data-normal="<?php echo $theme_root; ?>/img/icon_linkedin_white.png" data-retina="<?php echo $theme_root; ?>/img/icon_linkedin_whitex2.png"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="52" height="52" viewBox="0 0 52 52" class="linkedinwhite"><filter id="filter" x="9" y="9" width="34" height="34" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#fff"/><feComposite result="composite" operator="in" in2="SourceGraphic"/><feBlend result="blend" in2="SourceGraphic"/></filter></defs><path id="_" data-name="" class="li1" d="M14.252,22.1h5.11V37.457h-5.11V22.1ZM18.9,19.261a2.911,2.911,0,0,1-2.1.763H16.774a2.812,2.812,0,0,1-2.046-.763,2.531,2.531,0,0,1-.785-1.892,2.488,2.488,0,0,1,.807-1.9,2.945,2.945,0,0,1,2.091-.752,2.828,2.828,0,0,1,2.057.752,2.615,2.615,0,0,1,.8,1.9A2.514,2.514,0,0,1,18.9,19.261Zm13.738,9.988q0-3.473-2.566-3.473a2.5,2.5,0,0,0-1.637.542,3.417,3.417,0,0,0-1,1.316,3.655,3.655,0,0,0-.155,1.239v8.584h-5.11q0.066-13.893,0-15.353h5.11v2.234H27.238a5.143,5.143,0,0,1,4.624-2.588,5.538,5.538,0,0,1,4.27,1.748,7.349,7.349,0,0,1,1.615,5.155v8.8h-5.11V29.249ZM41.12,10.876a6.138,6.138,0,0,0-4.5-1.869H15.381a6.382,6.382,0,0,0-6.371,6.371V36.616a6.382,6.382,0,0,0,6.371,6.371H36.618a6.383,6.383,0,0,0,6.371-6.371V15.378A6.138,6.138,0,0,0,41.12,10.876Z"/></svg></a></nav>
			</div>
			<div class="row clearfix">
				<div class="address">
					<h3>Bartlett &amp; Company Inc</h3>
					<address>1601 Market Street,<br>
					Suite 2560,<br>
					Philadelphia,<br>
					PA 19103</address>
					<p>Tel: +1 215 546 9660<br>
					Email: <a href="mailto:usmail@bartlettgroup.com">usmail@bartlettgroup.com</a></p>
				</div>
				<nav class="footerLinks">
					<?php
						// footer links
						wp_nav_menu( array(
							'menu_class'     => 'nav-menu',
							'theme_location' => 'footer',
						) );
					?>
				</nav>
				<div class="declaration">
					<p class="a1">Bartlett Group (Holdings) Limited incorporated and registered in England and Wales. Registered address: Broadway Hall, Horsforth, Leeds. United Kingdom. LS18 4RS. Registered in England & Wales under company registration number 00945137  Bartlett Group (Holdings) Limited is a holding company, some of whose subsidiaries are authorised and regulated by the Financial Conduct Authority.</p>
					<p class="copyright">&copy; Bartlett Group 2017</p>
				</div>
			</div>
		</div>
	</footer>

</div>
    

<?php
wp_enqueue_script( 'bartlett_ajax_request', get_template_directory_uri() . '/js/ajax.js', array( 'jquery' ) );
wp_localize_script( 'bartlett_ajax_request', 'bartlett_ajax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ), 'current_blog' => $current_blog->blog_id ) );
wp_footer();
?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBvYQD3pGGYKrinw2QAXODQZ919slgNBY&amp;callback=initMap"></script>
</body>
</html>
