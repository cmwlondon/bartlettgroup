<?php
function my_theme_enqueue_styles() {

    $parent_style = 'stackbartlett-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    wp_enqueue_script( 'stackbartlett-extranet', get_stylesheet_directory_uri() . '/js/extranet.js', array( 'jquery' ), '20150330', true );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
?>