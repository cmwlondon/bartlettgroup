<?php
/*
product detail (business) related products module
used by: single-product.php
*/
// $related_products = get_related_products($taglist,$postid);
$metadata = get_post_meta($postid);
$related_product_ids = ($metadata['rld_order'][0] != '') ? explode(',',$metadata['rld_order'][0]) : [];
$synced_products = synchronise_related_products( $related_product_ids,  $postid );
$related_products = (count($synced_products['assigned']) > 0) ? get_products_by_order($synced_products['assigned']) : [];

if ( count($related_products) > 0 ) :
    ?>
        <article class="module similarProducts greybg">

            <h3><?php if ($metadata['rld_heading'][0] != '') : echo $metadata['rld_heading'][0]; else : ?>Related products<?php endif; ?></h3>
            <div class="wrapper clearfix">
                <?php
                    foreach ($related_products AS $related_product) :
                    $relatedProductMeta = get_post_meta($related_product->ID);

                    $relatedProductInserts = array(
                        'title' => $related_product->post_title,
                        'image' => $relatedProductMeta['rld_image'][0],
                        'link' => get_permalink($related_product->ID)
                    );
                ?>
                <div class="item">
                    <div class="clipper">
                        <img alt="<?php echo $relatedProductInserts['title']; ?>" src="<?php echo $relatedProductInserts['image']; ?>">
                        <a href="<?php echo $relatedProductInserts['link']; ?>"><span><?php echo $relatedProductInserts['title']; ?></span></a>
                    </div>
                </div>

                <?php 
                    endforeach;
                ?>
            </div>
        </article>
<?php endif; ?>
