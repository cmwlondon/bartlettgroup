<?php 
/*
product detail (business) case studies module
used by: single-product.php
*/
$metadata = get_post_meta($postid);
$case_study_ids = ($metadata['pr_case_study_order'][0] != '') ? explode(',',$metadata['pr_case_study_order'][0]) : [];
$case_studies = get_case_studies_in_order($case_study_ids);

if (count($case_study_ids) > 0) :
?>
		<article class="module caseStudy <?php echo $colourScheme; ?>">
			<?php
				$side = 'picLeft';
				foreach($case_study_ids AS $case_study_id) :
				$case_study_meta = get_post_meta($case_study_id);
			?>
			<div class="singleColumn"><h3><?php echo $case_study_meta['cs_header1'][0]; ?></h3></div>
			<div class="doubleColumn clearfix <?php echo $side; ?>">
				<div class="column pic">
					<div class="clipper"><img alt="" src="<?php echo $case_study_meta['cs_image'][0]; ?>">
				</div>
				</div><div class="column text">
					
					<h4><?php echo $case_study_meta['cs_header2'][0]; ?></h4>
					<div class="copy">
						<p><?php echo $case_study_meta['cs_copy'][0]; ?></p>
					</div>
					<p><?php echo $case_study_meta['cs_source'][0]; ?></p>
				</div>
			</div>
			<?php
				$side = ($side == 'picLeft') ? 'picRight' : 'picLeft';
				endforeach;
			?>
		</article>
<?php 
endif;
?>
