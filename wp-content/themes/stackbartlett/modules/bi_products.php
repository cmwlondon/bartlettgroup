<?php
/*
business insutrance product list page: bp1_
*/
$thisCategory = $products[$productselect];
$theme_root = get_template_directory_uri();
?>
		<article class="generalContent whitebg">
			<div class="singleColumn secondary">
				<h3><?php echo $thisCategory['heading']; ?></h3>
				<p><?php echo $thisCategory['copy']; ?></p>
			</div>
		</article>

		<?php
		$background = 'greybg';
		$arrangement = 'rightImage';
		foreach ( $thisCategory['idlist'] AS $productID ) :
			// find page with meta data 'bp2_product' == $productID
			$parentPage = get_product_parent_page($productID)[0];
			$pageLink = get_permalink($parentPage->ID);
			$productMeta = get_post_meta($productID);

			$thumbnailID = get_post_thumbnail_id($productID);
			$imageData = wp_get_attachment_image_src( $thumbnailID, 'full');
			$productImage = $imageData[0];

		?>
		<article class="generalContent pair <?php echo $background.' '.$arrangement; ?>">
			<div class="doubleColumnWrapper">
				<div class="doubleColumnItem rightColumn imageBox">
					<img alt="" src="<?php echo $productImage; ?>">
				</div>
				<div class="doubleColumnItem leftColumn textBox">
					<h3><?php echo $parentPage->post_title; ?></h3>
					<div><?php echo $productMeta['bp_extra_copy'][0]; ?></div>
					<div class="buttonBox"><a href="<?php echo $pageLink; ?>" class="lozengeButtonEx"><span>VIEW <?php echo $parentPage->post_title; ?></span></a></div>
				</div>
				<br class="cb">
			</div>
		</article>
		<?php
			$background = ($background == 'greybg') ? 'whitebg' : 'greybg';
			$arrangement = ($arrangement == 'rightImage') ? 'leftImage' : 'rightImage';
		endforeach;
		?>
