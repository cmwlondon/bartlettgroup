<?php
/*
Template Name: home

@package WordPress
@subpackage stackbartlett
*/

$defaultFooter = true;

$thumbnailID = get_post_thumbnail_id($post->ID);
$imageData = wp_get_attachment_image_src( $thumbnailID, 'full');
$headerImage = $imageData[0];

$metadata = get_post_meta($post->ID);

set_query_var( 'postid', $post->ID );
set_query_var( 'individual_title', $metadata['bgh_individual_products_head'][0] );
set_query_var( 'business_title', $metadata['bgh_business_products_head'][0] );
set_query_var( 'individual', $metadata['bgh_individual_order'][0] );
set_query_var( 'business', $metadata['bgh_business_order'][0] );
set_query_var( 'quote_context', 'home' );
set_query_var( 'metakey', 'bgh_quotes' );

?>
<?php get_header();?>
	</div>

	<div id="content" class="site-content">

		<div class="fullscreen">
			<div class="clipper video">
				<video id="homevideo" preload="metadata" poster="<?php echo $metadata['bgh_videoposter'][0]; ?>" autoplay loop muted nocontrols playsinline>
						<?php if ($metadata['bgh_video_mp4'][0] != '' ) : ?><source src="<?php echo $metadata['bgh_video_mp4'][0]; ?>" type="video/mp4"/><?php endif; ?>
						<?php if ($metadata['bgh_video_webm'][0] != '' ) : ?><source src="<?php echo $metadata['bgh_video_webm'][0]; ?>" type="video/webm"/><?php endif; ?>
						<?php if ($metadata['bgh_video_ogg'][0] != '' ) : ?><source src="<?php echo $metadata['bgh_video_ogg'][0]; ?>" type="video/ogg"/><?php endif; ?>
				</video>
			</div>
		</div>

		<div class="pagetop">
			<div class="headerBackground"></div>
			<div class="overlay homeOverlay">
				<div>
					<h1><?php echo $metadata['bgh_banner_text'][0]; ?></h1>
					<a href="#contentCATUAnchor" class="nuButton catuTrigger"><span class="caret"><?php echo $metadata['bgh_banner_button_label'][0]; ?></span></a>
				</div>
			</div>
			<div class="down"><span>scroll down for page content</span></div>
		</div>

		<article class="module homeIntro intro" id="contentAnchor">
			<div class="singleColumn">
				<h2><?php echo $metadata['bgh_intro_head'][0]; ?></h2>
				<hr class="bgindividual02">
				<p><?php echo $metadata['bgh_intro_copy'][0]; ?></p>
			</div>
		</article>

<!-- meet the team START -->
		<!-- divider_home 891 / 1346 66.19% -->
		<article class="module meettheteam dualdpiSelect" id="contentCATUAnchor" data-normal="<?php echo $metadata['bgh_mtt_normal'][0]; ?>" data-retina="<?php echo $metadata['bgh_mtt_retina'][0]; ?>" style="xbackground-image:url('<?php echo $ourStory['aos_img1']; ?>');">
			<div class="bg"><div class="mttIOSclip"><img></div></div>
			<div class="doubleColumn clearfix">
				<div class="column">
					<h2><?php echo $metadata['bgh_right_head'][0]; ?></h2>
					<div>
						<?php echo $metadata['bgh_right_copy'][0]; ?>
					</div>
					<a href="<?php echo $metadata['bgh_button_link'][0]; ?>" class="nuButton"><span class="caret"><?php echo html_entity_decode($metadata['bgh_button_label'][0]); ?></span></a>
				</div>
			</div>
		</article>
<!-- meet the team END -->

<!-- product list START -->
		<?php get_template_part( 'modules/products2' ); ?>
<!-- product list END -->

<!-- quotes START -->	
		<?php get_template_part( 'modules/quotes' ); ?>
<!-- quotes END -->

<!-- find out more START -->
		<article class="module whitebg findoutmore">
			<div class="singleColumn">
				<h2><?php echo $metadata['bgh_tail_head'][0]; ?></h2>
				<p><?php echo $metadata['bgh_tail_copy'][0]; ?></p>
				<a href="<?php echo $metadata['bgh_button2_link'][0]; ?>" class="nuButton"><span class="caret"><?php echo html_entity_decode($metadata['bgh_button2_label'][0]); ?></span></a>
			</div>
		</article>
<!-- find out more END -->

<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
