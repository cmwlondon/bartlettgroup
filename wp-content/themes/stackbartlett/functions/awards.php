<?php
function get_awards() {

    $query_parameters = array(
        'post_type' => 'award',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'rand'
    );

	return do_query($query_parameters);
}

function get_awards_in_order($order) {
    $query_parameters = array(
        'post_type' => 'award',
        'post_status' => 'publish',
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

function synchronise_awards( $order, $postid, $metakey ) {
	$allAwards = get_awards();
	return synchronise($order, $allAwards, $postid, $metakey);
}

/*
function synchronise_awards($order, $post_id) {
	
	$allAwards = get_awards();
	$allAwardIDs = get_item_ids($allAwards);

	$newOrderItems = array();
	$removedOrderItems = array();

	// remove items from order list which have been removed from all items
	foreach ( $order AS $key => $id) {
        if (!in_array($id, $allAwardIDs)) {
        	$removedOrderItems[] = array (
        		'key' => $key,
        		'id' => $id
        	);
        }
	}

	$removeReversed = array_reverse($removedOrderItems);
	foreach ( $removeReversed AS $removeItem) {
		array_splice($order, $removeItem['key'], 1);
	}

	// save updated order list
    update_post_meta($post_id, 'about_award_order', implode(',',$order) );

    // find products which haven't been assigned to a product page
	$unassigned = array_diff($allAwardIDs, $order);

	return array(
		'assigned' => $order,
		'unassigned' => $unassigned
	);
}
*/
?>