<?php
// email form
add_action( 'wp_ajax_nopriv_send_email', 'send_email_ajax' );
add_action( 'wp_ajax_send_email', 'send_email_ajax' );

// ajax action : 'send_email'

function send_email_ajax(){
	// verify nonce
	switch ( $_REQUEST['context'] ) {
		case 'individual' : {
			$nonceVerified = wp_verify_nonce( $_REQUEST['key'], 'email_individual_'.$_REQUEST['post'] );
		} break;
		case 'business' : {
			$nonceVerified = wp_verify_nonce( $_REQUEST['key'], 'email_business_'.$_REQUEST['post'] );
		} break;
	}

	if ( $nonceVerified ) {
		// nonce OK
		// verify fields: name, email, query/comment

		$data = array(
			'context' => $_REQUEST['context'], // individual / business
			'post' => $_REQUEST['post'], // post id of page/post from which form was triggered
			'key' => $_REQUEST['key'],
			'verified' => ($nonceVerified) ? 'true' : 'false',
			'forename' => $_REQUEST['forename'],
			'surname' => $_REQUEST['surname'],
			'role' => $_REQUEST['role'],
			'company' => $_REQUEST['company'],
			'phone' => $_REQUEST['phone'],
			'mobile' => $_REQUEST['mobile'],
			'email1' => $_REQUEST['email1'],
			'email2' => $_REQUEST['email2']
		);

		$invalidfields = array();
		$requiredFields = array('forename', 'surname', 'email1', 'email2');
		$emailFields = array('email1', 'email2');
		// check for empty fields
		foreach( $requiredFields as $field ){
			$thisField = $data[$field];

			if( preg_match('/^[\s]*$/' , $thisField ) ){
				$invalidfields[] = $field;
			} else {
				if ( strpos($field, 'email') !== false ) {
					// email field - is it a valid email address
					if (!filter_var($thisField, FILTER_VALIDATE_EMAIL)){
						$invalidfields[] = $field;
					}
				}
			}
		}

		// if all required fields are populated and valid
		if ( count($invalidfields) == 0) {
			// do the email fields match?
			if ( $data['email1'] !== $data['email2']) {
				$invalidfields[] = 'email1-email2';
			}
		}

		if ( count($invalidfields) == 0) {
			// all fields validate, process

			// build and send email

			$response = array(
				'status' => 'ok',
				'data' => $data
			);
		} else {
			// go back to form with errors

			/* errors
			'forname' - missing forename
			'surname' - missing surname
			'email1' - missing/invalid email1
			'email2' - missing/invalid email2
			'email1-email2' - email1 != email2
			*/
			$response = array(
				'status' => 'fail',
				'error' => $invalidfields
			);
		}
	
	} else {
		$response = array(
			'status' => 'fail',
			'error' => array('nonce')
		);
	}	
	
	generic_ajax( $response );
}
?>