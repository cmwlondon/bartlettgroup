<?php
// $filter = 'insurance' or 'wealth management'
function get_business_products($filters) {
	if (!is_null($filters) && count($filters) > 0) {
	    $query_parameters = array(
	        'post_type' => 'business_product',
	        'post_status' => 'publish',
			'tax_query' => array(
				array(//filtering on tags
					'taxonomy' => 'post_tag',
					'field' => 'slug',
					'terms' => array( $filters )
				)
			),
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );
	} else {
	    $query_parameters = array(
	        'post_type' => 'business_product',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );
	}

	return do_query($query_parameters);
}
// filter1 AND filter2 AND ...
function get_business_products_on_and($filters) {
	if (!is_null($filters) && count($filters) > 0) {
		$tagQueries = [];
		foreach ($filters As $filter) {
			$tagQueries[] = array(
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => $filter
			);
		}

	    $query_parameters = array(
	        'post_type' => 'business_product',
	        'post_status' => 'publish',
			'tax_query' => $tagQueries,
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );
	} else {
	    $query_parameters = array(
	        'post_type' => 'business_product',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );
	}

	return do_query($query_parameters);
}
function get_business_product_by_id($productid) {
    $query_parameters = array(
        'post_type' => 'business_product',
        'post_status' => 'publish',
        'p' => $productid,
        'posts_per_page' => 1
    );

	return do_query($query_parameters);
}

function get_business_products_in_order($order) {
    $query_parameters = array(
        'post_type' => 'business_product',
        'post_status' => 'publish',
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

function get_product_parent_page($productid){
	$query_parameters = array(
		'post_type'  => 'page',
		'meta_query' => array(
			array(
				'key'     => 'bp2_product',
				'value'   => $productid
			)
		)
	);
	return do_query($query_parameters);
}

function synchronise_business_products($order, $filters, $post_id) {
	$filteredBusinessProducts = get_business_products_on_and($filters);
	$filteredBusinessProductIDs = get_item_ids($filteredBusinessProducts);

	$newOrderItems = array();
	$removedOrderItems = array();

	// remove items from order list which have been removed from all items
	foreach ( $order AS $key => $id) {
        if (!in_array($id, $filteredBusinessProductIDs)) {
        	$removedOrderItems[] = array (
        		'key' => $key,
        		'id' => $id
        	);
        }
	}

	$removeReversed = array_reverse($removedOrderItems);
	foreach ( $removeReversed AS $removeItem) {
		array_splice($order, $removeItem['key'], 1);
	}

	// save updated order list
    update_post_meta($post_id, 'bp1_ci_items', implode(',',$order) );

    // find products which haven't been assigned to a product page
	$unassigned = array_diff($filteredBusinessProductIDs, $order);

	return array(
		'assigned' => $order,
		'unassigned' => $unassigned
	);
}

function find_related_product_pages($filters, $currentPage) {
	$tagQueries = [];
	foreach ($filters As $filter) {
		$tagQueries[] = array(
			'taxonomy' => 'post_tag',
			'field' => 'slug',
			'terms' => $filter
		);
	}

    $query_parameters = array(
	    'post_type'  => 'page', 

		'tax_query' => $tagQueries,

		'post__not_in' => array($currentPage),
		
	    'meta_query' => array( 
	        array(
	            'key'   => '_wp_page_template', 
	            'value' => 'business-product2.php'
	        )
	    )
    );

	return do_query($query_parameters);
}
?>