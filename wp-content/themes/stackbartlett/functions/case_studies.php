<?php
function get_all_case_studies($postid = null) {
	if (is_null($postid)) {
		// no post id passed in to function, get *ALL* products
	    $query_parameters = array(
	        'post_type' => 'case_study',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );
	} else {
		// get all products except product identified by $postid
	    $query_parameters = array(
			'post__not_in' => array($postid),	    	
	        'post_type' => 'case_study',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );

	}

	return do_query($query_parameters);
}

function get_filtered_case_studies($filters) {

	// build taxonomy query based on tags passed in array filters
	// tag1 AND tag2 AND ...
	$tagQueries = [];
	foreach ($filters As $filter) {
		$tagQueries[] = array(
			'taxonomy' => 'post_tag',
			'field' => 'slug',
			'terms' => $filter
		);
	}

    $query_parameters = array(
        'post_type' => 'case_study',
        'post_status' => 'publish',
    	'tax_query' => $tagQueries,
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' =>'ASC'
    );

	return do_query($query_parameters);
}

function get_case_studies_in_order($order) {
    $query_parameters = array(
        'post_type' => 'case_study',
        'post_status' => 'publish',
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

function synchronise_case_studies($order, $filter, $postid) {
	if (count($filter) > 0) {
		$allCaseStudies = get_filtered_case_studies($filter);
		$results = synchronise($order, $allCaseStudies, $postid, 'bp_case_studies');
	} else {
		$results = array(
			'assigned' => [],
			'unassigned' => []
		);
	}

	return $results;
}

/* ajax call function template START */
/*
	add_action( 'wp_ajax_nopriv_template', 'template_ajax' ); // front end (no privilege) access
	add_action( 'wp_ajax_template', 'template_ajax' ); // wp admin only access

	// ajax action : 'template'

	function template_ajax(){
		// insert your data into $data for JSON response 
		// $_POST[] or $_REQUEST[]
		$data = array(
			"action" => "template",
			"response" => "OK"
		);

		generic_ajax( $data );
	}
*/
/* ajax call function template END */

add_action( 'wp_ajax_nopriv_get_all_case_studies', 'get_all_case_studies_ajax' ); // front end (no privilege) access
add_action( 'wp_ajax_get_all_case_studies', 'get_all_case_studies_ajax' ); // wp admin only access

// ajax action : 'get_all_case_studies'

function get_all_case_studies_ajax(){
	$postid = $_REQUEST['postid'];

	$items = get_all_case_studies($postid);

	$itemsPrime = array();
	$itemIDs = array();

	foreach($items AS $item) {
		$itemIDs[] = $item->ID; // build list of product ids
		// $itemMetaData = get_post_meta($item->ID);

		// get tags for each product
		$taglist = buildTaglist($item->ID);

		$itemsPrime[] = array(
			"id" => $item->ID,
			"title" => $item->post_title,
			"tags" => $taglist['string']
		);
	}

	$data = array(
		"action" => "get_all_case_studies",
		"postid" => $postid,
		"items" => $itemsPrime,
		"ids" => $itemIDs,
		"response" => "OK"
	);

	generic_ajax( $data );
}

add_action( 'wp_ajax_nopriv_get_filtered_case_studies', 'get_filtered_case_studies_ajax' ); // front end (no privilege) access
add_action( 'wp_ajax_get_filtered_case_studies', 'get_filtered_case_studies_ajax' ); // wp admin only access

// ajax action : 'get_filtered_case_studies'

function get_filtered_case_studies_ajax(){
	$postid = $_REQUEST['postid'];
	$tags = $_REQUEST['tags'];
	$order = $_REQUEST['order'];

	$items = get_filtered_case_studies( explode(',', $tags) );

	$itemsPrime = array();
	$itemIDs = array();

	$assignedItems = synchronise_case_studies( explode(',', $order), explode(',', $tags), $postid );

	foreach($items AS $item) {
		$itemIDs[] = $item->ID; // build list of product ids
		// $itemMetaData = get_post_meta($item->ID);

		// get tags for each product
		$taglist = buildTaglist($item->ID);

		$itemsPrime[] = array(
			"id" => $item->ID,
			"title" => $item->post_title,
			"tags" => $taglist['string']
		);
	}

	$data = array(
		"action" => "get_filtered_case_studies",
		"postid" => $postid,
		"tags" => $tags,
		"order" => $order,
		"items" => $itemsPrime,
		"ids" => $itemIDs,
		"assigned" => $assignedItems['assigned'],
		"unassigned" => $assignedItems['unassigned'],
		"response" => "OK"
	);

	generic_ajax( $data );
}

?>
