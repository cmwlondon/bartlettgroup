<?php
/*
functions relating to custom post type 'product'

get_all_products()
get_filtered_products($tags,$mode)
get_products_by_order($order)
get_related_products($tags,$postid)
synchronise_products($order, $tags, $mode, $postid, $metakey)
*/

// get_all_products()
function get_all_products($postid = null) {
	if (is_null($postid)) {
		// no post id passed in to function, get *ALL* products
	    $query_parameters = array(
	        'post_type' => 'product',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );
	} else {
		// get all products except product identified by $postid
	    $query_parameters = array(
			'post__not_in' => array($postid),	    	
	        'post_type' => 'product',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );

	}

	return do_query($query_parameters);
}

// get_filtered_products($tags,$mode)
// $mode = 'and'
// matches tag1 AND tag2 AND ..
// $mode = 'or'
// matches tag1 OR tag2 OR ..
function get_filtered_products($tags, $mode) {
	$tagQueries = [];

	switch ($mode) {
		case "and" : {
			foreach ($tags As $tag) {
				$tagQueries[] = array(
					'taxonomy' => 'post_tag',
					'field' => 'slug',
					'terms' => $tag
				);
			}

		} break;
		case "or" : {
			$tagQueries = array(
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => $tags
			);
		} break;
	}

    $query_parameters = array(
        'post_type' => 'product',
        'post_status' => 'publish',
		'tax_query' => $tagQueries,
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' =>'ASC'
    );

	return do_query($query_parameters);
}

// get_products_by_order($order)
function get_products_by_order($order) {
    $query_parameters = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

// get_related_products($tags,$postid)
// returns products which match all $tags, excluding the product with id $postid
function get_related_products($tags, $postid) {

	$tagQueries = [];

	foreach ($tags As $tag) {
		$tagQueries[] = array(
			'taxonomy' => 'post_tag',
			'field' => 'slug',
			'terms' => $tag
		);
	}

    $query_parameters = array(
        'post_type' => 'product',
        'post_status' => 'publish',
		'tax_query' => $tagQueries,
		'post__not_in' => array($postid),
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' =>'ASC'
    );

	return do_query($query_parameters);
}

// synchronise_products($order, $tags, $postid, $metakey)
function synchronise_products( $order, $tags, $postid, $metakey ) {
	$filteredProducts = get_filtered_products($tags, 'and');

	return synchronise($order, $filteredProducts, $postid, $metakey);
}

function synchronise_related_products( $order, $postid ) {
	$allProducts = get_all_products($postid);
	// return array(gettype($order) , $postid, $allProducts);
	return synchronise($order, $allProducts, $postid, 'rld_order');
}

/* ajax call function template START */
/*
	add_action( 'wp_ajax_nopriv_template', 'template_ajax' ); // front end (no privilege) access
	add_action( 'wp_ajax_template', 'template_ajax' ); // wp admin only access

	// ajax action : 'template'

	function template_ajax(){
		// insert your data into $data for JSON response 
		// $_POST[] or $_REQUEST[]
		$data = array(
			"action" => "template",
			"response" => "OK"
		);

		generic_ajax( $data );
	}
*/
/* ajax call function template END */

add_action( 'wp_ajax_nopriv_get_all_products', 'get_all_products_ajax' ); // front end (no privilege) access
add_action( 'wp_ajax_get_all_products', 'get_all_products_ajax' ); // wp admin only access

// ajax action : 'get_all_products'

function get_all_products_ajax(){
	$postid = $_REQUEST['postid'];
	$products = get_all_products($postid); // pass in current product id to exclude this product from list of all products

	$productsPrime = array();
	$productIDs = array();

	foreach($products AS $product) {
		$productIDs[] = $product->ID; // build list of product ids
		// $productMetaData = get_post_meta($product->ID);

		// get tags for each product
		$taglist = buildTaglist($product->ID);

		$productsPrime[] = array(
			"id" => $product->ID,
			"title" => $product->post_title,
			"tags" => $taglist['string']
		);
	}
	

	$data = array(
		"action" => "get_all_products",
		"postid" => $postid,
		"products" => $productsPrime,
		"ids" => $productIDs,
		"response" => "OK"
	);

	generic_ajax( $data );
}

add_action( 'wp_ajax_nopriv_get_filtered_products', 'get_filtered_products_ajax' ); // front end (no privilege) access
add_action( 'wp_ajax_get_filtered_products', 'get_filtered_products_ajax' ); // wp admin only access

// ajax action : 'get_filtered_products'

function get_filtered_products_ajax(){
	$postid = $_REQUEST['postid'];
	$tags = $_REQUEST['tags'];

	// $products = get_filtered_products( explode(',', $tags), 'and' );
	$products = get_related_products( explode(',', $tags), $postid);

	$productsPrime = array();
	$productIDs = array();

	foreach($products AS $product) {
		$productIDs[] = $product->ID; // build list of product ids
		// $productMetaData = get_post_meta($product->ID);

		// get tags for each product
		$taglist = buildTaglist($product->ID);

		$productsPrime[] = array(
			"id" => $product->ID,
			"title" => $product->post_title,
			"tags" => $taglist['string']
		);
	}

	$data = array(
		"action" => "get_filtered_products",
		"postid" => $postid,
		"tags" => $tags,
		"products" => $productsPrime,
		"ids" => $productIDs,
		"response" => "OK"
	);

	generic_ajax( $data );
}

?>