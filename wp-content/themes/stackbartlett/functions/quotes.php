<?php
function get_quotes() {

    $query_parameters = array(
        'post_type' => 'quote',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'rand'
    );

	return do_query($query_parameters);
}

function get_quotes_in_order($order) {
    $query_parameters = array(
        'post_type' => 'quote',
        'post_status' => 'publish',
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

function synchronise_quotes($order, $context, $metakey, $post_id) {
	
	$allQuotes = get_quotes();
	$allQuoteIDs = get_item_ids($allQuotes);

	$newOrderItems = array();
	$removedOrderItems = array();

	// remove items from order list which have been removed from all items
	foreach ( $order AS $key => $id) {
        if (!in_array($id, $allQuoteIDs)) {
        	$removedOrderItems[] = array (
        		'key' => $key,
        		'id' => $id
        	);
        }
	}

	$removeReversed = array_reverse($removedOrderItems);
	foreach ( $removeReversed AS $removeItem) {
		array_splice($order, $removeItem['key'], 1);
	}

	// save updated order list
    update_post_meta($post_id, $metakey, implode(',',$order) );

    // find products which haven't been assigned to a product page
	$unassigned = array_diff($allQuoteIDs, $order);

	return array(
		'assigned' => $order,
		'unassigned' => $unassigned
	);
}
?>