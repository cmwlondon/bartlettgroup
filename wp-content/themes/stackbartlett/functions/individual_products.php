<?php
// $filter = 'insurance' or 'wealth management'
function get_individual_products($filters = null) {
	if (!is_null($filters) && count($filters) > 0) {
		$tagQueries = [];
		foreach ($filters As $filter) {
			$tagQueries[] = array(
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => $filter
			);
		}

	    $query_parameters = array(
	        'post_type' => 'individual_product',
	        'post_status' => 'publish',
			'tax_query' => $tagQueries,
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );
	} else {
	    $query_parameters = array(
	        'post_type' => 'individual_product',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	        'orderby' => 'ID',
	        'order' =>'ASC'
	    );
	}

	return do_query($query_parameters);
}

function get_products_in_order($order) {
    $query_parameters = array(
        'post_type' => 'individual_product',
        'post_status' => 'publish',
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

function synchronise_individual_products($order, $filter, $post_id) {

	$filteredProducts = get_individual_products($filter);
	$filteredProductIDs = get_item_ids($filteredProducts);

	$newOrderItems = array();
	$removedOrderItems = array();

	// remove items from order list which have been removed from all items
	foreach ( $order AS $key => $id) {
        if (!in_array($id, $filteredProductIDs)) {
        	$removedOrderItems[] = array (
        		'key' => $key,
        		'id' => $id
        	);
        }
	}

	$removeReversed = array_reverse($removedOrderItems);
	foreach ( $removeReversed AS $removeItem) {
		array_splice($order, $removeItem['key'], 1);
	}

	// save updated order list
    update_post_meta($post_id, 'ip_product_list_product_items', implode(',',$order) );

    // find products which haven't been assigned to a product page
	$unassigned = array_diff($filteredProductIDs, $order);

	return array(
		'assigned' => $order,
		'unassigned' => $unassigned
	);
}
?>