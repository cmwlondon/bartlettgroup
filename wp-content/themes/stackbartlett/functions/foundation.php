<?php
// get_all_foundations()
function get_all_foundations() {
    $query_parameters = array(
        'post_type' => 'foundation',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' =>'ASC'
    );
	return do_query($query_parameters);
}

// get_foundations_by_order($order)
function get_foundations_by_order($order) {
    $query_parameters = array(
        'post_type' => 'foundation',
        'post_status' => 'publish',
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

// synchronise_foundations($order, $tags, $postid, $metakey)
function synchronise_foundations( $order, $postid, $metakey ) {
	$allFoundations = get_all_foundations();
	$allFoundationIDs = get_item_ids($allFoundations);

	$newOrderItems = array();
	$removedOrderItems = array();

	// remove items from order list which have been removed from all items
	// find missing items
	foreach ( $order AS $key => $id) {
        if (!in_array($id, $allFoundationIDs)) {
        	$removedOrderItems[] = array (
        		'key' => $key,
        		'id' => $id
        	);
        }
	}

	$removeReversed = array_reverse($removedOrderItems);
	foreach ( $removeReversed AS $removeItem) {
		array_splice($order, $removeItem['key'], 1);
	}

	// save updated order list
    update_post_meta($post_id, $metakey, implode(',',$order) );

    // find products which haven't been assigned to a product page
	$unassigned = array_diff($allFoundationIDs, $order);

	return array(
		'assigned' => $order,
		'unassigned' => $unassigned
	);
}
?>