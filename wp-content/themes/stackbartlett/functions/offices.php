<?php
function get_all_offices() {
    $query_parameters = array(
        'post_type' => 'office',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' =>'ASC'
    );

	return do_query($query_parameters);
}

function get_offices_in_order($order) {
    $query_parameters = array(
        'post_type' => 'office',
        'post_status' => 'publish', // only include published offices
        'posts_per_page' => -1,
        'post__in' => $order,
        'orderby' => 'post__in'
    );

	return do_query($query_parameters);
}

function synchronise_offices($order, $post_id) {
	// get list of id of all offices (A)
	$allOffices = get_all_offices();
	$allOfficeIDs = get_item_ids($allOffices);
	$newOrderItems = array();
	$removedOrderItems = array();

	// remove items from order list which have been removed from all items
	foreach ( $order AS $key => $id) {
        if (!in_array($id, $allOfficeIDs)) {
        	$removedOrderItems[] = array (
        		'key' => $key,
        		'id' => $id
        	);
        }
	}

	$removeReversed = array_reverse($removedOrderItems);
	foreach ( $removeReversed AS $removeItem) {
		array_splice($order, $removeItem['key'], 1);
	}

	// update B in database
    update_post_meta($post_id, 'c_locations', implode(',',$order) );

	$unassigned = array_diff($allOfficeIDs, $order);

	return array(
		'assigned' => $order,
		'unassigned' => $unassigned
	);
}
?>