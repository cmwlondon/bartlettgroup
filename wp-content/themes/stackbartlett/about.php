<?php
/*
Template Name: about

@package WordPress
@subpackage stackbartlett
*/

$defaultFooter = true;

// get header image
$thumbnailID = get_post_thumbnail_id($post->ID);
$imageData = wp_get_attachment_image_src( $thumbnailID, 'full');
$headerImage = $imageData[0];

$metadata = get_post_meta($post->ID);

$bannerTitle = $metadata['about_banner_heading'][0];
$bannerNormal = $metadata['about_banner_normal'][0];
$bannerRetina = $metadata['about_banner_retina'][0];

$intro = array(
    $metadata['about_intro_heading'][0],
    $metadata['about_intro_copy'][0]
);

$syncedTeam = synchronise_team(explode(',', $metadata['about_team_order'][0]), $postid);
$management_team = array(
	'title' => $metadata['about_team_title'][0],
	'copy' => $metadata['about_team_copy'][0]
);

$ourStory = array(
	'aos_img1' => $metadata['aos_img1'][0],
	'aos_heading1' => $metadata['aos_heading1'][0],
	'aos_copy1' => html_entity_decode($metadata['aos_copy1'][0]),
	'aos_heading2' => $metadata['aos_heading2'][0],
	'aos_copy2l' => html_entity_decode($metadata['aos_copy2l'][0]),
	'aos_copy2r' => html_entity_decode($metadata['aos_copy2r'][0]),
	'aos_img2' => $metadata['aos_img2'][0],
	'aos_quote' => html_entity_decode($metadata['aos_quote'][0])
);

$story1Normal = $metadata['ourstory1_normal'][0];
$story1Retina = $metadata['ourstory1_retina'][0];
$story2Normal = $metadata['ourstory2_normal'][0];
$story2Retina = $metadata['ourstory2_retina'][0];

$foundationCaption = html_entity_decode($metadata['about_foundations_caption'][0]);
$syncedFoundationItems = synchronise_foundations(explode(',', $metadata['about_foundations_order'][0]), $post->ID, 'about_foundations_order');
$foundations = (count($syncedFoundationItems['assigned']) > 0 ) ? get_foundations_by_order($syncedFoundationItems['assigned']) : [];

set_query_var( 'awards_title', $metadata['about_awards_title'][0] );
set_query_var( 'awards_order', $metadata['about_awards_order'][0] );
set_query_var( 'pageid', $post->ID );
set_query_var( 'metakey', 'about_awards_order' );

get_header();
?>
	</div>

	<div id="content" class="site-content aboutPage">
		<div class="fsSubPage">
			<div class="headerBackground"></div>
			<div class="overlay">
				<div class="table"><h1><?php echo $bannerTitle; ?></h1></div>
			</div>
			<div class="clipper2 inlinedpiSelect" data-normal="<?php echo $bannerNormal; ?>" data-retina="<?php echo $bannerRetina; ?>"><div><img alt="<?php echo $bannerTitle; ?>"></div></div>
			<!-- div class="down"><span>scroll down for page content</span></div> -->
			<!-- <div class="clipper dpiSelect" data-normal="<?php echo $bannerNormal; ?>" data-retina="<?php echo $bannerRetina; ?>" style="background-image:url('<?php echo $bannerNormal; ?>');"></div> -->
		</div>
		
<!-- page intro START -->
		<article class="module intro" id="contentAnchor">
			<div class="singleColumn p1">
				<h2><?php echo $intro[0]; ?></h2>
				<hr class="bgindividual02">
				<div><?php echo html_entity_decode($intro[1]); ?></div>
			</div>
		</article>
<!-- page intro END -->

<!-- senior team START -->
<?php
if ( count($syncedTeam['assigned']) > 0 ) :
	set_query_var( 'team', $syncedTeam['assigned'] );
?>
		<article class="module seniorteam" id="seniorteam">
			<div class="singleColumn">
				<h2><?php echo $management_team['title']; ?></h2>
				<hr>
				<p><?php echo $management_team['copy']; ?></p>
			</div>
			<?php get_template_part( 'modules/people-about' ); ?>
			<div class="singleColumn cubutton">
				<a href="<?php echo $metadata['about_contact_us'][0]; ?>" class="nuButton"><span class="caret">CONTACT US</span></a>
			</div>
		</article>
<?php
endif;
?>
<!-- senior team END -->

<!-- bartlett story START -->
<article class="story module dualdpiSelect" data-normal="<?php echo $story1Normal; ?>" data-retina="<?php echo $story1Retina; ?>">
	<div class="bg"><img></div>
	<div class="singleColumn area1">
		<h2><?php echo $ourStory['aos_heading1']; ?></h2>
	</div>
	<div class="area2">
		<p class="intro"><?php echo $ourStory['aos_copy1']; ?></p>
		<div class="doubleColumn clearfix">
			<div class="column image"><img class="inlinedpiSelect" alt="" data-normal="<?php echo $story2Normal; ?>" data-retina="<?php echo $story2Retina; ?>" src="<?php echo $story2Normal; ?>"></div><div class="column text"><?php echo $ourStory['aos_copy2r']; ?></div>
		</div>
	</div>
</article>
<!-- bartlett story END -->

<!-- bartlett foundation START -->
<?php
	if ( count($foundations) > 0 ) :
		$col_a = '';
		$col_b = '';
		foreach($foundations AS $key => $foundation) :
			$foundationMetadata = get_post_meta($foundation->ID);
			$title = $foundationMetadata['foundation_title'][0];
			$copy = html_entity_decode($foundationMetadata['foundation_copy'][0]);
			$image = $foundationMetadata['foundation_image'][0];

			$offset = ($key == 0) ? '0%' : '100%';
			$col_a = $col_a . "<div class=\"item\" style=\"left:$offset;\"><div class=\"clip\"><img alt=\"$title\" src=\"$image\"></div></div>";
		endforeach;
?>
<article class="foundation module">
	<div class="singleColumn">
		<h2><?php echo $metadata['about_foundations_title'][0]; ?></h2>
		<hr class="bgindividual02">
		<p><?php echo html_entity_decode($metadata['about_foundations_copy'][0]); ?></p>
	</div>
	<div class="foundationItems doubleColumn clearfix">
		<div class="images column">
			<!-- /* 462 x 254 */ -->
			<div class="frame">
				<?php echo $col_a; ?>
			</div>
			<div class="controls"></div>
		</div>
		<div class="captions column">
			<?php echo $foundationCaption; ?>
		</div>
	</div>
</article>
<?php endif; ?>
<!-- bartlett foundation END -->

<!-- awards START -->
<!-- custom post type: award -->
		<?php get_template_part( 'modules/awards' ); ?>
<!-- awards END -->


<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
