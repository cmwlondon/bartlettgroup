<div class="customField mediumtext">
	<label for="person_forename">Forename</label>
	<input type="text" id="person_forename" name="person_forename" value="<?php echo $values['forename']; ?>">
</div>

<div class="customField mediumtext">
	<label for="person_surname">Surname</label>
	<input type="text" id="person_surname" name="person_surname" value="<?php echo $values['surname']; ?>">
</div>

<div class="customField mediumtext">
	<label for="person_role">Role</label>
	<input type="text" id="person_role" name="person_role" value="<?php echo $values['role']; ?>">
</div>

<div class="customField mediumtext">
	<label for="person_section">Section</label>
	<input type="text" id="person_section" name="person_section" value="<?php echo $values['section']; ?>">
</div>

<div class="customField mediumtext">
	<label for="person_email">Email</label>
	<input type="text" id="person_email" name="person_email" value="<?php echo $values['email']; ?>">
</div>

<div class="customField mediumtext">
	<label for="person_linkedin">Linkedin</label>
	<input type="text" id="person_linkedin" name="person_linkedin" value="<?php echo $values['linkedin']; ?>">
</div>

<div class="customField mediumtext">
	<label for="person_phone">Phone</label>
	<input type="text" id="person_phone" name="person_phone" value="<?php echo $values['phone']; ?>">
</div>

<div class="customField mediumtext">
	<label for="person_mobile">Mobile</label>
	<input type="text" id="person_mobile" name="person_mobile" value="<?php echo $values['mobile']; ?>">
</div>

<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label for="personNorml">Portrait image (normal)</label>
		<input type="hidden" id="personNorml" name="personNorml" value="<?php echo $values['person_normal']; ?>">
		<input type="hidden" id="person_normal_id" name="person_normal_id" value="<?php echo $values['person_normal_id']; ?>" class="ident">
		<input type="hidden" id="person_normal_width" name="person_normal_width" value="<?php echo $values['person_normal_w']; ?>" class="width">
		<input type="hidden" id="person_normal_height" name="person_normal_height" value="<?php echo $values['person_normal_h']; ?>" class="height">
		<a href="" class="personNormalPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['person_normal']; ?>" <?php if ($values['person_normal'] == '' || $values['person_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['person_normal'] == '' || $values['person_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['person_normal']; ?></p>
	</div>

	<div class="customField imageSelector multiImage">
		<label for="personRetina">Portrait image (retina)</label>
		<input type="hidden" id="personRetina" name="personRetina" value="<?php echo $values['person_retina']; ?>">
		<input type="hidden" id="person_retina_id" name="person_retina_id" value="<?php echo $values['person_retina_id']; ?>" class="ident">
		<input type="hidden" id="person_retina_width" name="person_retina_width" value="<?php echo $values['person_retina_w']; ?>" class="width">
		<input type="hidden" id="person_retina_height" name="person_retina_height" value="<?php echo $values['person_retina_h']; ?>" class="height">
		<a href="" class="personRetinaPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['person_retina']; ?>" <?php if ($values['person_retina'] == '' || $values['person_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['person_retina'] == '' || $values['person_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['person_retina']; ?></p>
	</div>
</div>

<!--
<div class="customField imageSelector">
	<label for="person_portrait">Main image</label>
	<input type="hidden" id="person_portrait" name="person_portrait" value="<?php echo $values['portrait']; ?>">
	<input type="hidden" id="person_portrait_id" name="person_portrait_id" value="<?php echo $values['portrait_id']; ?>" class="ident">
	<input type="hidden" id="person_portrait_width" name="person_portrait_width" value="<?php echo $values['portrait_w']; ?>" class="width">
	<input type="hidden" id="person_portrait_height" name="person_portrait_height" value="<?php echo $values['portrait_h']; ?>" class="height">
	<a href="" class="mediaTrigger_portrait action">Add/Update image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['portrait']; ?>" <?php if ($values['portrait'] == '' || $values['portrait'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['portrait'] == '' || $values['portrait'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['portrait']; ?></p>
</div>
-->

<div class="customField mediumtext">
	<label for="person_bio">Bio</label>
	<textarea id="person_bio" name="person_bio"><?php echo $values['bio']; ?></textarea>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.personNormalPicker')
    }));
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.personRetinaPicker')
    }));

	/* thisMediaPanel_large = new MediaPanel({
		"trigger" : jQuery('.mediaTrigger_portrait')
	}); */
});
</script>
