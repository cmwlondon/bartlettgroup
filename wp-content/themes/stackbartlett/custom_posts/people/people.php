<?php
/*
custom post type: person

forename
surname
role
section
email
linkedin
phone
mobile
image1 (large)
image2 (small) NOT USED

*/

function bartlett_people_init()
{
        $labels = array(
            'name'                => _x( 'People', 'post type general name' ),
            'singular_name'       => _x( 'Person', 'post type singular name' ),
            'add_new'             => _x( 'Add New Person', 'person' ),
            'add_new_item'        => __( 'Add New Person'),
            'edit_item'           => __( 'Edit Person' ),
            'new_item'            => __( 'New Person' ),
            'all_items'           => __( 'All People' ),
            'view_item'           => __( 'View Person' ),
            'search_items'        => __( 'Search People' ),
            'not_found'           => __( 'No People found' ),
            'not_found_in_trash'  => __( 'No People§ found in Trash' ),
            'parent_item_colon'   => '',
            'menu_name'           => 'People'
        );
        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'has_archive'        => 'People',
            'hierarchical'       => false,
            'menu_position'      => null,
            // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
            'supports'           => array( 'title', 'thumbnail'),
            'register_meta_box_cb' => 'add_person_metabox'
        );

        register_post_type( 'person', $args );

        /* add custom columns to the admin people 'browse' page */
        function person_columns_head($defaults) {
            $date = $defaults['date'];  // save the date column
            unset($defaults['date']);

            $defaults['_person_image'] = 'Portrait';
            $defaults['_person_role'] = 'Role';
            $defaults['_person_section'] = 'Section';
            $defaults['date'] = $date;
            return $defaults;
        }

        /* define what is displayed in each column */ 
        function person_columns_content($column_name, $post_ID) {
            $metadata = $metadata = get_post_meta($post_ID);

            $role = $metadata['p_role'][0];
            $section = $metadata['p_section'][0];
            $portrait = $metadata['person_normal'][0];

            switch($column_name) {
                case "_person_image" : {
                    echo "<img class=\"listPortrait\" alt=\"\" src=\"$portrait\">";
                } break;
                case "_person_role" : {
                    echo $role;
                } break;
                case "_person_section" : {
                    echo $section;
                } break;
            }
        }
        add_filter('manage_person_posts_columns', 'person_columns_head');
        add_action('manage_person_posts_custom_column', 'person_columns_content', 1, 2);

}
add_action( 'init', 'bartlett_people_init' );

/* define custom metabox */
function add_person_metabox() {

    $screens = array( 'person' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'myplugin_of',
            __( 'Person', 'myplugin_textdomain' ),
            'person_meta_box_callback',
            $screen,
            'normal'
        );

    }
}
// add_action( 'add_meta_boxes_quote', 'add_quote_metaboxes' );

function person_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'person_save_meta_box_data', 'person_meta_box_nonce' );

    $values['forename'] = get_post_meta( $post->ID, 'p_forename', true );
    $values['surname'] = get_post_meta( $post->ID, 'p_surname', true );
    $values['role'] = get_post_meta( $post->ID, 'p_role', true );
    $values['section'] = get_post_meta( $post->ID, 'p_section', true );
    $values['email'] = get_post_meta( $post->ID, 'p_email', true );
    $values['phone'] = get_post_meta( $post->ID, 'p_phone', true );
    $values['mobile'] = get_post_meta( $post->ID, 'p_mobile', true );
    $values['linkedin'] = get_post_meta( $post->ID, 'p_linkedin', true );
    $values['person_normal_id'] = get_post_meta( $post->ID, 'person_normal_id', true );
    $values['person_normal_w'] = get_post_meta( $post->ID, 'person_normal_w', true );
    $values['person_normal_h'] = get_post_meta( $post->ID, 'person_normal_h', true );
    $values['person_retina_id'] = get_post_meta( $post->ID, 'person_retina_id', true );
    $values['person_retina_w'] = get_post_meta( $post->ID, 'person_retina_w', true );
    $values['person_retina_h'] = get_post_meta( $post->ID, 'person_retina_h', true );
    $values['person_normal'] = get_post_meta( $post->ID, 'person_normal', true );
    $values['person_retina'] = get_post_meta( $post->ID, 'person_retina', true );
    $values['bio'] = get_post_meta( $post->ID, 'p_bio', true );

    require get_template_directory() . '/custom_posts/people/metaboxes/person.php';
}

function person_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['person_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['person_meta_box_nonce'], 'person_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('person_forename', 'p_forename'),
        array('person_surname', 'p_surname'),
        array('person_role', 'p_role'),
        array('person_section', 'p_section'),
        array('person_email', 'p_email'),
        array('person_linkedin', 'p_linkedin'),
        array('person_phone', 'p_phone'),
        array('person_mobile', 'p_mobile'),
        array('personNorml', 'person_normal'),
        array('person_normal_id', 'person_normal_id'),
        array('person_normal_width', 'person_normal_w'),
        array('person_normal_height', 'person_normal_h'),
        array('personRetina', 'person_retina'),
        array('person_retina_id', 'person_retina_id'),
        array('person_retina_width', 'person_retina_w'),
        array('person_retina_height', 'person_retina_h'),
        array('person_bio','p_bio')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'person_save_meta_box_data' );

?>
