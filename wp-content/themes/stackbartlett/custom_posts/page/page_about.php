<?php
/*
-----------------------------------------------
*/

// about banner START
function about_banner_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_banner_save_meta_box_data', 'about_banner_meta_box_nonce' );

    $values['ab_heading'] = get_post_meta( $post->ID, 'about_banner_heading', true );
    $values['banner_normal'] = get_post_meta( $post->ID, 'about_banner_normal', true );
    $values['banner_retina'] = get_post_meta( $post->ID, 'about_banner_retina', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/about_banner.php';
}

function about_banner_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_banner_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_banner_meta_box_nonce'], 'about_banner_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('aboutBannerheading', 'about_banner_heading'),
        array('banner_normal', 'about_banner_normal'),
        array('banner_retina', 'about_banner_retina')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'about_banner_save_meta_box_data' );
// about banner END

// about intro START
function about_intro_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_intro_save_meta_box_data', 'about_intro_meta_box_nonce' );

    $values['ain_heading'] = get_post_meta( $post->ID, 'about_intro_heading', true );
    $values['ain_copy'] = get_post_meta( $post->ID, 'about_intro_copy', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/about_intro.php';
}

function about_intro_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_intro_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_intro_meta_box_nonce'], 'about_intro_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('aboutIntroheading', 'about_intro_heading'),
        array('aboutIntroCopy', 'about_intro_copy'),
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ($field[1] == 'about_intro_copy') {
                $my_data = htmlentities( $_POST[$field[0]] );
                
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

    /*
    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
    */
}
add_action( 'save_post', 'about_intro_save_meta_box_data' );
// about intro END

/* the Bartlett Story
img1
heading
copy (single P)
rightcol (nulti P)
img2
*/

// about our story START
function about_our_story_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_our_story_save_meta_box_data', 'about_our_story_meta_box_nonce' );

    $values['ourstory1_normal'] = get_post_meta( $post->ID, 'ourstory1_normal', true );
    $values['ourstory1_retina'] = get_post_meta( $post->ID, 'ourstory1_retina', true );
    $values['ourstory2_normal'] = get_post_meta( $post->ID, 'ourstory2_normal', true );
    $values['ourstory2_retina'] = get_post_meta( $post->ID, 'ourstory2_retina', true );

    $values['aos_heading1'] = get_post_meta( $post->ID, 'aos_heading1', true );
    $values['aos_copy1'] = get_post_meta( $post->ID, 'aos_copy1', true );
    $values['aos_copy2r'] = get_post_meta( $post->ID, 'aos_copy2r', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/about_our_story.php';
}

function about_our_story_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_our_story_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_our_story_meta_box_nonce'], 'about_our_story_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('ourstory1_normal', 'ourstory1_normal'),
        array('ourstory1_retina', 'ourstory1_retina'),
        array('ourstory2_normal', 'ourstory2_normal'),
        array('ourstory2_retina', 'ourstory2_retina'),
        array('aosHeading1', 'aos_heading1'),
        array('aosCopy1', 'aos_copy1'),
        array('aosCopy2r', 'aos_copy2r')
    );

    $htmlFields = array('aos_copy1', 'aos_copy2l', 'aos_copy2r', 'aos_quote');

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ( in_array ( $field[1], $htmlFields) ) {
                $my_data = htmlentities( $_POST[$field[0]] );
                
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'about_our_story_save_meta_box_data' );
// about our story END

// about foundations START
function about_foundations_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_foundations_save_meta_box_data', 'about_foundations_meta_box_nonce' );

    $values['af_order'] = get_post_meta( $post->ID, 'about_foundations_order', true );
    $values['af_title'] = get_post_meta( $post->ID, 'about_foundations_title', true );
    $values['af_copy'] = get_post_meta( $post->ID, 'about_foundations_copy', true );
    $values['af_caption'] = get_post_meta( $post->ID, 'about_foundations_caption', true );
    $values['postid'] = $post->ID;

    require get_template_directory() . '/custom_posts/page/metaboxes/about_foundation.php';
}

function about_foundations_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_foundations_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_foundations_meta_box_nonce'], 'about_foundations_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('assignedFoundations', 'about_foundations_order', false),
        array('aboutFoundationTitle', 'about_foundations_title', false),
        array('aboutFoundationCopy', 'about_foundations_copy', true),
        array('aboutFoundationCaption', 'about_foundations_caption', true)
    );

    foreach($fields AS $field) {

        if ( isset( $_POST[$field[0]] )) {
            if ( $field[2] ) {
                $my_data = htmlentities( $_POST[$field[0]] );
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'about_foundations_save_meta_box_data' );
// about foundations END

// about quotes START
function about_quotes_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_quotes_save_meta_box_data', 'about_quotes_meta_box_nonce' );

    $values['about_quotes'] = get_post_meta( $post->ID, 'about_quote_order', true );
    $values['postid'] = $post->ID;

    require get_template_directory() . '/custom_posts/page/metaboxes/about_quotes.php';
}

function about_quotes_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_quotes_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_quotes_meta_box_nonce'], 'about_quotes_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('assignedQuotes', 'about_quote_order'),
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'about_quotes_save_meta_box_data' );
// about quotes END

// about people START
function about_people_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_people_save_meta_box_data', 'about_people_meta_box_nonce' );

    $values['about_team'] = get_post_meta( $post->ID, 'about_team_order', true );
    $values['at_title'] = get_post_meta( $post->ID, 'about_team_title', true );
    $values['at_copy'] = get_post_meta( $post->ID, 'about_team_copy', true );
    $values['about_contact_us'] = get_post_meta( $post->ID, 'about_contact_us', true );

    $values['postid'] = $post->ID;

    require get_template_directory() . '/custom_posts/page/metaboxes/about_people.php';
}

function about_people_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_people_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_people_meta_box_nonce'], 'about_people_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('assignedTeam', 'about_team_order'),
        array('aboutTeamTitle', 'about_team_title'),
        array('aboutTeamCopy', 'about_team_copy'),
        array('aboutContactUs', 'about_contact_us')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'about_people_save_meta_box_data' );
// about people END

// about awards/partners START
function about_awards_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'about_awards_save_meta_box_data', 'about_awards_meta_box_nonce' );

    $values['awards_title'] = get_post_meta( $post->ID, 'about_awards_title', true );
    $values['awards_order'] = get_post_meta( $post->ID, 'about_awards_order', true );
    $values['postid'] = $post->ID;
    
    require get_template_directory() . '/custom_posts/page/metaboxes/about_awards.php';
}

function about_awards_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['about_awards_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['about_awards_meta_box_nonce'], 'about_awards_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('awardsTitle', 'about_awards_title'),
        array('awardsOrder', 'about_awards_order'),
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'about_awards_save_meta_box_data' );
// about about awards/partners END

?>
