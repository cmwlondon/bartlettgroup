<?php
/*
about: quote sorter
$values['about_quotes'] -> #assignedQuotes -> about_quote_order
*/

$syncedQuotes = synchronise_quotes(explode(',', $values['about_quotes']), $values['postid'],'about_quote_order');
$assigned = (count($syncedQuotes['assigned']) > 0 ) ? get_quotes_in_order($syncedQuotes['assigned']) : [];
$unassigned = (count($syncedQuotes['unassigned']) > 0 ) ? get_quotes_in_order($syncedQuotes['unassigned']) : [];
$theme_root = get_template_directory_uri();
$dummy1State = (count($assigned) > 0) ? ' hidden' : '';
$dummy2State = (count($unassigned) > 0) ? ' hidden' : '';
?>

<div class="customField itemSorter teamleaderSorter">
    <input type="hidden" id="assignedQuotes" name="assignedQuotes" value="<?php echo implode(',', $syncedQuotes['assigned']); ?>">

    <p>Assigned</p>
    <ul id="quoteSorterAssign" class="slideConnected">
        <li class="clearfix dummy<?php echo $dummy1State; ?>" id="quoteDummy1">-</li>
    <?php
    foreach($assigned AS $key => $quote) :
        $quote_id = $quote->ID;
    ?>
    <li id="<?php echo "quote$quote_id"; ?>"><?php echo $quote->post_title; ?></li>
    <?php
        endforeach;
    ?>
    </ul>

    <p>Unassigned</p>
    <ul id="quoteSorterUnassign" class="slideConnected">
        <li class="clearfix dummy<?php echo $dummy2State; ?>" id="quoteDummy2">-</li>
    <?php
    foreach($unassigned AS $key => $quote) :
        $quote_id = $quote->ID;
    ?>
    <li id="<?php echo "quote$quote_id"; ?>"><?php echo $quote->post_title; ?></li>
    <?php
        endforeach;
    ?>
    </ul>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // make people order items sortable
        if (jQuery('#quoteSorterAssign').length != 0) {
            jQuery('#quoteSorterAssign, #quoteSorterUnassign').sortable({
                "connectWith" : '.slideConnected',
                "receive" : function ( event, ui ) {
                    if(ui.item.hasClass("dummy"))
                        ui.sender.sortable("cancel");
                },
                "start" : function( event, ui ) {
                },
                "stop" : function( event, ui ) {
                    newOrderList = [];
                    jQuery('#quoteSorterAssign li').not(".dummy").each(function(i){
                        newOrderList.push( jQuery(this).attr("id").substr(5) );
                    });
                    jQuery('#assignedQuotes').val(newOrderList.join());
                }
            }).disableSelection();

            jQuery('#quoteSorterAssign').on('sortstop',function(event, ui) {
                console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
            });
            jQuery('#quoteSorterUnassign').on('sortstop',function(event, ui) {
                console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
            });
        }

    });
</script>

