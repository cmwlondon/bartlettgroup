            <?php
            // bgh_intro_head
            $metabox->the_field('intro_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Intro Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>Intro Copy</p>
                <?php
                // bgh_intro_copy
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('intro_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_intro_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'introcopy', $settings );
            ?>
            </div>

            <div class="multiFieldWrapper">
                <p class="head">Highlight boxes</p>
                <p>
                  <a href="#" title="Remove all links" class="dodelete-highlight_box button">Remove All</a>
                  <a href="#" title="Add new link" class="docopy-highlight_box button">Add Highlight box</a>
                </p>
                <?php
                $options = array('length' => 1, 'limit' => 5);
                while($mb->have_fields_and_multi('highlight_box')) :
                    $mb->the_group_open();
                ?>

                <div class="multiFieldBox">

                    <?php
                    $metabox->the_field('image');
                    $mainImage = $metabox->get_the_value();
                    $mainImageName = $metabox->get_the_name();
                    ?>
                    <div class="customField imageSelector">
                        <label for="<?php echo $mainImageTarget; ?>">Highlight box image</label>
                        <input type="hidden" id="<?php echo $mainImageTarget; ?>" name="<?php echo $mainImageName; ?>" value="<?php echo $mainImage; ?>">
                        <a href="" class="highlightPicker action">Add/Update image</a>
                        <img class="imageThumbnail" alt="" src="<?php echo $mainImage; ?>" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>>
                        <p class="imagepath" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $mainImage; ?></p>
                    </div>

                    <?php $mb->the_field('title'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">Title</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <?php $mb->the_field('copy'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">Copy</label>
                        <textarea  id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" cols="80" rows="8"><?php $mb->the_value(); ?></textarea>
                    </div>

                    <?php $mb->the_field('button_text'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">Button Text</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>
                    <?php $mb->the_field('button_link'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">Button Link</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>


                    <a href="#" title="Remove link" class="dodelete button">Remove Highlight box</a>
                </div>

                <?php
                    $mb->the_group_close();
                endwhile;
                ?>
            </div>

            <script type="text/javascript">
            var highlightPickers = [];
            jQuery(document).ready(function(){
                highlightPickers.push(new MediaPanel({
                    "trigger" : jQuery('.highlightPicker')
                }));
            });
            </script>

