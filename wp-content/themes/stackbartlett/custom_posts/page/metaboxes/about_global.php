<?php
/*
    $values['ag_image'] -> #aboutGlobalimage -> about_global_image
    $values['ag_title'] -> #aboutGlobaltitle -> about_global_title
    $values['ag_copy'] -> #aboutGlobalCopy -> about_global_copy
    $values['ag_button_text'] -> #aboutGlobalButtonText -> about_global_button_text
    $values['ag_button_link'] -> #aboutGlobalButtonLink -> about_global_button_link
*/
?>

<div class="customField imageSelector">
	<label for="aboutGlobalimage">background image</label>
	<input type="hidden" id="aboutGlobalimage" name="aboutGlobalimage" value="<?php echo $values['ag_image']; ?>">
	<a href="" class="pic4Picker action">Add/Update product image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['ag_image']; ?>" <?php if ($values['ag_image'] == '' || $values['ag_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['ag_image'] == '' || $values['ag_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ag_image']; ?></p>
</div>

<div class="customField mediumtext">
	<label for="aboutGlobaltitle">Heading</label>
	<input type="text" id="aboutGlobaltitle" name="aboutGlobaltitle" value="<?php echo $values['ag_title']; ?>">
</div>

<div class="customField wpeditor">
	<p>Copy</p>
	<?php
		$settings = array(
			'textarea_name' => 'aboutGlobalCopy',
			'editor_class'  => 'ag_copy_wpeditor',
			'textarea_rows' => 6
		);
		wp_editor( $values['ag_copy'], 'ag_copy', $settings );
	?>
</div>
<!--
<div class="customField mediumtext">
	<label for="aboutGlobalButtonText">Button Text</label>
	<input type="text" id="aboutGlobalButtonText" name="aboutGlobalButtonText" value="<?php echo $values['ag_button_text']; ?>">
</div>
<div class="customField mediumtext">
	<label for="aboutGlobalButtonLink">Button Link</label>
	<input type="text" id="aboutGlobalButtonLink" name="aboutGlobalButtonLink" value="<?php echo $values['ag_button_link']; ?>">
</div>
-->
<script type="text/javascript">
var image4Pickers = [];

jQuery(document).ready(function(){
	image4Pickers.push( new MediaPanel({
		"trigger" : jQuery('.pic4Picker')
	}) );
});
</script>
