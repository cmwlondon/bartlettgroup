<?php
/*
about business product highlight block

    $values['ab_bgcolor'] -> #aboutBusbgcolor -> about_business_bgcolor
    $values['ab_image'] -> #aboutBusimage -> about_business_image
    $values['ab_title'] -> #aboutBustitle -> about_business_title
    $values['ab_copy'] -> #aboutBusCopy -> about_business_copy
    $values['ab_button_text'] -> #aboutBusButtonText -> about_business_button_text
    $values['ab_button_link'] -> #aboutBusButtonLink -> about_business_button_link
*/
?>


<input type="hidden" id="aboutBusbgcolor" name="aboutBusbgcolor" value="<?php /* echo $values['ab_bgcolor']; */ echo '544595'; ?>">

<div class="customField imageSelector">
	<label for="aboutBusimage">Highlight image</label>
	<input type="hidden" id="aboutBusimage" name="aboutBusimage" value="<?php echo $values['ab_image']; ?>">
	<a href="" class="pic2Picker action">Add/Update product image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['ab_image']; ?>" <?php if ($values['ab_image'] == '' || $values['ab_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['ab_image'] == '' || $values['ab_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ab_image']; ?></p>
</div>

<div class="customField mediumtext">
	<label for="aboutBustitle">Heading</label>
	<input type="text" id="aboutBustitle" name="aboutBustitle" value="<?php echo $values['ab_title']; ?>">
</div>

<div class="customField wpeditor">
	<p>Copy</p>
	<?php
		$settings = array(
			'textarea_name' => 'aboutBusCopy',
			'editor_class'  => 'ab_copy_wpeditor',
			'textarea_rows' => 6
		);
		wp_editor( $values['ab_copy'], 'ab_copy', $settings );
	?>
</div>

<div class="customField mediumtext">
	<label for="aboutBusButtonText">Button Text</label>
	<input type="text" id="aboutBusButtonText" name="aboutBusButtonText" value="<?php echo $values['ab_button_text']; ?>">
</div>
<div class="customField mediumtext">
	<label for="aboutBusButtonLink">Button Link</label>
	<input type="text" id="aboutBusButtonLink" name="aboutBusButtonLink" value="<?php echo $values['ab_button_link']; ?>">
</div>

<script type="text/javascript">
var image2Pickers = [];

jQuery(document).ready(function(){
	image2Pickers.push( new MediaPanel({
		"trigger" : jQuery('.pic2Picker')
	}) );
});
</script>
