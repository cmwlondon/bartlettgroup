<?php
/*
about page intro section
heading
copy

$values['heading'] -> #aboutIntroheading -> about_intro_heading
$values['copy'] -> aboutIntroheading -> about_intro_copy
*/
?>
<div class="customField mediumtext">
	<label for="aboutIntroheading">Heading</label>
	<input type="text" id="aboutIntroheading" name="aboutIntroheading" value="<?php echo $values['ain_heading']; ?>">
</div>

<div class="customField wpeditor">
	<p>Copy</p>
	<?php
		$settings = array(
			'textarea_name' => 'aboutIntroCopy',
			'editor_class'  => 'aboutintro_wpeditor',
			'textarea_rows' => 6
		);
		wp_editor( html_entity_decode($values['ain_copy']), 'aboutintro', $settings );
	?>
</div>
