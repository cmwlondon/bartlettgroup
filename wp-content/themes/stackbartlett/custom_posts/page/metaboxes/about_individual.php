<?php
/*
about individual product highlight block

    $values['ai_bgcolor'] -> #aboutIndividbgcolor -> about_individual_bgcolor
    $values['ai_image'] -> #aboutIndividimage -> about_individual_image
    $values['ai_title'] -> #aboutIndividtitle -> about_individual_title
    $values['ai_copy'] -> #aboutIndividCopy -> about_individual_copy
    $values['ai_button_text'] -> #aboutIndividButtonText -> about_individual_button_text
    $values['ai_button_link'] -> #aboutIndividButtonLink -> about_individual_button_link
*/
?>


<input type="hidden" id="aboutIndividbgcolor" name="aboutIndividbgcolor" value="<?php /* echo $values['ai_bgcolor']; */ echo '109dcb'; ?>">

<div class="customField imageSelector">
	<label for="aboutIndividimage">Highlight image</label>
	<input type="hidden" id="aboutIndividimage" name="aboutIndividimage" value="<?php echo $values['ai_image']; ?>">
	<a href="" class="pic1Picker action">Add/Update product image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['ai_image']; ?>" <?php if ($values['ai_image'] == '' || $values['ai_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['ai_image'] == '' || $values['ai_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ai_image']; ?></p>
</div>

<div class="customField mediumtext">
	<label for="aboutIndividtitle">Heading</label>
	<input type="text" id="aboutIndividtitle" name="aboutIndividtitle" value="<?php echo $values['ai_title']; ?>">
</div>

<div class="customField wpeditor">
	<p>Copy</p>
	<?php
		$settings = array(
			'textarea_name' => 'aboutIndividCopy',
			'editor_class'  => 'ai_copy_wpeditor',
			'textarea_rows' => 6
		);
		wp_editor( $values['ai_copy'], 'ai_copy', $settings );
	?>
</div>

<div class="customField mediumtext">
	<label for="aboutIndividButtonText">Button Text</label>
	<input type="text" id="aboutIndividButtonText" name="aboutIndividButtonText" value="<?php echo $values['ai_button_text']; ?>">
</div>
<div class="customField mediumtext">
	<label for="aboutIndividButtonLink">Button Link</label>
	<input type="text" id="aboutIndividButtonLink" name="aboutIndividButtonLink" value="<?php echo $values['ai_button_link']; ?>">
</div>

<script type="text/javascript">
var image1Pickers = [];

jQuery(document).ready(function(){
	image1Pickers.push( new MediaPanel({
		"trigger" : jQuery('.pic1Picker')
	}) );
});
</script>
