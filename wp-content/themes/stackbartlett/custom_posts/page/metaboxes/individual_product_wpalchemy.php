<?php
/*
individual product metabox template

page template: indivdual_product.php

fields:
    title
    intro_head
    intro_copy

    product []

    closing head
    closing copy

    left heading
    left copy
    left button
    left link

    right heading
    right copy
    right button
    right link
*/
?>

<?php $mb->the_field('intro_head'); ?>
<div class="customField mediumtext">
    <label for="<?php $mb->the_name(); ?>">Heading</label>
    <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
</div>

<div class="customField wpeditor">
    <p>Intro copy</p>
    <?php
    // insert a wpeditor into a wpalchemy template
    $mb->the_field('intro_copy');
    $settings = array(
        'textarea_name' => $mb->get_the_name(),
        'editor_class'  => 'intro_copy_wpeditor',
        'textarea_rows' => 6
    );
    wp_editor( html_entity_decode($mb->get_the_value()), 'introcopy', $settings );
?>
</div>

<!-- product sorter START -->
    <?php
    // synchronise_individual_products -> /wp_content/themes/stackbartlett/functions.php
    // post type: individual_product
    $posttags = get_the_tags();
    $taglist = [];
    foreach($posttags AS $tag) {
        $taglist[] = $tag->name;
    }

    // might be more categories under 'individual'
    $productFilter = (in_array('insurance', $taglist)) ? 'insurance' : 'wealth management';

    $md = get_post_meta($post->ID);
    $md = unserialize($md['individual_product_meta'][0]);

    $mb->the_field('product_items');
    $individual_products = synchronise_individual_products(explode(',', $metabox->get_the_value()), array($productFilter), $post->ID);
    $assigned = (count($individual_products['assigned']) > 0) ? get_products_in_order($individual_products['assigned']) : []; // 
    
    $unassigned = (count($individual_products['unassigned']) > 0) ? get_products_in_order($individual_products['unassigned']) : [];

    ?>
    <div class="customField itemSorter productSorter">
        <p class="head">Individual Products</p>
        <input type="hidden" id="assignedProducts" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">

        <p>Assigned</p>
        <ul id="productSorterAssign" class="slideConnected">
        <?php
        foreach($assigned AS $individual_product) :
        ?>
        <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
            <h3><?php echo $individual_product->post_title; ?></h3>
        </li>
        <?php
            endforeach;
        ?>
        </ul>

        <p>Unassigned</p>
        <ul id="productSorterUnAssign" class="slideConnected">
        <?php 
        foreach($unassigned AS $individual_product) :
        ?>
        <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
            <h3><?php echo $individual_product->post_title; ?></h3>
        </li>
        <?php
            endforeach;
        ?>
        </ul>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
        if (jQuery('#productSorterAssign').length != 0) {
            jQuery('#productSorterAssign, #productSorterUnAssign').sortable({
                "connectWith" : '.slideConnected',
                "receive" : function ( event, ui ) {
                    if(ui.item.hasClass("dummy"))
                        ui.sender.sortable("cancel");
                },
                "start" : function( event, ui ) {
                },
                "stop" : function( event, ui ) {
                    newOrderList = [];
                    jQuery('#productSorterAssign li').not(".dummy").each(function(i){

                        newOrderList.push( jQuery(this).attr("id").substr(7) );

                    });
                    jQuery('#assignedProducts').val(newOrderList.join());
                }
            }).disableSelection();

            jQuery('#productSorterAssign').on('sortstop',function(event, ui) {
                console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
            });
            jQuery('#productSorterUnassign').on('sortstop',function(event, ui) {
                console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
            });
        }
        });
    </script>
<!-- product sorter END -->

<?php $mb->the_field('closing_head'); ?>
<div class="customField mediumtext">
    <label for="<?php $mb->the_name(); ?>">Closing Heading</label>
    <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
</div>

<div class="customField wpeditor">
    <p>Closing copy</p>
    <?php
    // insert a wpeditor into a wpalchemy template
    $mb->the_field('closing_m_copy');
    $settings = array(
        'textarea_name' => $mb->get_the_name(),
        'editor_class'  => 'cm_copy_wpeditor',
        'textarea_rows' => 6
    );
    wp_editor( html_entity_decode($mb->get_the_value()), 'cmcopy', $settings );
?>
</div>

<?php $mb->the_field('closing_l_head'); ?>
<div class="customField mediumtext">
    <label for="<?php $mb->the_name(); ?>">Closing left column heading</label>
    <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
</div>

<div class="customField wpeditor">
    <p>Closing left column copy</p>
    <?php
    // insert a wpeditor into a wpalchemy template
    $mb->the_field('closing_l_copy');
    $settings = array(
        'textarea_name' => $mb->get_the_name(),
        'editor_class'  => 'cl_copy_wpeditor',
        'textarea_rows' => 6
    );
    wp_editor( html_entity_decode($mb->get_the_value()), 'clcopy', $settings );
?>
</div>

<?php $mb->the_field('closing_l_link'); ?>
<div class="customField mediumtext">
    <label for="<?php $mb->the_name(); ?>">Closing left column button link</label>
    <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
</div>

<?php $mb->the_field('closing_l_button'); ?>
<div class="customField mediumtext">
    <label for="<?php $mb->the_name(); ?>">Closing left column button text</label>
    <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
</div>

<?php $mb->the_field('closing_r_head'); ?>
<div class="customField mediumtext">
    <label for="<?php $mb->the_name(); ?>">Closing right column heading</label>
    <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
</div>

<div class="customField wpeditor">
    <p>Closing right column copy</p>
    <?php
    // insert a wpeditor into a wpalchemy template
    $mb->the_field('closing_r_copy');
    $settings = array(
        'textarea_name' => $mb->get_the_name(),
        'editor_class'  => 'cr_copy_wpeditor',
        'textarea_rows' => 6
    );
    wp_editor( html_entity_decode($mb->get_the_value()), 'crcopy', $settings );
?>
</div>

<?php $mb->the_field('closing_r_link'); ?>
<div class="customField mediumtext">
    <label for="<?php $mb->the_name(); ?>">Closing right column button link</label>
    <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
</div>

<?php $mb->the_field('closing_r_button'); ?>
<div class="customField mediumtext">
    <label for="<?php $mb->the_name(); ?>">Closing right column button text</label>
    <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
</div>
