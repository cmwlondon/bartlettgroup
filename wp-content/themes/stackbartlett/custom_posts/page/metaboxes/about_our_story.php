<?php 
/*
about: The Bartlett Story

aosImg1 -> $values['aos_img1'] -> aos_img1
aosHeading1 -> $values['aos_heading1'] -> aos_heading1
aosCopy1 -> $values['aos_copy1'] -> aos_copy1
aosHeading2 -> $values['aos_heading2'] -> aos_heading2
aosCopy2l -> $values['aos_copy2l'] -> aos_copy2l
aosCopy2r -> $values['aos_copy2r'] -> aos_copy2r
aosImg2 -> $values['aos_img2'] -> aos_img2
aosQuote -> $values['aos_quote'] -> aos_quote

*/
?>

<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label for="awardImage">Image (normal)</label>
		<input type="hidden" id="ourstory1_normal" name="ourstory1_normal" value="<?php echo $values['ourstory1_normal']; ?>">
		<a href="" class="ourstory1_normalPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['ourstory1_normal']; ?>" <?php if ($values['ourstory1_normal'] == '' || $values['ourstory1_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['ourstory1_normal'] == '' || $values['ourstory1_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ourstory1_normal']; ?></p>
	</div>

	<div class="customField imageSelector multiImage">
		<label for="awardImage">Image (retina)</label>
		<input type="hidden" id="ourstory1_retina" name="ourstory1_retina" value="<?php echo $values['ourstory1_retina']; ?>">
		<a href="" class="ourstory1_retinaPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['ourstory1_retina']; ?>" <?php if ($values['ourstory1_retina'] == '' || $values['ourstory1_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['ourstory1_retina'] == '' || $values['ourstory1_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ourstory1_retina']; ?></p>
	</div>
</div>

<div class="customField mediumtext">
	<label for="aosHeading1">Heading 1</label>
	<input type="text" id="aosHeading1" name="aosHeading1" value="<?php echo $values['aos_heading1']; ?>">
</div>

<div class="customField wpeditor">
	<p>Intro</p>
	<?php
		$settings = array(
			'textarea_name' => 'aosCopy1',
			'editor_class'  => 'aos_copy1_wpeditor',
			'textarea_rows' => 6
		);
		wp_editor( html_entity_decode($values['aos_copy1']), 'aos_copy1', $settings );
	?>
</div>

<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label for="awardImage">Quote Image (normal)</label>
		<input type="hidden" id="ourstory2_normal" name="ourstory2_normal" value="<?php echo $values['ourstory2_normal']; ?>">
		<a href="" class="ourstory2_normalPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['ourstory2_normal']; ?>" <?php if ($values['ourstory2_normal'] == '' || $values['ourstory2_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['ourstory2_normal'] == '' || $values['ourstory2_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ourstory2_normal']; ?></p>
	</div>

	<div class="customField imageSelector multiImage">
		<label for="awardImage">Quote Image (retina)</label>
		<input type="hidden" id="ourstory2_retina" name="ourstory2_retina" value="<?php echo $values['ourstory2_retina']; ?>">
		<a href="" class="ourstory2_retinaPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['ourstory2_retina']; ?>" <?php if ($values['ourstory2_retina'] == '' || $values['ourstory2_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['ourstory2_retina'] == '' || $values['ourstory2_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['ourstory2_retina']; ?></p>
	</div>
</div>

<div class="customField wpeditor">
	<p>Right column</p>
	<?php
		$settings = array(
			'textarea_name' => 'aosCopy2r',
			'editor_class'  => 'aos_copy2r_wpeditor',
			'textarea_rows' => 6
		);
		wp_editor( html_entity_decode($values['aos_copy2r']), 'aos_copy2r', $settings );
	?>
</div>

<script type="text/javascript">
var image3Pickers = [];

jQuery(document).ready(function(){
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.ourstory1_normalPicker')
    }));
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.ourstory1_retinaPicker')
    }));
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.ourstory2_normalPicker')
    }));
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.ourstory2_retinaPicker')
    }));

});
</script>
