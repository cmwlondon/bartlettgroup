<?php
?>
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label for="awardImage">Banner image (normal)</label>
		<input type="hidden" id="banner_normal" name="banner_normal" value="<?php echo $values['banner_normal']; ?>">
		<a href="" class="banner_normalPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['banner_normal']; ?>" <?php if ($values['banner_normal'] == '' || $values['banner_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['banner_normal'] == '' || $values['banner_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['banner_normal']; ?></p>
	</div>

	<div class="customField imageSelector multiImage">
		<label for="awardImage">Banner image (retina)</label>
		<input type="hidden" id="banner_retina" name="banner_retina" value="<?php echo $values['banner_retina']; ?>">
		<a href="" class="banner_retinaPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['banner_retina']; ?>" <?php if ($values['banner_retina'] == '' || $values['banner_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['banner_retina'] == '' || $values['banner_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['banner_retina']; ?></p>
	</div>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.banner_normalPicker')
    }));
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.banner_retinaPicker')
    }));
});
</script>

<div class="customField mediumtext">
	<label for="contactBannerheading">Banner Heading</label>
	<input type="text" id="contactBannerheading" name="contactBannerheading" value="<?php echo $values['cb_heading']; ?>">
</div>
