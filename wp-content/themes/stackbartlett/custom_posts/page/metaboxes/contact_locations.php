<?php
// $offices = get_offices_in_order( explode ( ',', $values['locations']) );

$syncedLocations = synchronise_offices(explode(',', $values['locations']), $values['postid']);
$assigned = (count($syncedLocations['assigned']) > 0 ) ? get_offices_in_order($syncedLocations['assigned']) : [];
$unassigned = (count($syncedLocations['unassigned']) > 0 ) ? get_offices_in_order($syncedLocations['unassigned']) : [];
$theme_root = get_template_directory_uri();
$dummy1State = (count($assigned) > 0) ? ' hidden' : '';
$dummy2State = (count($unassigned) > 0) ? ' hidden' : '';
?>
<div class="customField itemSorter officeSorter">
    <input type="hidden" id="officeOrder" name="officeOrder" value="<?php echo implode(',', $syncedLocations['assigned']); ?>">
    <p>Assigned</p>
    <ul id="officeSorterAssign"  class="slideConnected">
        <li class="clearfix dummy dummy<?php echo $dummy1State; ?>" id="officeDummy1">
            <div class="portrait"><img alt="-" src="<?php echo $theme_root; ?>/img/dummy.png"></div>
            <h3>-</h3>
        </li>

        <?php
        foreach($assigned AS $key => $office) :
            $office_id = $office->ID;
            $metadata = $metadata = get_post_meta($office_id);
            $office_title = $office->post_title;
            $map_image = $metadata['o_map'][0];
        ?>
        <li class="clearfix" id="<?php echo "office$office_id"; ?>">
            <div class="map"><img alt="" src="<?php echo $map_image; ?>"></div>
            <h3><?php echo $office_title; ?><h3>
        </li>
        <?php
            endforeach;
        ?>
    </ul>

    <p>Unassigned</p>
    <ul id="officeSorterUnassign" class="slideConnected">
        <li class="clearfix dummy dummy<?php echo $dummy2State; ?>" id="officeDummy2">
            <div class="portrait"><img alt="-" src="<?php echo $theme_root; ?>/img/dummy.png"></div>
            <h3>-</h3>
        </li>
        <?php
        foreach($unassigned AS $key => $office) :
            $office_id = $office->ID;
            $metadata = $metadata = get_post_meta($office_id);
            $office_title = $office->post_title;
            $map_image = $metadata['o_map'][0];
        ?>
        <li class="clearfix" id="<?php echo "office$office_id"; ?>">
            <div class="map"><img alt="" src="<?php echo $map_image; ?>"></div>
            <h3><?php echo $office_title; ?><h3>
        </li>
        <?php
            endforeach;
        ?>
    </ul>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // make people order items sortable
        if (jQuery('#officeSorterAssign').length != 0) {

            jQuery('#officeSorterAssign,#officeSorterUnassign').sortable({
                "connectWith" : '.slideConnected',
                "receive" : function ( event, ui ) {
                    if(ui.item.hasClass("dummy"))
                        ui.sender.sortable("cancel");
                },
                "start" : function( event, ui ) {
                },
                "stop" : function( event, ui ) {
                    var newOrderList = [];
                    jQuery('#officeSorterAssign li').not(".dummy").each(function(i){
                        newOrderList.push( jQuery(this).attr("id").substr(6) );
                    });
                    console.log("stop1 %s", newOrderList.join());
                    jQuery('#officeOrder').val(newOrderList.join());
                }
            }).disableSelection();

            jQuery('#officeSorterAssign').on('sortstop',function(event, ui) {
                console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
            });
            jQuery('#officeSorterUnassign').on('sortstop',function(event, ui) {
                console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
            });
        }
    });
</script>
