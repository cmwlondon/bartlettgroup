<?php
/*
business product 2nd level metabox template

page template: business_product2.php

fields:
    title
    intro_head
    intro_copy
    main_copy
    sidebar_image
    document downloads
    misc links
    key contact
    case study
    related products
*/
?>
            <?php
            $metabox->the_field('intro_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Intro Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>Intro Copy</p>
                <?php
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('intro_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_intro_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'introcopy', $settings );
            ?>
            </div>

            <!-- commercial insurance products START -->
            <?php
            $metabox->the_field('commin_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Commercial Insurance Products section Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>Commercial Insurance Products section Copy</p>
                <?php
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('commin_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_commin_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'commincopy', $settings );
            ?>
            </div>

            <!-- commercial insurance products sorter -->
            <?php
            $mb->the_field('ci_items');
            $tags = array('commercial','insurance');
            $items = synchronise_business_products(explode(',',$mb->get_the_value()), $tags, $post->id);
            $assigned = (count($items['assigned']) > 0) ? get_business_products_in_order($items['assigned']) : []; // 
            $unassigned = (count($items['unassigned']) > 0) ? get_business_products_in_order($items['unassigned']) : [];
            ?>
            <!-- assigned:  <?php echo implode(',',$items['assigned']); ?> unassigned: <?php echo implode(',',$items['unassigned']); ?>-->
            <div class="customField itemSorter productSorter">
                <p class="head">Commercial Insurance Products</p>
                <input type="hidden" id="assignedCIProducts" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">

                <p>Assigned</p>
                <ul id="ciProductSorterAssign" class="slideciConnected">
                <?php
                foreach($assigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>

                <p>Unassigned</p>
                <ul id="ciProductSorterUnAssign" class="slideciConnected">
                <?php 
                foreach($unassigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>
            </div>

            <!-- commercial insurance products END -->

            <!-- speciality insurance products START -->
            <?php
            $metabox->the_field('specin_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Speciality Insurance Products section Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>Speciality Insurance Products section Copy</p>
                <?php
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('specin_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_commin_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'specincopy', $settings );
            ?>
            </div>
            <!-- speciality insurance products sorter -->
            <?php
            $mb->the_field('si_items');
            $tags = array('speciality','insurance');
            $items = synchronise_business_products(explode(',',$mb->get_the_value()), $tags, $post->id);
            $assigned = (count($items['assigned']) > 0) ? get_business_products_in_order($items['assigned']) : []; // 
            $unassigned = (count($items['unassigned']) > 0) ? get_business_products_in_order($items['unassigned']) : [];
            ?>
            <!-- assigned:  <?php echo implode(',',$items['assigned']); ?> unassigned: <?php echo implode(',',$items['unassigned']); ?>-->
            <div class="customField itemSorter productSorter">
                <p class="head">Speciality Insurance Products</p>
                <input type="hidden" id="assignedSIProducts" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">

                <p>Assigned</p>
                <ul id="siProductSorterAssign" class="slidesiConnected">
                <?php
                foreach($assigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>

                <p>Unassigned</p>
                <ul id="siProductSorterUnAssign" class="slidesiConnected">
                <?php 
                foreach($unassigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>
            </div>

            <!-- speciality insurance products END -->

            <script type="text/javascript">
                jQuery(document).ready(function(){
                    if (jQuery('#ciProductSorterAssign').length != 0) {
                        jQuery('#ciProductSorterAssign, #ciProductSorterUnAssign').sortable({
                            "connectWith" : '.slideciConnected',
                            "receive" : function ( event, ui ) {
                                if(ui.item.hasClass("dummy"))
                                    ui.sender.sortable("cancel");
                            },
                            "start" : function( event, ui ) {
                            },
                            "stop" : function( event, ui ) {
                                newOrderList = [];
                                jQuery('#ciProductSorterAssign li').not(".dummy").each(function(i){

                                    newOrderList.push( jQuery(this).attr("id").substr(7) );

                                });
                                jQuery('#assignedCIProducts').val(newOrderList.join());
                            }
                        }).disableSelection();

                        jQuery('#ciProductSorterAssign').on('sortstop',function(event, ui) {
                            console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
                        });
                        jQuery('#ciProductSorterUnAssign').on('sortstop',function(event, ui) {
                            console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
                        });
                    }

                    if (jQuery('#siProductSorterAssign').length != 0) {
                        jQuery('#siProductSorterAssign, #siProductSorterUnAssign').sortable({
                            "connectWith" : '.slidesiConnected',
                            "receive" : function ( event, ui ) {
                                if(ui.item.hasClass("dummy"))
                                    ui.sender.sortable("cancel");
                            },
                            "start" : function( event, ui ) {
                            },
                            "stop" : function( event, ui ) {
                                newOrderList = [];
                                jQuery('#siProductSorterAssign li').not(".dummy").each(function(i){

                                    newOrderList.push( jQuery(this).attr("id").substr(7) );

                                });
                                jQuery('#assignedSIProducts').val(newOrderList.join());
                            }
                        }).disableSelection();

                        jQuery('#siProductSorterAssign').on('sortstop',function(event, ui) {
                            console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
                        });
                        jQuery('#siProductSorterUnAssign').on('sortstop',function(event, ui) {
                            console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
                        });
                    }
                });
            </script>

