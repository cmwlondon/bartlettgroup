<?php
/*
business product product category: wealth management metabox template

page template: business_product3.php

fields:
    title
    intro_head
    intro_copy
*/
    $posttags = get_the_tags();
    $taglist = [];
    if ($posttags) {
        foreach($posttags AS $tag) {
            $taglist[] = $tag->name;
        }
    }

            
?>
            <?php
            $metabox->the_field('intro_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Intro Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>Intro Copy</p>
                <?php
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('intro_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_intro_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'introcopy', $settings );
            ?>
            </div>

            <?php
            $mb->the_field('wm_items');

            // $taglist = array('wealth management');
            $items = synchronise_business_products(explode(',',$mb->get_the_value()), $taglist, $post->id);
            $assigned = (count($items['assigned']) > 0) ? get_business_products_in_order($items['assigned']) : []; // 
            $unassigned = (count($items['unassigned']) > 0) ? get_business_products_in_order($items['unassigned']) : [];
            ?>
            <div class="customField itemSorter productSorter">
                <p class="head">Wealth Management Products</p>
                <input type="hidden" id="assignedWMProducts" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">

                <p>Assigned</p>
                <ul id="wmProductSorterAssign" class="slideciConnected">
                <?php
                foreach($assigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>

                <p>Unassigned</pp>
                <ul id="wmProductSorterUnAssign" class="slideciConnected">
                <?php 
                foreach($unassigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function(){
                    if (jQuery('#wmProductSorterAssign').length != 0) {
                        jQuery('#wmProductSorterAssign, #wmProductSorterUnAssign').sortable({
                            "connectWith" : '.slideciConnected',
                            "receive" : function ( event, ui ) {
                                if(ui.item.hasClass("dummy"))
                                    ui.sender.sortable("cancel");
                            },
                            "start" : function( event, ui ) {
                            },
                            "stop" : function( event, ui ) {
                                newOrderList = [];
                                jQuery('#wmProductSorterAssign li').not(".dummy").each(function(i){

                                    newOrderList.push( jQuery(this).attr("id").substr(7) );

                                });
                                jQuery('#assignedWMProducts').val(newOrderList.join());
                            }
                        }).disableSelection();

                        jQuery('#wmProductSorterAssign').on('sortstop',function(event, ui) {
                            console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
                        });
                        jQuery('#wmProductSorterUnAssign').on('sortstop',function(event, ui) {
                            console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
                        });
                    }

                });
            </script>

