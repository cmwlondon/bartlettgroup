<?php
/*
about: managment team sorter
$values['about_team'] -> #assignedTeam -> about_team_order
$values['at_title'] -> #aboutTeamTitle -> about_team_title
$values['at_copy'] -> #aboutTeamCopy -> about_team_copy
*/

$syncedTeam = synchronise_team(explode(',', $values['about_team']), $values['postid']);
$assigned = (count($syncedTeam['assigned']) > 0 ) ? get_people_in_order($syncedTeam['assigned']) : [];
$unassigned = (count($syncedTeam['unassigned']) > 0 ) ? get_people_in_order($syncedTeam['unassigned']) : [];
$theme_root = get_template_directory_uri();
$dummy1State = (count($assigned) > 0) ? ' hidden' : '';
$dummy2State = (count($unassigned) > 0) ? ' hidden' : '';
?>

<div class="customField mediumtext">
    <label for="aboutTeamTitle">Heading</label>
    <input type="text" id="aboutTeamTitle" name="aboutTeamTitle" value="<?php echo $values['at_title']; ?>">
</div>

<div class="customField wpeditor">
    <p>Copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'aboutTeamCopy',
            'editor_class'  => 'at_copy_wpeditor',
            'textarea_rows' => 6
        );
        wp_editor( $values['at_copy'], 'at_copy', $settings );
    ?>
</div>

<div class="customField itemSorter teamleaderSorter">
    <input type="hidden" id="assignedTeam" name="assignedTeam" value="<?php echo implode(',', $syncedTeam['assigned']); ?>">

    <p>Assigned</p>
    <ul id="teamSorterAssign" class="slideConnected">
        <li class="clearfix dummy<?php echo $dummy1State; ?>" id="personDummy1">
            <div class="portrait"><img alt="-" src="<?php echo $theme_root; ?>/img/dummy.png"></div>
            <h3>-</h3>
            <h4>-</h4>
            <h5>-</h5>
        </li>
    <?php
    foreach($assigned AS $key => $person) :
        $person_id = $person->ID;
        $metadata = get_post_meta($person_id);
        $forename = $metadata['p_forename'][0];
        $surname = $metadata['p_surname'][0];
        $role = $metadata['p_role'][0];
        $section = $metadata['p_section'][0];
        $portrait = $metadata['person_normal'][0];
        $bio = $metadata['p_bio'][0];
        
        $fullName = $forename . ' ' . $surname;
    ?>
    <li class="clearfix" id="<?php echo "person$person_id"; ?>">
        <div class="portrait"><img alt="<?php echo $fullName; ?>" src="<?php echo $portrait; ?>"></div>
        <h3><?php echo $fullName; ?></h3>
        <h4><?php echo $role; ?></h4>
        <h5><?php echo $section; ?></h5>
    </li>
    <?php
        endforeach;
    ?>
    </ul>

    <p>Unassigned</p>
    <ul id="teamSorterUnassign" class="slideConnected">
        <li class="clearfix dummy<?php echo $dummy2State; ?>" id="personDummy2">
            <div class="portrait"><img alt="-" src="<?php echo $theme_root; ?>/img/dummy.png"></div>
            <h3>-</h3>
            <h4>-</h4>
            <h5>-</h5>
        </li>
    <?php
    foreach($unassigned AS $key => $person) :
        $person_id = $person->ID;
        $metadata = $metadata = get_post_meta($person_id);
        $forename = $metadata['p_forename'][0];
        $surname = $metadata['p_surname'][0];
        $role = $metadata['p_role'][0];
        $section = $metadata['p_section'][0];
        $portrait = $metadata['person_normal'][0];
        $fullName = $forename . ' ' . $surname;
    ?>
    <li class="clearfix" id="<?php echo "person$person_id"; ?>">
        <div class="portrait"><img alt="<?php echo $fullName; ?>" src="<?php echo $portrait; ?>"></div>
        <h3><?php echo $fullName; ?></h3>
        <h4><?php echo $role; ?></h4>
        <h5><?php echo $section; ?></h5>
    </li>
    <?php
        endforeach;
    ?>
    </ul>
</div>

<div class="customField mediumtext">
    <label for="aboutTeamTitle">'Contact us' link</label>
    <input type="text" id="aboutContactUs" name="aboutContactUs" value="<?php echo $values['about_contact_us']; ?>">
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // make people order items sortable
        if (jQuery('#teamSorterAssign').length != 0) {
            jQuery('#teamSorterAssign, #teamSorterUnassign').sortable({
                "connectWith" : '.slideConnected',
                "receive" : function ( event, ui ) {
                    if(ui.item.hasClass("dummy"))
                        ui.sender.sortable("cancel");
                },
                "start" : function( event, ui ) {
                },
                "stop" : function( event, ui ) {
                    newOrderList = [];
                    jQuery('#teamSorterAssign li').not(".dummy").each(function(i){
                        newOrderList.push( jQuery(this).attr("id").substr(6) );
                    });
                    jQuery('#assignedTeam').val(newOrderList.join());
                }
            }).disableSelection();

            /*
            jQuery('#teamSorterAssign').on('sortstop',function(event, ui) {
                console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
            });
            jQuery('#teamSorterUnassign').on('sortstop',function(event, ui) {
                console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
            });
            */
        }

    });
</script>

