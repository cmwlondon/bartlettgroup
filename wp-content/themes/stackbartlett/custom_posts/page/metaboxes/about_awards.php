<?php
$synced_awards = synchronise_awards( explode(',', $values['awards_order']), $pageid, $metakey );
$assigned = (count($synced_awards['assigned']) > 0) ? get_awards_in_order($synced_awards['assigned']) : [];
$unassigned = (count($synced_awards['unassigned']) > 0) ? get_awards_in_order($synced_awards['unassigned']) : [];

?>
<div class="customField mediumtext">
	<label for="aboutBannerheading">Title</label>
	<input type="text" id="awardsTitle" name="awardsTitle" value="<?php echo $values['awards_title']; ?>">
</div>

<div class="customField itemSorter awardSorter">
    <input type="hidden" id="awardsOrder" name="awardsOrder" value="<?php echo implode(',', $synced_awards['assigned']); ?>">

    <p>Assigned</p>
    <ul id="awardSorterAssign" class="slideConnected">
    <?php
    foreach($assigned AS $key => $award) :
        $award_id = $award->ID;
        $metadata = get_post_meta($award_id );
	    $attachment = wp_get_attachment_image_src($metadata['pic_id'][0],'full'); 

    ?>
    <li class="clearfix" id="<?php echo "award$award_id"; ?>"><div class="pic"><img alt="<?php echo $award->post_title; ?>" src="<?php echo $attachment[0]; ?>"></div><h3><?php echo $award->post_title; ?></h3></li>
    <?php
        endforeach;
    ?>
    </ul>

    <p>Unassigned</p>
    <ul id="awardSorterUnassign" class="slideConnected">
    <?php
    foreach($unassigned AS $key => $award) :
        $award_id = $award->ID;
        $metadata = get_post_meta($award_id );
	    $attachment = wp_get_attachment_image_src($metadata['pic_id'][0],'full'); 

    ?>
    <li class="clearfix" id="<?php echo "award$award_id"; ?>"><div class="pic"><img alt="<?php echo $award->post_title; ?>" src="<?php echo $attachment[0]; ?>"></div><h3><?php echo $award->post_title; ?></h3></li>
    <?php
        endforeach;
    ?>
    </ul>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // make people order items sortable
        if (jQuery('#awardSorterAssign').length != 0) {
            jQuery('#awardSorterAssign, #awardSorterUnassign').sortable({
                "connectWith" : '.slideConnected',
                "receive" : function ( event, ui ) {
                    if(ui.item.hasClass("dummy"))
                        ui.sender.sortable("cancel");
                },
                "start" : function( event, ui ) {
                },
                "stop" : function( event, ui ) {
                    newOrderList = [];
                    jQuery('#awardSorterAssign li').not(".dummy").each(function(i){
                        newOrderList.push( jQuery(this).attr("id").substr(5) );
                    });
                    jQuery('#awardsOrder').val(newOrderList.join());
                }
            }).disableSelection();

            /*
            jQuery('#awardSorterAssign').on('sortstop',function(event, ui) {
                console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
            });
            jQuery('#awardSorterUnassign').on('sortstop',function(event, ui) {
                console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
            });
			*/
        }

    });
</script>

