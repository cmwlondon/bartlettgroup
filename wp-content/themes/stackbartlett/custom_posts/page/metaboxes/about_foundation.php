<?php
    $syncedTeam = synchronise_foundations(explode(',', $values['af_order']), $values['postid'], 'about_foundations_order');
    $assigned = (count($syncedTeam['assigned']) > 0 ) ? get_foundations_by_order($syncedTeam['assigned']) : [];
    $unassigned = (count($syncedTeam['unassigned']) > 0 ) ? get_foundations_by_order($syncedTeam['unassigned']) : [];
    $theme_root = get_template_directory_uri();
    $dummy1State = (count($assigned) > 0) ? ' hidden' : '';
    $dummy2State = (count($unassigned) > 0) ? ' hidden' : '';
?>

<div class="customField itemSorter teamleaderSorter">
    <div class="customField longtext">
        <label for="aboutFoundationTitle">Heading</label>
        <input type="text" id="aboutFoundationTitle" name="aboutFoundationTitle" value="<?php echo $values['af_title']; ?>">
    </div>

    <div class="customField wpeditor">
        <p>Copy</p>
        <?php
            $settings = array(
                'textarea_name' => 'aboutFoundationCopy',
                'editor_class'  => 'afc_wpeditor',
                'textarea_rows' => 6
            );
            wp_editor( html_entity_decode($values['af_copy']), 'afc', $settings );
        ?>
    </div>

    <div class="customField wpeditor">
        <p>Caption Copy</p>
        <?php
            $settings = array(
                'textarea_name' => 'aboutFoundationCaption',
                'editor_class'  => 'lc_wpeditor',
                'textarea_rows' => 6
            );
            wp_editor( html_entity_decode($values['af_caption']), 'lc', $settings );
        ?>
    </div>

    <p>Assigned</p>
    <input type="hidden" id="assignedFoundations" name="assignedFoundations" value="<?php echo implode(',', $syncedTeam['assigned']); ?>">
    <ul id="foundationSorterAssign" class="slideConnected">
        <li class="clearfix dummy<?php echo $dummy1State; ?>" id="personDummy1">
            <div class="portrait"><img alt="-" src="<?php echo $theme_root; ?>/img/dummy.png"></div>
            <h3>-</h3>
            <h4>-</h4>
            <h5>-</h5>
        </li>

    <?php
    foreach($assigned AS $key => $foundation) :
        $metadata = get_post_meta($foundation->ID);
        $title = $metadata['foundation_title'][0];
        $image = $metadata['foundation_image'][0];
    ?>
    <li class="clearfix" id="item<?php echo $foundation->ID; ?>">
        <div class="portrait"><img alt="<?php echo $title; ?>" src="<?php echo $image; ?>"></div>
        <h3><?php echo $title; ?></h3>
    </li>
    <?php
        endforeach;
    ?>

    </ul>

    <p>Unassigned</p>
    <ul id="foundationSorterUnassign" class="slideConnected">
        <li class="clearfix dummy<?php echo $dummy2State; ?>" id="personDummy2">
            <div class="portrait"><img alt="-" src="<?php echo $theme_root; ?>/img/dummy.png"></div>
            <h3>-</h3>
            <h4>-</h4>
            <h5>-</h5>
        </li>

    <?php
    foreach($unassigned AS $key => $foundation) :
        $metadata = get_post_meta($foundation->ID);
        $title = $metadata['foundation_title'][0];
        $image = $metadata['foundation_image'][0];
    ?>
    <li class="clearfix" id="item<?php echo $foundation->ID; ?>">
        <div class="portrait"><img alt="<?php echo $title; ?>" src="<?php echo $image; ?>"></div>
        <h3><?php echo $title; ?></h3>
    </li>
    <?php
        endforeach;
    ?>

    </ul>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // make people order items sortable
        if (jQuery('#foundationSorterAssign').length != 0) {
            jQuery('#foundationSorterAssign, #foundationSorterUnassign').sortable({
                "connectWith" : '.slideConnected',
                "receive" : function ( event, ui ) {
                    if(ui.item.hasClass("dummy"))
                        ui.sender.sortable("cancel");
                },
                "start" : function( event, ui ) {
                },
                "stop" : function( event, ui ) {
                    newOrderList = [];
                    jQuery('#foundationSorterAssign li').not(".dummy").each(function(i){
                        newOrderList.push( jQuery(this).attr("id").substr(4) );
                    });
                    jQuery('#assignedFoundations').val(newOrderList.join());
                }
            }).disableSelection();

            /*
            jQuery('#foundationSorterAssign').on('sortstop',function(event, ui) {
                console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
            });
            jQuery('#foundationSorterUnassign').on('sortstop',function(event, ui) {
                console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
            });
            */
        }

    });
</script>

