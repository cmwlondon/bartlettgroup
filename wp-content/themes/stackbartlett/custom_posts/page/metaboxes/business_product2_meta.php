<?php
/*
business product 2nd level metabox template

page template: business_product2.php

fields:
    title
    intro_head
    intro_copy
    product[
        main_copy
        sidebar_image
        document downloads
        misc links
        key contact
        case study
        related products
    ]
*/
    $posttags = get_the_tags();
    $taglist = [];
    if ($posttags) {
        foreach($posttags AS $tag) {
            $taglist[] = $tag->name;
        }
    }
    $related_products = get_business_products($taglist);
?>
            <?php
            $metabox->the_field('intro_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Intro Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>Intro Copy</p>
                <?php
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('intro_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_intro_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'introcopy', $settings );
            ?>
            </div>


            <hr>
            <div class="customField selector">
            <?php
            $metabox->the_field('product');
            ?>
            <label for="<?php $metabox->the_name(); ?>">Set Product to display in this page</label>
            <select name="<?php $metabox->the_name(); ?>" id="<?php $metabox->the_name(); ?>">
                <option value="-999">Select Product to display</option>
            <?php
            foreach ($related_products AS $product) :
            ?>
                <option value="<?php echo $product->ID; ?>" <?php if ( $metabox->get_the_value() == $product->ID) { echo "selected"; } ?>><?php echo $product->post_title; ?></option>
            <?php endforeach; ?>
            </select>
            </div>
            
            <hr>

            <?php
            $metabox->the_field('related_product_image');
            $mainImage = $metabox->get_the_value();
            $mainImageName = $metabox->get_the_name();
            ?>
            <div class="customField imageSelector">
                <label for="<?php echo $mainImageTarget; ?>">Related Products image (displayed under 'Related products' on other pages of same product type)</label>
                <input type="hidden" id="<?php echo $mainImageTarget; ?>" name="<?php echo $mainImageName; ?>" value="<?php echo $mainImage; ?>">
                <a href="" class="relatedPicker action">Add/Update image</a>
                <img class="imageThumbnail" alt="" src="<?php echo $mainImage; ?>" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>>
                <p class="imagepath" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $mainImage; ?></p>
            </div>

    	<script type="text/javascript">
        var relatedPickers = [];
    	jQuery(document).ready(function(){
            relatedPickers.push(new MediaPanel({
                "trigger" : jQuery('.relatedPicker')
            }));
        });
		</script>

