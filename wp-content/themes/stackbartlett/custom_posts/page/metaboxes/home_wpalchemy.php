            <?php
            // bgh_banner_text
            $metabox->the_field('banner_text');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Banner text</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <?php
            // bgh_banner_button_label
            $metabox->the_field('banner_button_label');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Banner button label</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <?php
            // bgh_banner_button_label
            $metabox->the_field('banner_button_link');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Banner button link</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <?php
            // bgh_video_mp4
            $metabox->the_field('video_mp4');
            $mp4Video = $metabox->get_the_value();
            $mp4VideoName = $metabox->get_the_name();
            ?>
            <div class="customField videoSelector">
                <label for="<?php echo $mp4VideoName; ?>">Landing page video</label>
                <p>MP4</p>
                <input class="id" type="hidden" id="<?php echo $mp4VideoName; ?>" name="<?php echo $mp4VideoName; ?>" value="<?php echo $mp4Video; ?>">

                <div class="vpWrapperFull"  <?php if ($mp4Video == '' || $mp4Video == '-') : ?>style="opacity:0;"<?php endif; ?>>
                    <video preload onloadeddata="videoLoaded(this);">
                        <?php if ($mp4Video !== '') : ?><source src="<?php echo $mp4Video; ?>" type="video/mp4"></source><?php endif; ?>
                        
                    </video>
                    <div class="videoIcons"><div>play</div></div>
                    <div class="timeline">
                        <div class="outer">
                            <div class="track"></div>
                            <div class="inner"></div>
                            <div class="marker"></div>
                        </div>
                        <p class="elapsed">00.00</p>
                        <p class="total">00.00</p>
                    </div>
                    <div class="controlLayer"></div>
                </div>
                <a href="" class="homeVideoPicker1 action add">Add/Update video</a>
                <a href="" class="homeVideoPicker1 action remove">remove video</a>

            </div>
            <?php
            // bgh_video_webm
            $metabox->the_field('video_webm');
            $webmVideo = $metabox->get_the_value();
            $webmVideoName = $metabox->get_the_name();
            ?>
            <div class="customField videoSelector">
                <p>webm: <span class="namespan"><?php echo $webmVideo; ?></span></p>
                <input class="id" type="hidden" id="<?php echo $webmVideoName; ?>" name="<?php echo $webmVideoName; ?>" value="<?php echo $webmVideo; ?>">
                <a href="" class="homeVideoPicker2 action add">Add/Update video</a>
                <a href="" class="homeVideoPicker2 action remove">remove video</a>
            </div>
            <?php
            // bgh_video_ogg
            $metabox->the_field('video_ogg');
            $oggVideo = $metabox->get_the_value();
            $oggVideoName = $metabox->get_the_name();

            ?>
            <div class="customField videoSelector">
                <p>OGG:  <span class="namespan"><?php echo $oggVideo; ?></span></p>
                <input class="id" type="hidden" id="<?php echo $oggVideoName; ?>" name="<?php echo $oggVideoName; ?>" value="<?php echo $oggVideo; ?>">
                <a href="" class="homeVideoPicker3 action add">Add/Update video</a>
                <a href="" class="homeVideoPicker3 action remove">remove video</a>
            </div>

            <?php
            // bgh_image
            $mb->the_field('videoposter');
            ?>
            <div class="customField imageSelector">
                <label for="<?php $mb->the_name(); ?>">video poster image</label>
                <input type="hidden" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                <a href="" class="posterTrigger action">Add/Update image</a>
                <img  class="imageThumbnail" alt="" src="<?php $mb->the_value(); ?>" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>>
                <p class="imagepath" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>><?php $mb->the_value(); ?></p>
            </div>

            <script type="text/javascript">
            var 

            </script>

            <script type="text/javascript">
            var videoPickers = [],
                posterImagePickers = [],
                fullPlayer;

            function videoLoaded(video) {
                // fullPlayer.loaded(video);
            }

            jQuery(document).ready(function(){
                fullPlayer = new VideoPlayer({
                    "videoWrapper" : jQuery('.vpWrapperFull'),
                    "videoTag" : jQuery('.vpWrapperFull video'),
                    "icon" : jQuery('.vpWrapperFull .videoIcons'),
                    "control" : jQuery('.vpWrapperFull .controlLayer'),
                    "timeline" : jQuery('.vpWrapperFull .timeline'),
                    "mode" : "full"
                });

                videoPickers.push(new VideoPicker({
                    "trigger" : jQuery('.homeVideoPicker1'),
                    "videoWidget" : fullPlayer,
                    "buildVideoTag" : true
                }));

                videoPickers.push(new VideoPicker({
                    "trigger" : jQuery('.homeVideoPicker2'),
                    "videoWidget" : fullPlayer,
                    "buildVideoTag" : false
                }));
                videoPickers.push(new VideoPicker({
                    "trigger" : jQuery('.homeVideoPicker3'),
                    "videoWidget" : fullPlayer,
                    "buildVideoTag" : false
                }));
                jQuery(document).ready(function(){
                    posterImagePickers.push(new MediaPanel({
                        "trigger" : jQuery('.posterTrigger')
                    }));
                });
            });
            </script>

            <?php
            // bgh_intro_head
            $metabox->the_field('intro_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Intro Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>Intro Copy</p>
                <?php
                // bgh_intro_copy
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('intro_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_intro_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'introcopy', $settings );
            ?>
            </div>


            <?php
            /*
            home page product list - EXPERIMENTAL
            START
            ------------------------------------------------------------------------------------------------------------------------------------------------
            */
            ?>


            <?php
            // bgh_individual_products_head
            $metabox->the_field('individual_products_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Individual Products Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>
            <?php
            // bgh_individual_order
            $productFilter = array('individual');
            $metabox->the_field('individual_order');
            $individual_products = synchronise_products( explode(',', $metabox->get_the_value()), $productFilter, $post->ID, 'bgh_individual_order' );
            $assigned = (count($individual_products['assigned']) > 0) ? get_products_by_order($individual_products['assigned']) : []; // 
            $unassigned = (count($individual_products['unassigned']) > 0) ? get_products_by_order($individual_products['unassigned']) : [];
            ?>
            <div class="customField itemSorter productSorter">
                <p>Individual products</p>
                <input type="hidden" id="assignedIndividialProducts" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">

                <p>Assigned</p>
                <ul id="individualSorterAssign" class="slideConnected">
                <?php
                foreach($assigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>

                <p>Unassigned</p>
                <ul id="individualSorterUnassign" class="slideConnected">
                <?php 
                foreach($unassigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>
            </div>


            <?php
            // bgh_business_products_head
            $metabox->the_field('business_products_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Business Products Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>
            <?php
            // bgh_business_order
            $productFilter = array('business');
            $metabox->the_field('business_order');
            $business_products = synchronise_products( explode(',', $metabox->get_the_value()), $productFilter, $post->ID, 'bgh_business_order' );
            $assigned = (count($business_products['assigned']) > 0) ? get_products_by_order($business_products['assigned']) : []; // 
            $unassigned = (count($business_products['unassigned']) > 0) ? get_products_by_order($business_products['unassigned']) : [];
            ?>
            <div class="customField itemSorter productSorter">
                <p>Business products</p>
                <input type="hidden" id="assignedBusinessProducts" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">

                <p>Assigned</p>
                <ul id="businessSorterAssign" class="slideConnected">
                <?php
                foreach($assigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>

                <p>Unassigned</p>
                <ul id="businessSorterUnassign" class="slideConnected">
                <?php 
                foreach($unassigned AS $individual_product) :
                ?>
                <li class="clearfix" id="<?php echo "product$individual_product->ID"; ?>">
                    <h3><?php echo $individual_product->post_title; ?></h3>
                </li>
                <?php
                    endforeach;
                ?>
                </ul>
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function(){
                    if (jQuery('#individualSorterAssign').length != 0) {
                        jQuery('#individualSorterAssign, #individualSorterUnassign').sortable({
                            "connectWith" : '.slideConnected',
                            "receive" : function ( event, ui ) {
                                if(ui.item.hasClass("dummy"))
                                    ui.sender.sortable("cancel");
                            },
                            "start" : function( event, ui ) {
                            },
                            "stop" : function( event, ui ) {
                                newOrderList = [];
                                jQuery('#individualSorterAssign li').not(".dummy").each(function(i){

                                    newOrderList.push( jQuery(this).attr("id").substr(7) );

                                });
                                jQuery('#assignedIndividialProducts').val(newOrderList.join());
                            }
                        }).disableSelection();

                        jQuery('#individualSorterAssign').on('sortstop',function(event, ui) {
                            console.log('individualSorterAssign stop');
                        });
                        jQuery('#individualSorterUnassign').on('sortstop',function(event, ui) {
                            console.log();
                        });
                    }

                    if (jQuery('#businessSorterAssign').length != 0) {
                        jQuery('#businessSorterAssign, #businessSorterUnassign').sortable({
                            "connectWith" : '.slideConnected',
                            "receive" : function ( event, ui ) {
                                if(ui.item.hasClass("dummy"))
                                    ui.sender.sortable("cancel");
                            },
                            "start" : function( event, ui ) {
                            },
                            "stop" : function( event, ui ) {
                                newOrderList = [];
                                jQuery('#businessSorterAssign li').not(".dummy").each(function(i){

                                    newOrderList.push( jQuery(this).attr("id").substr(7) );

                                });
                                jQuery('#assignedBusinessProducts').val(newOrderList.join());
                            }
                        }).disableSelection();

                        jQuery('#businessSorterAssign').on('sortstop',function(event, ui) {
                            console.log('businessSorterAssign stop');
                        });
                        jQuery('#businessSorterUnassign').on('sortstop',function(event, ui) {
                            console.log();
                        });
                    }
                });
            </script>
            
            <?php
            /*
            home page product list - EXPERIMENTAL
            END
            ------------------------------------------------------------------------------------------------------------------------------------------------
            */
            ?>
            <div class="clearfix multiImages">
                <?php
                // bgh_image
                $mb->the_field('mtt_normal');
                ?>
                <div class="customField imageSelector multiImage">
                    <label for="<?php $mb->the_name(); ?>">'Meet the team' background image (normal)</label>
                    <input type="hidden" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    <a href="" class="mttNormalTrigger action">Add/Update image</a>
                    <img  class="imageThumbnail" alt="" src="<?php $mb->the_value(); ?>" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>>
                    <p class="imagepath" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>><?php $mb->the_value(); ?></p>
                </div>
                <?php
                // bgh_image
                $mb->the_field('mtt_retina');
                ?>
                <div class="customField imageSelector multiImage">
                    <label for="<?php $mb->the_name(); ?>">'Meet the team' background image (retina)</label>
                    <input type="hidden" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    <a href="" class="mttRetinaTrigger action">Add/Update image</a>
                    <img  class="imageThumbnail" alt="" src="<?php $mb->the_value(); ?>" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>>
                    <p class="imagepath" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>><?php $mb->the_value(); ?></p>
                </div>
            </div>

            <script type="text/javascript">
            var mttImagePickers = [];

            jQuery(document).ready(function(){
                mttImagePickers.push(new MediaPanel({
                    "trigger" : jQuery('.mttNormalTrigger')
                }));
                mttImagePickers.push(new MediaPanel({
                    "trigger" : jQuery('.mttRetinaTrigger')
                }));
            });
            </script>

            <?php
            // bgh_right_head
            $metabox->the_field('right_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Secondary Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>Secondary Heading Copy</p>
                <?php
                // bgh_right_copy
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('right_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_intro_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'copy2', $settings );
            ?>
            </div>

            <?php
            // bgh_button_label
            $metabox->the_field('button_label');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Button label</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <?php
            // bgh_button_link
            $metabox->the_field('button_link');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Button link</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <?php
            // bgh_quotes
            $metabox->the_field('quotes');
            $synced_quotes = synchronise_quotes(explode(',', $metabox->get_the_value()), 'home', 'bgh_quotes', $post->ID);
            $assigned = (count($synced_quotes['assigned']) > 0) ? get_quotes_in_order($synced_quotes['assigned']) : [];
            $unassigned = (count($synced_quotes['unassigned']) > 0) ? get_quotes_in_order($synced_quotes['unassigned']) : [];
            ?>
<div class="customField itemSorter relatedProductSorter">
    <input type="hidden" id="quotesOrder" name="<?php $metabox->the_name(); ?>" value="<?php echo implode(',', $synced_quotes['assigned']); ?>">

    <p>Assigned</p>
    <ul id="quoteSorterAssign" class="slide2Connected">
    <?php
    foreach($assigned AS $quote) :
    ?>
    <li class="clearfix" id="<?php echo "quote$quote->ID"; ?>">
        <h3><?php echo $quote->post_title; ?></h3>
    </li>
    <?php
        endforeach;
    ?>
    </ul>

    <p>Unassigned</p>
    <ul id="quoteSorterUnAssign" class="slide2Connected">
    <?php 
    foreach($unassigned AS $quote) :
    ?>
    <li class="clearfix" id="<?php echo "quote$quote->ID"; ?>">
        <h3><?php echo $quote->post_title; ?></h3>
    </li>
    <?php
        endforeach;
    ?>
    </ul>
</div>

<script type="text/javascript">
var quotes_order = '<?php echo $values['quotes_order']; ?>';

jQuery(document).ready(function(){

    if (jQuery('#quoteSorterAssign').length != 0) {
        jQuery('#quoteSorterAssign, #quoteSorterUnAssign').sortable({
            "connectWith" : '.slide2Connected',
            "receive" : function ( event, ui ) {
                if(ui.item.hasClass("dummy"))
                    ui.sender.sortable("cancel");
            },
            "start" : function( event, ui ) {
            },
            "stop" : function( event, ui ) {
                newOrderList = [];
                jQuery('#quoteSorterAssign li').not(".dummy").each(function(i){
                    console.log(jQuery(this).attr("id").substr(5));
                    newOrderList.push( jQuery(this).attr("id").substr(5) );
                });
                jQuery('#quotesOrder').val(newOrderList.join());
            }
        }).disableSelection();
    }
});
</script>


            <?php
            // bgh_tail_head
            $metabox->the_field('tail_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">'Find out more' Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>'Find out more' Copy</p>
                <?php
                // bgh_tail_copy
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('tail_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_intro_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'copy3', $settings );
            ?>
            </div>

            <?php
            // bgh_button2_label
            $metabox->the_field('button2_label');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">'Find out more' Button label</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <?php
            // bgh_button2_link
            $metabox->the_field('button2_link');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">'Find out more' Button link</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>
