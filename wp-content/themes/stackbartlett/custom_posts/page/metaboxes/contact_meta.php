<?php
/*
contact us metabox template

page template: contact.php

fields:
[offices: office ]
[teamleaders: person ]
*/

?>
<?php
$metabox->the_field('locations');
$offices = get_offices_in_order( explode ( ',', $metabox->get_the_value()) );
?>
<div class="customField itemSorter officeSorter">
    <p class="head">offices</p>
    <input type="hidden" id="officeOrder" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
    <ul id="officeSorter">
        <?php
        $testlist = [];
        foreach($offices AS $key => $office) :
            $office_id = $office->ID;
            $metadata = $metadata = get_post_meta($office_id);
            $office_title = $office->post_title;
            $map_image = $metadata['o_map'][0];

            $testlist[] = $office_id;
        ?>
        <li class="clearfix" id="<?php echo "office$office_id"; ?>">
            <div class="map"><img alt="" src="<?php echo $map_image; ?>"></div>
            <h3><?php echo $office_title; ?><h3>
        </li>
        <?php
            endforeach;
        ?>
    </ul>
    <p><?php echo implode($testlist, ','); ?></p>
</div>

<?php
$teamleaders = get_people('contact');
$metabox->the_field('teamleaders');
?>
<div class="customField itemSorter teamleaderSorter">
    <p class="head">Teamleaders</p>
    <input type="hidden" id="assignedPeople" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
    <p>Assigned</p>
    <ul class="assigned">
    <?php
    foreach($teamleaders AS $key => $person) :
        $person_id = $person->ID;
        $metadata = $metadata = get_post_meta($person_id);
        $forename = $metadata['p_forename'][0];
        $surname = $metadata['p_surname'][0];
        $role = $metadata['p_role'][0];
        $section = $metadata['p_section'][0];
        $portrait = $metadata['p_portrait'][0];
        $fullName = $forename . ' ' . $surname;
    ?>
    <li class="clearfix" id="<?php echo "person$person_id"; ?>">
        <div class="portrait"><img alt="<?php echo $fullName; ?>" src="<?php echo $portrait; ?>"></div>
        <h3><?php echo $fullName; ?></h3>
        <h4><?php echo $role; ?></h4>
        <h5><?php echo $section; ?></h5>
    </li>
    <?php
        endforeach;
    ?>
    </ul>
    <p>Unassigned</p>
    <ul class="unassigned">
    </ul>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // make people order items sortable
        if (jQuery('#officeSorter').length != 0) {
            jQuery('#officeSorter').sortable({
                "stop" : function( event, ui ) {
                    newOrderList = [];
                    jQuery('#officeSorter li').each(function(i){
                        newOrderList.push( jQuery(this).attr("id").substr(6) );
                    });
                    console.log(newOrderList.join(','));
                    jQuery('#officeOrder').val(newOrderList.join());
                }
            });
        }
    });
</script>

