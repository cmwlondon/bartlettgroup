<?php
/*
business product 2nd level metabox template

page template: business_product2.php

fields:
    title
    intro_head
    intro_copy
    main_copy
    sidebar_image
    document downloads
    misc links
    key contact
    case study
    related products
*/
?>
            <?php
            $metabox->the_field('intro_head');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Intro Heading</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

            <div class="customField wpeditor">
                <p>Intro Copy</p>
                <?php
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('intro_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_intro_copy_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'introcopy', $settings );
            ?>
            </div>

            <div class="customField wpeditor">
                <p>Main Copy</p>
                <?php
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('main_copy');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'bp2_maincopy_wpeditor',
                    'textarea_rows' => 12
                );
                wp_editor( html_entity_decode($metabox->get_the_value()), 'maincopy', $settings );
            ?>
            </div>
            <hr>
            <?php
            $metabox->the_field('sidebar_image');
            $mainImage = $metabox->get_the_value();
            $mainImageName = $metabox->get_the_name();
            ?>
            <div class="customField imageSelector">
                <label for="<?php echo $mainImageTarget; ?>">Sidebar image</label>
                <input type="hidden" id="<?php echo $mainImageTarget; ?>" name="<?php echo $mainImageName; ?>" value="<?php echo $mainImage; ?>">
                <a href="" class="mediaTrigger_sidebar action">Add/Update image</a>
                <img class="imageThumbnail" alt="" src="<?php echo $mainImage; ?>" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>>
                <p class="imagepath" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $mainImage; ?></p>
            </div>

            <hr>
            <div class="customField selector">
            <?php
            $metabox->the_field('keycontact');
            $people_keycontact = get_all_people('keycontact');

            ?>
            <label for="<?php $metabox->the_name(); ?>">key contacts</label>
            <select name="<?php $metabox->the_name(); ?>" id="<?php $metabox->the_name(); ?>">
                <option value="-999">Select key contact</option>
            <?php
            foreach ($people_keycontact AS $person) :
                $metadata = get_post_meta($person->ID);

                $forename = $metadata['p_forename'][0];
                $surname = $metadata['p_surname'][0];
                $role = $metadata['p_role'][0];
            ?>
                <option value="<?php echo $person->ID; ?>" <?php if ( $metabox->get_the_value() == $person->ID) { echo "selected"; } ?>><?php echo $forename." ".$surname." - ".$role; ?></option>
            <?php endforeach; ?>
            </select>
            </div>
            
            <hr>

            <div class="multiFieldWrapper">
                <p class="head">Document downloads</p>
                <p>
                  <a href="#" title="Remove all links" class="dodelete-document_links button">Remove All</a>
                  <a href="#" title="Add new link" class="docopy-document_links button">Add Document</a>
                </p>
                <?php
                $options = array('length' => 1, 'limit' => 5);
                while($mb->have_fields_and_multi('document_links')) :
                    $mb->the_group_open();
                ?>

                <div class="multiFieldBox">
                    <?php $mb->the_field('text'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">Document link text</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                     <div class="customField docSelector">
                        <?php $mb->the_field('link'); ?>
                        <p>Document URL/Link</p>
                        <input type="hidden" class="link" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                        <p class="docLinkPath" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>><?php $mb->the_value() ?></p>
                        <a href="" class="docLinkTrigger action">Add/Update document</a>
                        <?php $mb->the_field('docdata'); ?>
                        <input type="hidden" class="data" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <a href="#" title="Remove link" class="dodelete button">Remove document</a>
                </div>

                <?php
                    $mb->the_group_close();
                endwhile;
                ?>
            </div>

            <hr>

            <div class="multiFieldWrapper">
                <p class="head">Misc links</p>
                <p>
                  <a href="#" title="Remove all links" class="dodelete-misc_links button">Remove All</a>
                  <a href="#" title="Add new link" class="docopy-misc_links button">Add Misc Link</a>
                </p>
                <?php
                $options = array('length' => 1, 'limit' => 5);
                while($mb->have_fields_and_multi('misc_links')) :
                    $mb->the_group_open();
                ?>

                <div class="multiFieldBox">
                    <?php $mb->the_field('text'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">link text</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <div class="customField mediumtext">
                        <?php $mb->the_field('link'); ?>
                        <p>Link URL</p>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>
                    <a href="#" title="Remove link" class="dodelete button">Remove link</a>
                </div>

                <?php
                    $mb->the_group_close();
                endwhile;
                ?>

            </div>


            <div class="multiFieldWrapper">
                <p class="head">Case studies</p>
                <p>
                  <a href="#" title="Remove all links" class="dodelete-casestudies button">Remove All</a>
                  <a href="#" title="Add new link" class="docopy-casestudies button">Add Case study</a>
                </p>
                <?php
                $options = array('length' => 1, 'limit' => 5);
                while($mb->have_fields_and_multi('casestudies')) :
                    $mb->the_group_open();
                ?>

                <div class="multiFieldBox">
                    <?php $mb->the_field('header1'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">header1</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <?php $mb->the_field('header2'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">header1</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <?php $mb->the_field('copy'); ?>
                    <div class="customField">
                        <label for="<?php $mb->the_name(); ?>">Copy</label>
                        <textarea id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" rows="8" cols="80"><?php $mb->the_value(); ?></textarea>
                    </div>

                    <?php $mb->the_field('source'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">Source</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <?php $mb->the_field('cs_image'); ?>
                    <div class="customField imageSelector">
                        <label for="<?php $mb->the_name(); ?>">image</label>
                        <input type="hidden" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                        <a href="" class="mediaTrigger action">Add/Update image</a>
                        <img  class="imageThumbnail" alt="" src="<?php $mb->the_value(); ?>" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>>
                        <p class="imagepath" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>><?php $mb->the_value(); ?></p>
                    </div>
                    <a href="#" title="Remove link" class="dodelete button">Remove Case study</a>

                </div>


                <?php
                    $mb->the_group_close();
                endwhile;
                ?>
            </div>

            <div class="multiFieldWrapper">
                <p class="head">Related products</p>
                <p>
                  <a href="#" title="Remove all links" class="dodelete-related button">Remove All</a>
                  <a href="#" title="Add new link" class="docopy-related button">Add Related product</a>
                </p>
                <?php
                $options = array('length' => 1, 'limit' => 5);
                while($mb->have_fields_and_multi('related')) :
                    $mb->the_group_open();
                ?>

                <div class="multiFieldBox">
                    <?php $mb->the_field('title'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">Title</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <?php $mb->the_field('image'); ?>
                    <div class="customField imageSelector">
                        <label for="<?php $mb->the_name(); ?>">image</label>
                        <input type="hidden" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                        <a href="" class="mediaTrigger action">Add/Update image</a>
                        <img  class="imageThumbnail" alt="" src="<?php $mb->the_value(); ?>" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>>
                        <p class="imagepath" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>><?php $mb->the_value(); ?></p>
                    </div>

                    <div class="customField mediumtext">
                        <?php $mb->the_field('link'); ?>
                        <p>Link URL</p>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>
                    <a href="#" title="Remove link" class="dodelete button">Remove related product</a>
                </div>

                <?php
                    $mb->the_group_close();
                endwhile;
                ?>

            </div>

    	<script type="text/javascript">
		var thisMediaPanel_sidebar,
            caseStudyImagePicPickers = [],
            documentPickers = [],
            relatedImagePicker = [];

		jQuery(document).ready(function(){

			thisMediaPanel_sidebar = new MediaPanel({
				"trigger" : jQuery('.mediaTrigger_sidebar')
			});

            documentPickers.push(new DocPicker({
                "trigger" : jQuery('.docLinkTrigger')
            }));

            documentPickers.push(new MediaPanel({
                "trigger" : jQuery('.mediaTrigger')
            }));
        });
		</script>

