<?php

/* Custom Meta Boxes */
/*
link metaboxes to pages based on page template or id
*/

/*
include_post_id - metabox is only used for admin page for specific page
*/

/*
$landing_page_posts = new WPAlchemy_MetaBox(array
(
'id'        => 'homeboxes',
'title'     => __('Displayed Items'),
'types'     => array('page'),
'context'   => 'normal',
'priority'  => 'high',
'mode'      => WPALCHEMY_MODE_EXTRACT,
'autosave'  => true,
'include_post_id' => 2582, // stories page #2582
'prefix'    => 'landing_page_',
'template'  => get_template_directory() . '/post_types/page/metaboxes/landing_page_homeboxes.php',
));
*/

/*
include_template  - define templates to use this metabox with
*/

/*
$filter_people = new WPAlchemy_MetaBox(array
    (
    'id'                => 'filter_people',
    'title'             => __( 'Filter People By Office' ),
    'types'             => array( 'page' ),
    'context'           => 'normal',
    'mode'              => WPALCHEMY_MODE_EXTRACT,
    'priority'          => 'low',
    'template'          => get_template_directory() . '/post_types/page/metaboxes/filter_people.php',
    'autosave'          => true,
    'prefix'            => 'filter_people_',
    'include_template'  => array( 'part-people_everyone.php', 'part-people_listview.php' ),
    'hide_editor' => TRUE,
    'save_filter'       => 'clear_duplicates',
    'save_action'       => 'clear_everyone_and_listview_transients'
));
*/

/*
*/
/*
http://local.bartlett/wp-admin/post.php?post=220&action=edit
http://local.bartlett/wp-admin/post-new.php?post_type=person
*/

function pageInit(){
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    // $post_type = get_post($post_id)->post_type;
    $post_type = ( array_key_exists('post_type', $_GET) ) ? $_GET['post_type'] : get_post_type($_GET['post']);
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

    // assign wpalchemy metaboxes to page templates in admin 
    switch( $template_file ) {
        case "generic.php" : {
        } break;
        
        case "home.php" : {
            $landing = new WPAlchemy_MetaBox(array
                (
                'id'                => 'home_meta',
                'title'             => __( 'Home page' ),
                'types'             => array( 'page' ),
                'context'           => 'normal',
                // 'mode'              => WPALCHEMY_MODE_ARRAY,
                'mode'              => WPALCHEMY_MODE_EXTRACT,
                'priority'          => 'low',
                'template'          => get_template_directory() . '/custom_posts/page/metaboxes/home_wpalchemy.php',
                'autosave'          => true,
                'prefix'            => 'bgh_',
                'include_template'  => array( 'home.php' ),
                'hide_editor' => TRUE,
                'save_filter'       => 'clear_duplicates'
            ));
        } break;

        case "about.php" : {

        } break;

        case "contact.php" : {
        } break;
    }
}
add_action( 'init', 'pageInit' );

// standard wp metaboxes to specific pages based on page template
function my_meta_init() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

    switch( $template_file ) {
        case "generic.php" : {
        } break;
        
        case "home.php" : {
        } break;

        case "about.php" : {
            remove_post_type_support('page', 'editor');

            // banner
            add_meta_box(
                'about_banner',
                __( 'Banner', 'myplugin_textdomain' ),
                'about_banner_meta_box_callback',
                'page',
                'normal'
            );

            // intro
            add_meta_box(
                'about_intro',
                __( 'Introduction', 'myplugin_textdomain' ),
                'about_intro_meta_box_callback',
                'page',
                'normal'
            );

            // management team
            add_meta_box(
                'about_team',
                __( 'Our senior team', 'myplugin_textdomain' ),
                'about_people_meta_box_callback',
                'page',
                'normal'
            );

            // The Bartlett Story
            add_meta_box(
                'about_our_story',
                __( 'The Bartlett Story', 'myplugin_textdomain' ),
                'about_our_story_meta_box_callback',
                'page',
                'normal'
            );

            // foundation items
            add_meta_box(
                'about_foundations',
                __( 'Bartlett Foundation items', 'myplugin_textdomain' ),
                'about_foundations_meta_box_callback',
                'page',
                'normal'
            );

            // awards
            add_meta_box(
                'about_awards',
                __( 'Awards / Partners', 'myplugin_textdomain' ),
                'about_awards_meta_box_callback',
                'page',
                'normal'
            );
        } break;

        case "contact.php" : {
            // disable default text/html editor box 
            remove_post_type_support('page', 'editor');

            // banner
            add_meta_box(
                'contact_banner',
                __( 'Banner title', 'myplugin_textdomain' ),
                'contact_banner_meta_box_callback',
                'page',
                'normal'
            );

            add_meta_box(
                'myplugin_cl',
                __( 'Offices', 'myplugin_textdomain' ),
                'contact_location_meta_box_callback',
                'page',
                'normal'
            );

        } break;
    }    
}
add_action('admin_init','my_meta_init');

require get_template_directory() . '/custom_posts/page/page_contact.php';
require get_template_directory() . '/custom_posts/page/page_about.php';

/*
-----------------------------------------------
*/

/*
// individual product page product list START
function iproduct_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'iproduct_save_meta_box_data', 'iproduct_meta_box_nonce' );

    $values['team'] = get_post_meta( $post->ID, 'c_team', true );
    $values['postid'] = $post->ID;

    require get_template_directory() . '/custom_posts/page/metaboxes/individual_product_meta.php';
}

function iproduct_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['iproduct_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['iproduct_meta_box_nonce'], 'iproduct_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('assignedTeam', 'c_team'),
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'iproduct_save_meta_box_data' );
// individual product page product list END
*/

/*
-----------------------------------------------
*/

function clear_duplicates( $meta, $post_id )
{
    /*
    if(isset($meta['product_subhead']) && is_array($meta['product_subhead']))   
    {
        $meta['product_subhead'] = array_map('unserialize', array_unique(array_map('serialize', $meta['product_subhead'])));
    }
    */
    return $meta;
}

?>