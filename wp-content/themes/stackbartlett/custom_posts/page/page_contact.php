<?php
/* 'Contact us' page office and team sorters */
/*
-----------------------------------------------
*/

// about banner START
function contact_banner_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'contact_banner_save_meta_box_data', 'contact_banner_meta_box_nonce' );

    $values['cb_heading'] = get_post_meta( $post->ID, 'contact_banner_heading', true );
    $values['banner_normal'] = get_post_meta( $post->ID, 'banner_normal', true );
    $values['banner_retina'] = get_post_meta( $post->ID, 'banner_retina', true );

    require get_template_directory() . '/custom_posts/page/metaboxes/contact_banner.php';
}

function contact_banner_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['contact_banner_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['contact_banner_meta_box_nonce'], 'contact_banner_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('contactBannerheading', 'contact_banner_heading'),
        array('banner_normal', 'banner_normal'),
        array('banner_retina', 'banner_retina')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'contact_banner_save_meta_box_data' );
// about banner END

function contact_location_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'contact_location_save_meta_box_data', 'contact_location_meta_box_nonce' );

    $values['locations'] = get_post_meta( $post->ID, 'c_locations', true );
    $values['postid'] = $post->ID;

    require get_template_directory() . '/custom_posts/page/metaboxes/contact_locations.php';
}

function contact_location_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['contact_location_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['contact_location_meta_box_nonce'], 'contact_location_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('officeOrder', 'c_locations'),
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'contact_location_save_meta_box_data' );

/*
-----------------------------------------------
*/

// contact page team leaders START
function contact_team_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'contact_team_save_meta_box_data', 'contact_team_meta_box_nonce' );

    $values['title'] = get_post_meta( $post->ID, 'c_title', true );
    $values['copy'] = get_post_meta( $post->ID, 'c_copy', true );
    $values['team'] = get_post_meta( $post->ID, 'c_team', true );
    $values['postid'] = $post->ID;

    require get_template_directory() . '/custom_posts/page/metaboxes/contact_teamleaders.php';
}

function contact_team_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['contact_team_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['contact_team_meta_box_nonce'], 'contact_team_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('contactTeamTitle', 'c_title'),
        array('contactTeamCopy', 'c_copy'),
        array('assignedTeam', 'c_team')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'contact_team_save_meta_box_data' );
// contact page team leaders END
?>
