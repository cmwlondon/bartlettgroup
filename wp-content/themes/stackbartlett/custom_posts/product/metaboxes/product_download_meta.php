<?php
/*
downloadheading -> $values['dl_heading'] -> dl_heading
downloadcopy -> $values['dl_copy'] -> dl_copy
downloadlink -> $values['dl_link'] -> dl_link
downloaddata -> $values['dl_data'] -> dl_data
downloadlabel -> $values['dl_label'] -> dl_label
downloadimage -> $values['dl_image'] -> dl_image
*/
?>

<div class="customField mediumtext">
	<label for="downloadheading">Heading</label>
	<input type="text" id="downloadheading" name="downloadheading" value="<?php echo $values['dl_heading']; ?>">
</div>

<div class="customField wpeditor">
    <p>Copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'downloadcopy',
            'editor_class'  => 'dlcopy_copy_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['dl_copy'] ), 'dl_copy', $settings );
    ?>
</div>

<div class="customField checkbox">
    <input type="checkbox" value="contactenabled" id="contactenabled" name="contactenabled[]" <?php if ($values['dl_contact'] != '') : ?>checked="checked"<?php endif; ?>><label for="contactenabled">Show contact details</label>
</div>

<div class="customField wpeditor">
    <p>Contact name/details</p>
    <?php
        $settings = array(
            'textarea_name' => 'contactname',
            'editor_class'  => 'contactname_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['dl_conname'] ), 'contactname', $settings );
    ?>
</div>

<div class="customField mediumtext">
    <label for="contactemail">Contact email address</label>
    <input type="text" id="contactemail" name="contactemail" value="<?php echo $values['dl_conemail']; ?>">
</div>

<div class="customField checkbox">
    <input type="checkbox" value="brochureenabled" id="brochureenabled" name="brochureenabled[]" <?php if ($values['dl_brochure'] != '') : ?>checked="checked"<?php endif; ?>><label for="brochureenabled">Show brochure download</label>
</div>

<div class="customField docSelector">
    <p>Document URL/Link</p>
    <input type="hidden" class="link" id="downloadlink" name="downloadlink" value="<?php echo $values['dl_link']; ?>">
    <p class="docLinkPath" <?php if ($values['dl_link'] == '' || $values['dl_link'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['dl_link']; ?></p>
    <a href="" class="bdSelectTrigger action">Add/Update document</a>
    <input type="hidden" class="data" id="downloaddata" name="downloaddata" value="<?php echo $values['dl_data']; ?>">
</div>

<div class="customField mediumtext">
	<label for="downloadlabel">button label</label>
	<input type="text" id="downloadlabel" name="downloadlabel" value="<?php echo $values['dl_label']; ?>">
</div>

<div class="customField imageSelector">
	<label for="downloadimage">Select image (optional)</label>
	<input type="hidden" id="downloadimage" name="downloadimage" value="<?php echo $values['dl_image']; ?>">
	<a href="" class="dlimagePicker action">Add/Update image</a>
    <a href="" class="dlimageClear action">Remove image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['dl_image']; ?>" <?php if ($values['dl_image'] == '' || $values['dl_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['dl_image'] == '' || $values['dl_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['dl_image']; ?></p>
</div>

<script type="text/javascript">
var imagePickers = [],
	documentPickers = [];


jQuery(document).ready(function(){
    documentPickers.push(new DocPicker({
        "trigger" : jQuery('.bdSelectTrigger')
    }));

    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.dlimagePicker')
    }));

    jQuery('.dlimageClear').on('click', function(e){
        e.preventDefault();

        var Malex = jQuery(this).parents('.imageSelector').eq(0);
        var imgField = Malex.find('input');
        var imgThumb = Malex.find('img');
        var imagepath = Malex.find('.imagepath');

        imgField.val('');
        imgThumb.css({"opacity" : "0"});
        imgThumb.attr({"src" : ""});
        imagepath.text('');
    });
});
</script>
