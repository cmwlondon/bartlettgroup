<?php
// custom post type 'product' banner text 
// bannerText -> $values['banner_text'] -> banner_text
?>
<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label for="bannerNorml">Banner image (normal)</label>
		<input type="hidden" id="bannerNorml" name="bannerNorml" value="<?php echo $values['banner_normal']; ?>">
		<a href="" class="bannerNormalPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['banner_normal']; ?>" <?php if ($values['banner_normal'] == '' || $values['banner_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['banner_normal'] == '' || $values['banner_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['banner_normal']; ?></p>
	</div>

	<div class="customField imageSelector multiImage">
		<label for="bannerRetina">Banner image (retina)</label>
		<input type="hidden" id="bannerRetina" name="bannerRetina" value="<?php echo $values['banner_retina']; ?>">
		<a href="" class="bannerRetinaPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['banner_retina']; ?>" <?php if ($values['banner_retina'] == '' || $values['banner_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['banner_retina'] == '' || $values['banner_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['banner_retina']; ?></p>
	</div>
</div>
<div class="customField selectField">
	<label for="awardImage">Banner image alignment</label>
	<select name="bannerAlign" id="bannerAlign">
		<option value="left" <?php if ( $values['banner_align'] == 'left' ) : ?> selected<?php endif; ?>>Left</option>
		<option value="center"<?php if ( $values['banner_align'] == 'center' ) : ?> selected<?php endif; ?>>Centre</option>
		<option value="right"<?php if ( $values['banner_align'] == 'right' ) : ?> selected<?php endif; ?>>Right</option>
	</select>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.bannerNormalPicker')
    }));
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.bannerRetinaPicker')
    }));
});
</script>

<div class="customField mediumtext">
	<label for="bannerText">Banner Text</label>
	<input type="text" id="bannerText" name="bannerText" value="<?php echo $values['banner_text']; ?>">
</div>
