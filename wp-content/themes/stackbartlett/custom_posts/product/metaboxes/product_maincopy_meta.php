<?php
/*
mcHeading -> $values['maincopy_heading'] -> maincopy_heading
mcP1 -> $values['maincopy_p1'] -> maincopy_p1
mcLeft -> $values['maincopy_left'] -> maincopy_left
mcRight -> $values['maincopy_right'] -> maincopy_right
*/
?>

<div class="customField mediumtext">
	<label for="mcHeading">Heading</label>
	<input type="text" id="mcHeading" name="mcHeading" value="<?php echo $values['maincopy_heading']; ?>">
</div>

<div class="customField wpeditor">
    <p>inital copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'mcP1',
            'editor_class'  => 'main_p1_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['maincopy_p1'] ), 'main_p1', $settings );
    ?>
</div>

<div class="customField wpeditor">
    <p>left column</p>
    <?php
        $settings = array(
            'textarea_name' => 'mcLeft',
            'editor_class'  => 'main_left_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['maincopy_left'] ), 'main_left', $settings );
    ?>
</div>

<div class="customField wpeditor">
    <p>right column</p>
    <?php
        $settings = array(
            'textarea_name' => 'mcRight',
            'editor_class'  => 'main_right_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['maincopy_right'] ), 'main_right', $settings );
    ?>
</div>
