<?php
/*
stepsleftheading -> $values['stepsleft_heading'] -> stepsleft_heading
stepsleft -> $values['steps_left'] -> steps_left
stepsrightheading -> $values['stepsright_heading'] -> stepsright_heading
stepsright -> $values['steps_right'] -> steps_right

*/
?>

<div class="customField mediumtext">
	<label for="stepsleftheading">left heading</label>
	<input type="text" id="stepsleftheading" name="stepsleftheading" value="<?php echo $values['stepsleft_heading']; ?>">
</div>

<div class="customField wpeditor">
    <p>left column copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'stepsleft',
            'editor_class'  => 'steps_left_copy_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['steps_left'] ), 'steps_left', $settings );
    ?>
</div>

<div class="customField mediumtext">
	<label for="stepsrightheading">right heading</label>
	<input type="text" id="stepsrightheading" name="stepsrightheading" value="<?php echo $values['stepsright_heading']; ?>">
</div>

<div class="customField wpeditor">
    <p>right column copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'stepsright',
            'editor_class'  => 'steps_right_copy_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['steps_right'] ), 'steps_right', $settings );
    ?>
</div>
