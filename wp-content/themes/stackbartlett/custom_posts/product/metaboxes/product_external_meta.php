<?php
// product_external.php

/*
$values['showproductpage']
$values['externallink']
*/
?>

<div class="customField checkbox">
    <input type="checkbox" value="openlink" id="openlink" name="openlink[]" <?php if ($values['openlink'] != '') : ?>checked="checked"<?php endif; ?>><label for="openlink">Open external link instead of showing product page</label>
</div>

<div class="customField mediumtext">
    <label for="externallink">External link</label>
    <input type="text" id="externallink" name="externallink" value="<?php echo $values['externallink']; ?>">
</div>
