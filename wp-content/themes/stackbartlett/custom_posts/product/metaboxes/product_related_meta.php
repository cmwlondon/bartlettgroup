<?php
/*
relatedheading -> $values['related_heading'] -> related_heading
relatedimage -> $values['rld_image'] -> related_image
*/

$taglist = buildTaglist($post->ID);
// $related_products = get_related_products($taglist['array'], $post->ID);
// $all_products = get_all_products($post->ID);
$synced_products = synchronise_related_products( explode(',', $values['rld_order']),  $post->ID );
$assigned = (count($synced_products['assigned']) > 0) ? get_products_by_order($synced_products['assigned']) : [];
$unassigned = (count($synced_products['unassigned']) > 0) ? get_products_by_order($synced_products['unassigned']) : [];
?>

<!-- <div>
    <p><a href="" class="getAllProducts">All products (except this one)</a></p>
    <p><a href="" class="getfilteredProducts">Filtered products (except this one)</a></p>
</div> -->

<div class="customField mediumtext">
	<label for="relatedheading">heading</label>
	<input type="text" id="relatedheading" name="relatedheading" value="<?php echo $values['rld_heading']; ?>">
</div>

<div class="customField imageSelector">
	<label for="relatedimage">Select image to display under 'Related Products' on other product pages</label>
	<input type="hidden" id="relatedimage" name="relatedimage" value="<?php echo $values['rld_image']; ?>">
	<a href="" class="rldimagePicker action">Add/Update image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['rld_image']; ?>" <?php if ($values['rld_image'] == '' || $values['rld_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['rld_image'] == '' || $values['rld_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['rld_image']; ?></p>
</div>
<div class="customField itemSorter relatedProductSorter">
    <input type="hidden" id="relatedProductOrder" name="relatedProductOrder" value="<?php echo implode(',', $synced_products['assigned']); ?>">

    <p>Assigned</p>
    <ul id="rpSorterAssign" class="slide2Connected">
    <?php
    foreach($assigned AS $product) :
    ?>
    <li class="clearfix" id="<?php echo "relpro$product->ID"; ?>">
        <h3><?php echo $product->post_title; ?></h3>
    </li>
    <?php
        endforeach;
    ?>
    </ul>

    <p>Unassigned</p>
    <ul id="rpSorterUnAssign" class="slide2Connected">
    <?php 
    foreach($unassigned AS $product) :
    ?>
    <li class="clearfix" id="<?php echo "relpro$product->ID"; ?>">
        <h3><?php echo $product->post_title; ?></h3>
    </li>
    <?php
        endforeach;
    ?>
    </ul>
</div>


<script type="text/javascript">
var imagePickers = [],
    related_products_order = '<?php echo $values['rld_order']; ?>';;

jQuery(document).ready(function(){
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.rldimagePicker')
    }));

    if (jQuery('#rpSorterAssign').length != 0) {
        jQuery('#rpSorterAssign, #rpSorterUnAssign').sortable({
            "connectWith" : '.slide2Connected',
            "receive" : function ( event, ui ) {
                if(ui.item.hasClass("dummy"))
                    ui.sender.sortable("cancel");
            },
            "start" : function( event, ui ) {
            },
            "stop" : function( event, ui ) {
                newOrderList = [];
                jQuery('#rpSorterAssign li').not(".dummy").each(function(i){
                    newOrderList.push( jQuery(this).attr("id").substr(6) );
                });
                jQuery('#relatedProductOrder').val(newOrderList.join());
            }
        }).disableSelection();

        /*
        jQuery('#rpSorterAssign').on('sortstop',function(event, ui) {
            console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
        });
        jQuery('#rpSorterUnAssign').on('sortstop',function(event, ui) {
            console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
        });
        */
    }

    jQuery('.getAllProducts').on('click',function(e){
        e.preventDefault();

        jQuery.ajax({
            url: bartlett_ajax.ajaxUrl,
            data: {
                "action" : "get_all_products",
                "postid" : postid
            },
            success:function(data) {
                console.log('success');
                console.log(data);
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });  
        
    });
    
    jQuery('.getfilteredProducts').on('click',function(e){
        e.preventDefault();

        jQuery.ajax({
            url: bartlett_ajax.ajaxUrl,
            data: {
                "action" : "get_filtered_products",
                "postid" : postid,
                "tags" : taglist
            },
            success:function(data) {
                console.log('success');
                console.log(data);
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });  
    });

});
</script>
