<?php
/*
mcHeading -> $values['maincopy_heading'] -> maincopy_heading
mcP1 -> $values['maincopy_p1'] -> maincopy_p1
mcLeft -> $values['maincopy_left'] -> maincopy_left
mcRight -> $values['maincopy_right'] -> maincopy_right
*/
?>

<div class="customField mediumtext">
	<label for="ecHeading">Heading</label>
	<input type="text" id="ecHeading" name="ecHeading" value="<?php echo $values['extracopy_heading']; ?>">
</div>

<div class="customField wpeditor">
    <p>inital copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'ecP1',
            'editor_class'  => 'extra_p1_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['extracopy_p1'] ), 'extra_p1', $settings );
    ?>
</div>

<div class="customField wpeditor">
    <p>left column</p>
    <?php
        $settings = array(
            'textarea_name' => 'ecLeft',
            'editor_class'  => 'extra_left_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['extracopy_left'] ), 'extra_left', $settings );
    ?>
</div>

<div class="customField wpeditor">
    <p>right column</p>
    <?php
        $settings = array(
            'textarea_name' => 'ecRight',
            'editor_class'  => 'extra_right_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['extracopy_right'] ), 'extra_right', $settings );
    ?>
</div>
