<?php
/*
$values['main'] -> #main -> main
$values['sidebar'] -> #sidebar -> sidebar
$values['keycontact'] -> #keycontact -> keycontact
*/
?>

<div class="customField wpeditor">
    <p>copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'main',
            'editor_class'  => 'main_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['main'] ), 'mainwp', $settings );
    ?>
</div>

<!--
<div class="customField textarea">
    <label for="main">Main</label>
    <textarea id="main" name="main" cols="40" rows="8"><?php echo html_entity_decode( $values['main'] ); ?></textarea>
</div>
-->

<div class="customField imageSelector">
	<label for="awardImage">sidebar image</label>
	<input type="hidden" id="productSidebar" name="productSidebar" value="<?php echo $values['sidebar']; ?>">
	<a href="" class="sidebarPicker action">Add/Update image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['sidebar']; ?>" <?php if ($values['sidebar'] == '' || $values['sidebar'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['sidebar'] == '' || $values['sidebar'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['sidebar']; ?></p>
</div>

<div class="customField selector">
<?php
$people_keycontact = get_all_people('keycontact');
?>
<label for="keycontact">key contacts</label>
<select name="keycontact" id="keycontact">
    <option value="-999">Select key contact</option>
<?php
foreach ($people_keycontact AS $person) :
    $metadata = get_post_meta($person->ID);

    $forename = $metadata['p_forename'][0];
    $surname = $metadata['p_surname'][0];
    $role = $metadata['p_role'][0];
?>
    <option value="<?php echo $person->ID; ?>" <?php if ( $values['keycontact'] == $person->ID) { echo "selected"; } ?>><?php echo $forename." ".$surname." - ".$role; ?></option>
<?php endforeach; ?>
</select>
</div>

<div class="customField imageSelector">
    <label for="productRelated">Related products image</label>
    <input type="hidden" id="productRelated" name="productRelated" value="<?php echo $values['related']; ?>">
    <a href="" class="relatedPicker action">Add/Update image</a>
    <img class="imageThumbnail" alt="" src="<?php echo $values['related']; ?>" <?php if ($values['related'] == '' || $values['related'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
    <p class="imagepath" <?php if ($values['related'] == '' || $values['related'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['related']; ?></p>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.sidebarPicker')
    }));
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.relatedPicker')
    }));
});
</script>
