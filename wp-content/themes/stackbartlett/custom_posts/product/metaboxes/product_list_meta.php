<?php
/*
$values['list_image'] -> #listImage -> list_image
$values['list_title'] -> #listTitle -> list_title
$values['list_copy'] -> #listCopy -> list_copy
$values['list_button_text'] -> #listButtonText -> list_button_text
*/
?>

<div class="customField imageSelector">
	<label for="awardImage">list image</label>
	<input type="hidden" id="listImage" name="listImage" value="<?php echo $values['list_image']; ?>">
	<a href="" class="listPicker action">Add/Update image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['list_image']; ?>" <?php if ($values['list_image'] == '' || $values['list_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['list_image'] == '' || $values['list_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['list_image']; ?></p>
</div>

<div class="customField longtext">
    <label for="awardTitle">Title</label>
    <input type="text" id="listTitle" name="listTitle" value="<?php echo $values['list_title']; ?>">
</div>

<div class="customField wpeditor">
    <p>Copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'listCopy',
            'editor_class'  => 'lc_wpeditor',
            'textarea_rows' => 6
        );
        wp_editor( html_entity_decode($values['list_copy']), 'lc', $settings );
    ?>
</div>

<div class="customField longtext">
    <label for="listButtonText">Button label</label>
    <input type="text" id="listButtonText" name="listButtonText" value="<?php echo $values['list_button_text']; ?>">
</div>

<p>link: <?php echo get_permalink($post->ID); ?></p>

<script type="text/javascript">
var listImagePickers = [];

jQuery(document).ready(function(){
    listImagePickers.push( new MediaPanel({
    	"trigger" : jQuery('.listPicker')
    }) );
});
</script>
