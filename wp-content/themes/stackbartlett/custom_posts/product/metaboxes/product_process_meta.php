<?php
/*
processheading -> $values['process_heading'] -> process_heading
processleft -> $values['process_left'] -> process_left
processright -> $values['process_right'] -> process_right

*/
?>

<div class="customField mediumtext">
	<label for="processheading">heading</label>
	<input type="text" id="processheading" name="processheading" value="<?php echo $values['process_heading']; ?>">
</div>

<div class="customField wpeditor">
    <p>left column copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'processleft',
            'editor_class'  => 'process_left_copy_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['process_left'] ), 'process_left', $settings );
    ?>
</div>

<div class="customField wpeditor">
    <p>right column copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'processright',
            'editor_class'  => 'process_right_copy_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['process_right'] ), 'process_right', $settings );
    ?>
</div>
