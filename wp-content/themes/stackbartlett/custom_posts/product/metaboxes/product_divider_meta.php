<div class="clearfix multiImages">
	<div class="customField imageSelector multiImage">
		<label for="dividerNorml">Divider image (normal)</label>
		<input type="hidden" id="dividerNorml" name="dividerNorml" value="<?php echo $values['divider_normal']; ?>">
		<a href="" class="dividerNormalPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['divider_normal']; ?>" <?php if ($values['divider_normal'] == '' || $values['divider_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['divider_normal'] == '' || $values['divider_normal'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['divider_normal']; ?></p>
	</div>

	<div class="customField imageSelector multiImage">
		<label for="dividerRetina">Divider image (retina)</label>
		<input type="hidden" id="dividerRetina" name="dividerRetina" value="<?php echo $values['divider_retina']; ?>">
		<a href="" class="dividerRetinaPicker action">Add/Update image</a>
		<img class="imageThumbnail" alt="" src="<?php echo $values['divider_retina']; ?>" <?php if ($values['divider_retina'] == '' || $values['divider_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
		<p class="imagepath" <?php if ($values['divider_retina'] == '' || $values['divider_retina'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['divider_retina']; ?></p>
	</div>
</div>
<div class="customField selectField">
	<label for="awardImage">Divider image alignment</label>
	<select name="dividerAlign" id="dividerAlign">
		<option value="left" <?php if ( $values['divider_align'] == 'left' ) : ?> selected<?php endif; ?>>Left</option>
		<option value="center"<?php if ( $values['divider_align'] == 'center' ) : ?> selected<?php endif; ?>>Centre</option>
		<option value="right"<?php if ( $values['divider_align'] == 'right' ) : ?> selected<?php endif; ?>>Right</option>
	</select>
</div>

<script type="text/javascript">
var imagePickers = [];

jQuery(document).ready(function(){
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.dividerNormalPicker')
    }));
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.dividerRetinaPicker')
    }));
});
</script>
