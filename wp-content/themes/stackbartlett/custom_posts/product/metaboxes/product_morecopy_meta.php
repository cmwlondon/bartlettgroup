<?php
/*
morelefthead -> $values['moreleft_heading'] -> moreleft_heading
moreleftcopy -> $values['moreleft_copy'] -> moreleft_copy
morerighthead -> $values['moreright_heading'] -> moreright_heading
morerightcopy -> $values['moreright_copy'] -> moreright_copy
*/
?>

<div class="customField mediumtext">
	<label for="morelefthead">left column heading</label>
	<input type="text" id="morelefthead" name="morelefthead" value="<?php echo $values['moreleft_heading']; ?>">
</div>

<div class="customField wpeditor">
    <p>left column copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'moreleftcopy',
            'editor_class'  => 'more_left_copy_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['moreleft_copy'] ), 'moreleft_copy', $settings );
    ?>
</div>

<div class="customField mediumtext">
	<label for="morerighthead">right column heading</label>
	<input type="text" id="morerighthead" name="morerighthead" value="<?php echo $values['moreright_heading']; ?>">
</div>

<div class="customField wpeditor">
    <p>right column copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'morerightcopy',
            'editor_class'  => 'more_right_copy_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode( $values['moreright_copy'] ), 'moreright_copy', $settings );
    ?>
</div>
