<?php
/*
$values['intro_copy'] -> #introCopy -> intro_copy
*/
?>

<div class="customField wpeditor">
    <p>copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'introCopy',
            'editor_class'  => 'intro_wpeditor',
            'textarea_rows' => 6,
            'wpautop' => false
        );
        wp_editor( html_entity_decode($values['intro_copy']), 'introwp', $settings );
    ?>
</div>

<div class="customField checkbox">
    <input type="checkbox" value="showiip" id="showiip" name="showiip[]" <?php if ($values['show_iip'] != '') : ?>checked="checked"<?php endif; ?>><label for="showiip">Show insurance products instead of introduction copy</label>
</div>