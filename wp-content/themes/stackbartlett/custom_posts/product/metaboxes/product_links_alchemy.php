<div class="multiFieldWrapper">
    <p class="head">Document downloads</p>
    <p>
      <a href="#" title="Remove all links" class="dodelete-doc_links button">Remove All</a>
      <a href="#" title="Add new link" class="docopy-doc_links button">Add Document</a>
    </p>
    <?php
    $options = array('length' => 1, 'limit' => 5);
    while($mb->have_fields_and_multi('doc_links')) :
        $mb->the_group_open();
    ?>

    <div class="multiFieldBox">
        <?php $mb->the_field('text'); ?>
        <div class="customField mediumtext">
            <label for="<?php $mb->the_name(); ?>">Document link text</label>
            <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
        </div>

        <div class="customField docSelector">
            <?php $mb->the_field('link'); ?>
            <p>Document URL/Link</p>
            <input type="hidden" class="link" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
            <p class="docLinkPath" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>><?php $mb->the_value() ?></p>
            <a href="" class="docLinkTrigger action">Add/Update document</a>
            <?php $mb->the_field('docdata'); ?>
            <input type="hidden" class="data" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
        </div>

        <a href="#" title="Remove link" class="dodelete button">Remove document</a>
    </div>

    <?php
        $mb->the_group_close();
    endwhile;
    ?>
</div>

<hr>

<div class="multiFieldWrapper">
    <p class="head">Misc links</p>
    <p>
      <a href="#" title="Remove all links" class="dodelete-misc_links button">Remove All</a>
      <a href="#" title="Add new link" class="docopy-misc_links button">Add Misc Link</a>
    </p>
    <?php
    $options = array('length' => 1, 'limit' => 5);
    while($mb->have_fields_and_multi('misc_links')) :
        $mb->the_group_open();
    ?>

    <div class="multiFieldBox">
        <?php $mb->the_field('text'); ?>
        <div class="customField mediumtext">
            <label for="<?php $mb->the_name(); ?>">link text</label>
            <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
        </div>

        <div class="customField mediumtext">
            <?php $mb->the_field('link'); ?>
            <p>Link URL</p>
            <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
        </div>
        <a href="#" title="Remove link" class="dodelete button">Remove link</a>
    </div>

    <?php
        $mb->the_group_close();
    endwhile;
    ?>

</div>

<script type="text/javascript">
var documentPickers = [];

jQuery(document).ready(function(){

    documentPickers.push(new DocPicker({
        "trigger" : jQuery('.docLinkTrigger')
    }));

});
</script>
