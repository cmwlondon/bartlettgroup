<?php
$synced_quotes = synchronise_quotes(explode(',', $values['quotes_order']), 'product', 'quotes_order', $post->ID);
$assigned = (count($synced_quotes['assigned']) > 0) ? get_quotes_in_order($synced_quotes['assigned']) : [];
$unassigned = (count($synced_quotes['unassigned']) > 0) ? get_quotes_in_order($synced_quotes['unassigned']) : [];
?>

<pre></pre>
<div class="customField checkbox">
    <input type="checkbox" value="yes" id="quotesEnabled" name="quotesEnabled[]" <?php if ($values['quotes_enabled'] == 'yes') : ?>checked="checked"<?php endif; ?>><label for="quotesEnabled">Show quotes</label>
</div>

<div class="customField itemSorter relatedProductSorter">
    <input type="text" id="quotesOrder" name="quotesOrder" value="<?php echo implode(',', $synced_quotes['assigned']); ?>">

    <p>Assigned</p>
    <ul id="quoteSorterAssign" class="slide2Connected">
    <?php
    foreach($assigned AS $quote) :
    ?>
    <li class="clearfix" id="<?php echo "quote$quote->ID"; ?>">
        <h3><?php echo $quote->post_title; ?></h3>
    </li>
    <?php
        endforeach;
    ?>
    </ul>

    <p>Unassigned</p>
    <ul id="quoteSorterUnAssign" class="slide2Connected">
    <?php 
    foreach($unassigned AS $quote) :
    ?>
    <li class="clearfix" id="<?php echo "quote$quote->ID"; ?>">
        <h3><?php echo $quote->post_title; ?></h3>
    </li>
    <?php
        endforeach;
    ?>
    </ul>
</div>

<script type="text/javascript">
var imagePickers = [],
    related_products_order = '<?php echo $values['quotes_order']; ?>';;

jQuery(document).ready(function(){
    imagePickers.push(new MediaPanel({
        "trigger" : jQuery('.rldimagePicker')
    }));

    if (jQuery('#quoteSorterAssign').length != 0) {
        jQuery('#quoteSorterAssign, #quoteSorterUnAssign').sortable({
            "connectWith" : '.slide2Connected',
            "receive" : function ( event, ui ) {
                if(ui.item.hasClass("dummy"))
                    ui.sender.sortable("cancel");
            },
            "start" : function( event, ui ) {
            },
            "stop" : function( event, ui ) {
                newOrderList = [];
                jQuery('#quoteSorterAssign li').not(".dummy").each(function(i){
                	console.log(jQuery(this).attr("id").substr(5));
                    newOrderList.push( jQuery(this).attr("id").substr(5) );
                });
                jQuery('#quotesOrder').val(newOrderList.join());
            }
        }).disableSelection();
    }
});
</script>
