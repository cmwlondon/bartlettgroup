<?php
/*
$values['case_study_order'] -> #caseSudyOrder -> case_study_order
*/

$taglist = buildTaglist($post->ID);
?>

<!-- product sorter START -->
    <!-- <div>
        <p><a href="" class="getAllCaseStudies">All case studies</a></p>
        <p><a href="" class="getfilteredCaseStudies">Filtered case studies</a></p>
    </div> -->

    <?php
    // synchronise_case_studies -> /wp_content/themes/stackbartlett/functions.php
    // post type: case_study

    // post_meta key: bp_case_studies
    
    $case_studies = synchronise_case_studies( explode(',', $values['case_study_order']), $taglist['array'], $post->ID );

    $assigned = (count($case_studies['assigned']) > 0) ? get_case_studies_in_order($case_studies['assigned']) : []; // 
    $unassigned = (count($case_studies['unassigned']) > 0) ? get_case_studies_in_order($case_studies['unassigned']) : [];

    // only show the sorter if there is a case study to show
    $showCaseStudySorter = ( count($case_studies['assigned']) > 0 || count($case_studies['unassigned']) > 0 );
    if ($showCaseStudySorter) :
    ?>
    <div class="customField itemSorter productSorter">
        <p>Case studies matching this product's tags: (<strong><?php echo $taglist['string']; ?></strong>)</p>
        <input type="hidden" id="caseSudyOrder" name="caseSudyOrder" value="<?php echo implode(',',$case_studies['assigned']); ?>">

        <p>Assigned</p>
        <ul id="caseStudySorterAssign" class="slideConnected">
        <?php
        foreach($assigned AS $case_study) :
        ?>
        <li class="clearfix" id="<?php echo "product$case_study->ID"; ?>">
            <h3><?php echo $case_study->post_title; ?></h3>
        </li>
        <?php
            endforeach;
        ?>
        </ul>

        <p>Unassigned</p>
        <ul id="caseStudySorterUnAssign" class="slideConnected">
        <?php 
        foreach($unassigned AS $case_study) :
        ?>
        <li class="clearfix" id="<?php echo "product$case_study->ID"; ?>">
            <h3><?php echo $case_study->post_title; ?></h3>
        </li>
        <?php
            endforeach;
        ?>
        </ul>
    </div>
    <?php else : ?>
    <p>No case studies in same category found: check the tags for this product and/or add a case study with the same tags: (<strong><?php echo $taglist['string']; ?></strong>).</p>
    <?php endif; ?>
<!-- product sorter END -->

    <script type="text/javascript">
    var sidebarPickers = [],
        documentPickers = [],
        postid = <?php echo $post->ID; ?>,
        taglist = '<?php echo $taglist['string']; ?>',
        case_study_order = '<?php echo $values['case_study_order']; ?>';

    jQuery(document).ready(function(){

        sidebarPickers.push(new MediaPanel({
            "trigger" : jQuery('.mediaTrigger_sidebar')
        }));

        documentPickers.push(new DocPicker({
            "trigger" : jQuery('.docLinkTrigger')
        }));

        // case study sorter
        if (jQuery('#caseStudySorterAssign').length != 0) {
            jQuery('#caseStudySorterAssign, #caseStudySorterUnAssign').sortable({
                "connectWith" : '.slideConnected',
                "receive" : function ( event, ui ) {
                    if(ui.item.hasClass("dummy"))
                        ui.sender.sortable("cancel");
                },
                "start" : function( event, ui ) {
                },
                "stop" : function( event, ui ) {
                    newOrderList = [];
                    jQuery('#caseStudySorterAssign li').not(".dummy").each(function(i){

                        newOrderList.push( jQuery(this).attr("id").substr(7) );

                    });
                    jQuery('#caseSudyOrder').val(newOrderList.join());
                }
            }).disableSelection();

            jQuery('#caseStudySorterAssign').on('sortstop',function(event, ui) {
                console.log('assign: ' + jQuery(this).find('li').not(".dummy").length);
            });
            jQuery('#caseStudySorterUnassign').on('sortstop',function(event, ui) {
                console.log('unassign: ' + jQuery(this).find('li').not(".dummy").length);
            });
        }

        jQuery('.getAllCaseStudies').on('click',function(e){
            e.preventDefault();

            jQuery.ajax({
                url: bartlett_ajax.ajaxUrl,
                data: {
                    "action" : "get_all_case_studies",
                    "postid" : postid
                },
                success:function(data) {
                    console.log('success');
                    console.log(data);
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            });  
            
        });
        
        jQuery('.getfilteredCaseStudies').on('click',function(e){
            e.preventDefault();

            jQuery.ajax({
                url: bartlett_ajax.ajaxUrl,
                data: {
                    "action" : "get_filtered_case_studies",
                    "postid" : postid,
                    "tags" : taglist,
                    "order" : case_study_order
                },
                success:function(data) {
                    console.log('success');
                    console.log(data);
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            });  
        });

    });
    </script>
