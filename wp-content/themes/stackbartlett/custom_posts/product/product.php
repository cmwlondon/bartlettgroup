<?php
/*
custom post type: product

title
header image 'featured image'

intro heading
intro copy

main copy
sidebar image
downloads[]
misc links[]
key contact

case studies[]

related product images

list image
list title
list copy
list button text
list button link
*/

function product_init()
{
    $labels = array(
        'name'                => _x( 'Products', 'post type general name' ),
        'singular_name'       => _x( 'Product', 'post type singular name' ),
        'add_new'             => _x( 'Add New Product', 'product' ),
        'add_new_item'        => __( 'Add New Product'),
        'edit_item'           => __( 'Edit Product' ),
        'new_item'            => __( 'New Product' ),
        'all_items'           => __( 'All Products' ),
        'view_item'           => __( 'View Product' ),
        'search_items'        => __( 'Search Products' ),
        'not_found'           => __( 'No Products found' ),
        'not_found_in_trash'  => __( 'No Products found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Products'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
        'supports'           => array( 'title', 'thumbnail', 'tags'),
        'taxonomies' => array('post_tag'),
        'register_meta_box_cb' => 'add_product_metaboxes',

        // remove '/blog/' from beginning of custom posts
        // set custom post permalink to '/product/%postname%' in admin / settings / permalinks
        'rewrite' => array(
            'with_front' => false,
            'slug'       => 'product'
        )
    );

    register_post_type( 'product', $args );
}
add_action( 'init', 'product_init' );

/*
// add custom columns to the admin award 'browse' page and define column headings
function award_columns_head($defaults) {
    $date = $defaults['date'];  // save the date column
    unset($defaults['date']);

    $tags = $defaults['tags'];  // save the date column
    unset($defaults['tags']);

    $defaults['award_image'] = 'Image';
    $defaults['award_award'] = 'Award';
    $defaults['award_category'] = 'category';
    $defaults['award_year'] = 'Year';

    $defaults['tags'] = $tags;
    $defaults['date'] = $date;

    return $defaults;
}

// define what is displayed in each column
function award_columns_content($column_name, $post_ID) {
    $metadata = $metadata = get_post_meta($post_ID);

    $award_image = $metadata['a_image'][0];
    $year = $metadata['a_year'][0];
    $award = $metadata['a_award'][0];
    $category = $metadata['a_category'][0];

    switch($column_name) {
        case "award_image" : {
            echo "<img class=\"listAwardImage\" alt=\"\" src=\"$award_image\">";
        } break;
        case "award_year" : {
            echo $year;
        } break;
        case "award_award" : {
            echo $award;
        } break;
        case "award_category" : {
            echo $category;
        } break;
    }
}
add_filter('manage_award_posts_columns', 'award_columns_head');
add_action('manage_award_posts_custom_column', 'award_columns_content', 1, 2);
*/

/* define custom metabox */
function add_product_metaboxes(){

        // show product template or open external link
        add_meta_box(
            'myplugin_product_external',
            __( 'Open link or show product page', 'myplugin_textdomain' ),
            'product_external_meta_box_callback',
            'product',
            'normal'
        );

        // banner
        add_meta_box(
            'myplugin_product_banner',
            __( 'Banner', 'myplugin_textdomain' ),
            'product_banner_meta_box_callback',
            'product',
            'normal'
        );

        // block 1: introduction
        add_meta_box(
            'myplugin_product_intro',
            __( 'Introduction', 'myplugin_textdomain' ),
            'product_intro_meta_box_callback',
            'product',
            'normal'
        );

        // block 2: main copy
        add_meta_box(
            'myplugin_product_maincopy',
            __( 'block 1: Main copy', 'myplugin_textdomain' ),
            'product_maincopy_meta_box_callback',
            'product',
            'normal'
        );

        add_meta_box(
            'myplugin_product_extracopy',
            __( 'block 1: Extra copy', 'myplugin_textdomain' ),
            'product_extracopy_meta_box_callback',
            'product',
            'normal'
        );

        // divider image
        add_meta_box(
            'myplugin_product_divider',
            __( 'block 2: Divider image', 'myplugin_textdomain' ),
            'product_divider_meta_box_callback',
            'product',
            'normal'
        );

        // block 3: more copy
        add_meta_box(
            'myplugin_product_morecopy',
            __( 'block 3: More copy', 'myplugin_textdomain' ),
            'product_morecopy_meta_box_callback',
            'product',
            'normal'
        );

        // block 4: bartlett process
        add_meta_box(
            'myplugin_product_process',
            __( 'block 4: The Bartlett process', 'myplugin_textdomain' ),
            'product_process_meta_box_callback',
            'product',
            'normal'
        );

        // block 5: steps
        add_meta_box(
            'myplugin_product_steps',
            __( 'block 5: Steps', 'myplugin_textdomain' ),
            'product_steps_meta_box_callback',
            'product',
            'normal'
        );

        // case studies
        add_meta_box(
            'myplugin_product_case_studies',
            __( 'Case studies', 'myplugin_textdomain' ),
            'product_case_studies_meta_box_callback',
            'product',
            'normal'
        );

        // download
        add_meta_box(
            'myplugin_product_download',
            __( 'Find out more', 'myplugin_textdomain' ),
            'product_download_meta_box_callback',
            'product',
            'normal'
        );

        // quotes
        add_meta_box(
            'myplugin_product_quotes',
            __( 'Quotes', 'myplugin_textdomain' ),
            'product_quotes_meta_box_callback',
            'product',
            'normal'
        );

        // related products
        add_meta_box(
            'myplugin_product_related',
            __( 'Related products', 'myplugin_textdomain' ),
            'product_related_meta_box_callback',
            'product',
            'normal'
        );
}
// add_action( 'add_meta_boxes_quote', 'add_quote_metaboxes' );

function product_external_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_external_meta_box_data', 'product_external_meta_box_nonce' );

    $values['openlink'] = get_post_meta( $post->ID, 'product_openlink', true );
    $values['externallink'] = get_post_meta( $post->ID, 'product_link', true );


    require get_template_directory() . '/custom_posts/product/metaboxes/product_external_meta.php';
}

function product_external_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_external_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_external_meta_box_nonce'], 'product_external_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('externallink', 'product_link'),
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

    $my_data = (count($_POST['openlink']) > 0 ) ? 'openlink' : '';
    update_post_meta( $post_id, 'product_openlink', $my_data );
}
add_action( 'save_post', 'product_external_meta_box_data' );

// banner text START
function product_banner_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_banner_meta_box_data', 'product_banner_meta_box_nonce' );

    $values['banner_text'] = get_post_meta( $post->ID, 'banner_text', true );
    $values['banner_normal'] = get_post_meta( $post->ID, 'banner_normal', true );
    $values['banner_retina'] = get_post_meta( $post->ID, 'banner_retina', true );
    $values['banner_align'] = get_post_meta( $post->ID, 'banner_align', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_banner_meta.php';
}

function product_banner_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_banner_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_banner_meta_box_nonce'], 'product_banner_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('bannerText', 'banner_text'),
        array('bannerNorml', 'banner_normal'),
        array('bannerRetina', 'banner_retina'),
        array('bannerAlign', 'banner_align')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'product_banner_meta_box_data' );
// banner text END

// intro copy / insurance products graphic START
function product_intro_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_intro_meta_box_data', 'product_intro_meta_box_nonce' );

    $values['intro_copy'] = get_post_meta( $post->ID, 'intro_copy', true ); // multi-paragraph HTML field
    $values['show_iip'] = get_post_meta( $post->ID, 'show_iip', true ); // checkbox

    require get_template_directory() . '/custom_posts/product/metaboxes/product_intro_meta.php';
}

function product_intro_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_intro_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_intro_meta_box_nonce'], 'product_intro_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('introCopy', 'intro_copy')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            // $my_data = sanitize_text_field( $_POST[$field[0]] );
            $my_data = htmlentities( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
    $my_data = (count($_POST['showiip']) > 0 ) ? 'showiip' : '';
    update_post_meta( $post_id, 'show_iip', $my_data );
}
add_action( 'save_post', 'product_intro_meta_box_data' );
// intro copy / insurance products graphic END

// main product copy START
function product_maincopy_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_maincopy_meta_box_data', 'product_maincopy_meta_box_nonce' );

    $values['maincopy_heading'] = get_post_meta( $post->ID, 'maincopy_heading', true );
    $values['maincopy_p1'] = get_post_meta( $post->ID, 'maincopy_p1', true );
    $values['maincopy_left'] = get_post_meta( $post->ID, 'maincopy_left', true );
    $values['maincopy_right'] = get_post_meta( $post->ID, 'maincopy_right', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_maincopy_meta.php';
}

function product_maincopy_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_maincopy_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_maincopy_meta_box_nonce'], 'product_maincopy_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('mcHeading', 'maincopy_heading', false),
        array('mcP1', 'maincopy_p1', true),
        array('mcLeft', 'maincopy_left', true),
        array('mcRight', 'maincopy_right', true)
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ( $field[2] ) {
                $my_data = htmlentities( $_POST[$field[0]] );
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'product_maincopy_meta_box_data' );
// main product copy END

// extra product copy START
function product_extracopy_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_extracopy_meta_box_data', 'product_extracopy_meta_box_nonce' );

    $values['extracopy_heading'] = get_post_meta( $post->ID, 'extracopy_heading', true );
    $values['extracopy_p1'] = get_post_meta( $post->ID, 'extracopy_p1', true );
    $values['extracopy_left'] = get_post_meta( $post->ID, 'extracopy_left', true );
    $values['extracopy_right'] = get_post_meta( $post->ID, 'extracopy_right', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_extracopy_meta.php';
}

function product_extracopy_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_extracopy_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_extracopy_meta_box_nonce'], 'product_extracopy_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('ecHeading', 'extracopy_heading', false),
        array('ecP1', 'extracopy_p1', true),
        array('ecLeft', 'extracopy_left', true),
        array('ecRight', 'extracopy_right', true)
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ( $field[2] ) {
                $my_data = htmlentities( $_POST[$field[0]] );
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'product_extracopy_meta_box_data' );
// extra product copy END

// product full witdh interstitial image START
function product_divider_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_divider_meta_box_data', 'product_divider_meta_box_nonce' );

    $values['divider_normal'] = get_post_meta( $post->ID, 'divider_normal', true );
    $values['divider_retina'] = get_post_meta( $post->ID, 'divider_retina', true );
    $values['divider_align'] = get_post_meta( $post->ID, 'divider_align', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_divider_meta.php';
}

function product_divider_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_divider_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_divider_meta_box_nonce'], 'product_divider_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('dividerNorml', 'divider_normal'),
        array('dividerRetina', 'divider_retina'),
        array('dividerAlign', 'divider_align')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'product_divider_meta_box_data' );
// product full witdh interstitial image END

// product secondary copy START
function product_morecopy_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_morecopy_meta_box_data', 'product_morecopy_meta_box_nonce' );

    $values['moreleft_heading'] = get_post_meta( $post->ID, 'moreleft_heading', true );
    $values['moreleft_copy'] = get_post_meta( $post->ID, 'moreleft_copy', true );
    $values['moreright_heading'] = get_post_meta( $post->ID, 'moreright_heading', true );
    $values['moreright_copy'] = get_post_meta( $post->ID, 'moreright_copy', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_morecopy_meta.php';
}

function product_morecopy_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_morecopy_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_morecopy_meta_box_nonce'], 'product_morecopy_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('morelefthead', 'moreleft_heading', false),
        array('moreleftcopy', 'moreleft_copy', true),
        array('morerighthead', 'moreright_heading', false),
        array('morerightcopy', 'moreright_copy', true),
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ( $field[2] ) {
                $my_data = htmlentities( $_POST[$field[0]] );
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'product_morecopy_meta_box_data' );
// product secondary copy END

// 'Bartlett process' copy area START
function product_process_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_process_meta_box_data', 'product_process_meta_box_nonce' );

    $values['process_heading'] = get_post_meta( $post->ID, 'process_heading', true );
    $values['process_left'] = get_post_meta( $post->ID, 'process_left', true );
    $values['process_right'] = get_post_meta( $post->ID, 'process_right', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_process_meta.php';
}

function product_process_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_process_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_process_meta_box_nonce'], 'product_process_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('processheading', 'process_heading', false),
        array('processleft', 'process_left', true),
        array('processright', 'process_right', true)
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ( $field[2] ) {
                $my_data = htmlentities( $_POST[$field[0]] );
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'product_process_meta_box_data' );
// 'Bartlett process' copy area END

// product 'steps' block START
function product_steps_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_steps_meta_box_data', 'product_steps_meta_box_nonce' );

    $values['stepsleft_heading'] = get_post_meta( $post->ID, 'stepsleft_heading', true );
    $values['stepsright_heading'] = get_post_meta( $post->ID, 'stepsright_heading', true );
    $values['steps_left'] = get_post_meta( $post->ID, 'steps_left', true );
    $values['steps_right'] = get_post_meta( $post->ID, 'steps_right', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_steps_meta.php';
}

function product_steps_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_steps_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_steps_meta_box_nonce'], 'product_steps_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('stepsleftheading', 'stepsleft_heading', false),
        array('stepsrightheading', 'stepsright_heading', false),
        array('stepsleft', 'steps_left', true),
        array('stepsright', 'steps_right', true)
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ( $field[2] ) {
                $my_data = htmlentities( $_POST[$field[0]] );
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

}
add_action( 'save_post', 'product_steps_meta_box_data' );
// product 'steps' block END

// product related case studies START
/*
note: case studies selection depends on tags assigned to product
*/
function product_case_studies_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_case_studies_meta_box_data', 'product_case_studies_meta_box_nonce' );

    $values['case_study_order'] = get_post_meta( $post->ID, 'pr_case_study_order', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_case_studies_meta.php';
}

function product_case_studies_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_case_studies_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_case_studies_meta_box_nonce'], 'product_case_studies_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('caseSudyOrder', 'pr_case_study_order'),
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'product_case_studies_meta_box_data' );
// product related case studies END

// product brochure download START
function product_download_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_download_meta_box_data', 'product_download_meta_box_nonce' );

    $values['dl_heading'] = get_post_meta( $post->ID, 'dl_heading', true );
    $values['dl_copy'] = get_post_meta( $post->ID, 'dl_copy', true );
    $values['dl_brochure'] = get_post_meta( $post->ID, 'dl_brochure', true );
    $values['dl_contact'] = get_post_meta( $post->ID, 'dl_contact', true );
    $values['dl_conname'] = get_post_meta( $post->ID, 'dl_conname', true );
    $values['dl_conemail'] = get_post_meta( $post->ID, 'dl_conemail', true );
    $values['dl_link'] = get_post_meta( $post->ID, 'dl_link', true );
    $values['dl_data'] = get_post_meta( $post->ID, 'dl_data', true );
    $values['dl_label'] = get_post_meta( $post->ID, 'dl_label', true );
    $values['dl_image'] = get_post_meta( $post->ID, 'dl_image', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_download_meta.php';
}

function product_download_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_download_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_download_meta_box_nonce'], 'product_download_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('downloadheading', 'dl_heading', false),
        array('downloadcopy', 'dl_copy', true),
        array('contactname', 'dl_conname', true),
        array('contactemail', 'dl_conemail', false),
        array('downloadcopy', 'dl_copy', true),
        array('downloadlink', 'dl_link', false),
        array('downloaddata', 'dl_data', false),
        array('downloadlabel', 'dl_label', false),
        array('downloadimage', 'dl_image', false)
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ( $field[2] ) {
                $my_data = htmlentities( $_POST[$field[0]] );
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

    $my_data = (count($_POST['brochureenabled']) > 0 ) ? 'brochureenabled' : '';
    update_post_meta( $post_id, 'dl_brochure', $my_data );

    $my_data = (count($_POST['contactenabled']) > 0 ) ? 'contactenabled' : '';
    update_post_meta( $post_id, 'dl_contact', $my_data );

}
add_action( 'save_post', 'product_download_meta_box_data' );
// product brochure download END

// quotes START
function product_quotes_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_quotes_meta_box_data', 'product_quotes_meta_box_nonce' );

    $values['quotes_enabled'] = get_post_meta( $post->ID, 'quotes_enabled', true );
    $values['quotes_order'] = get_post_meta( $post->ID, 'quotes_order', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_quotes_meta.php';
}

function product_quotes_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_quotes_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_quotes_meta_box_nonce'], 'product_quotes_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('quotesOrder', 'quotes_order')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }

    $my_data = (count($_POST['quotesEnabled']) > 0 ) ? 'yes' : 'no';
    update_post_meta( $post_id, 'quotes_enabled', $my_data );
}
add_action( 'save_post', 'product_quotes_meta_box_data' );
// quotes END

// may need to add 'related products' picker/sorter tools similar to case studies
// related products START
function product_related_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'product_related_meta_box_data', 'product_related_meta_box_nonce' );

    $values['rld_heading'] = get_post_meta( $post->ID, 'rld_heading', true );
    $values['rld_image'] = get_post_meta( $post->ID, 'rld_image', true );
    $values['rld_order'] = get_post_meta( $post->ID, 'rld_order', true );

    require get_template_directory() . '/custom_posts/product/metaboxes/product_related_meta.php';
}

function product_related_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['product_related_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['product_related_meta_box_nonce'], 'product_related_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('relatedheading', 'rld_heading', false),
        array('relatedimage', 'rld_image', false),
        array('relatedProductOrder', 'rld_order', false)
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ( $field[2] ) {
                $my_data = htmlentities( $_POST[$field[0]] );
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'product_related_meta_box_data' );
// related products END
?>
