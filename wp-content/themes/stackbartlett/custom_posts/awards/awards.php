<?php
/*
custom post type: awards

image
year
award
category

*/

function bartlett_award_init()
{
    $labels = array(
        'name'                => _x( 'Awards', 'post type general name' ),
        'singular_name'       => _x( 'Award', 'post type singular name' ),
        'add_new'             => _x( 'Add New Award', 'award' ),
        'add_new_item'        => __( 'Add New Award'),
        'edit_item'           => __( 'Edit Award' ),
        'new_item'            => __( 'New Award' ),
        'all_items'           => __( 'All Awards' ),
        'view_item'           => __( 'View Award' ),
        'search_items'        => __( 'Search Awards' ),
        'not_found'           => __( 'No Awards found' ),
        'not_found_in_trash'  => __( 'No Awards found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Awards'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => 'Awards',
        'hierarchical'       => false,
        'menu_position'      => null,
        // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
        'supports'           => array( 'title', 'thumbnail',tags),
        'taxonomies' => array('post_tag'),
        'register_meta_box_cb' => 'add_award_metaboxes'
    );

    register_post_type( 'award', $args );
}
add_action( 'init', 'bartlett_award_init' );

/* add custom columns to the admin award 'browse' page and define column headings */
function award_columns_head($defaults) {
    $date = $defaults['date'];  // save the date column
    unset($defaults['date']);

    $tags = $defaults['tags'];  // save the date column
    unset($defaults['tags']);

    $defaults['award_image'] = 'Image';
    $defaults['award_award'] = 'Award';
    $defaults['award_category'] = 'category';
    $defaults['award_year'] = 'Year';

    $defaults['tags'] = $tags;
    $defaults['date'] = $date;

    return $defaults;
}

/* define what is displayed in each column */ 
function award_columns_content($column_name, $post_ID) {
    $metadata = $metadata = get_post_meta($post_ID);

    $award_image = $metadata['a_image'][0];
    $year = $metadata['a_year'][0];
    $award = $metadata['a_award'][0];
    $category = $metadata['a_category'][0];

    switch($column_name) {
        case "award_image" : {
            echo "<img class=\"listAwardImage\" alt=\"\" src=\"$award_image\">";
        } break;
        case "award_year" : {
            echo $year;
        } break;
        case "award_award" : {
            echo $award;
        } break;
        case "award_category" : {
            echo $category;
        } break;
    }
}
add_filter('manage_award_posts_columns', 'award_columns_head');
add_action('manage_award_posts_custom_column', 'award_columns_content', 1, 2);

/* define custom metabox */
function add_award_metaboxes(){

    $screens = array( 'award' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'myplugin_aw',
            __( 'Award', 'myplugin_textdomain' ),
            'award_meta_box_callback',
            $screen,
            'normal'
        );

    }
}
// add_action( 'add_meta_boxes_quote', 'add_quote_metaboxes' );

function award_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'award_save_meta_box_data', 'award_meta_box_nonce' );

    $values['award'] = get_post_meta( $post->ID, 'a_award', true );
    $values['category'] = get_post_meta( $post->ID, 'a_category', true );
    $values['year'] = get_post_meta( $post->ID, 'a_year', true );
    $values['image'] = get_post_meta( $post->ID, 'a_image', true );
    $values['link'] = get_post_meta( $post->ID, 'a_link', true );
    $values['pic_id'] = get_post_meta( $post->ID, 'pic_id', true );
    $values['pic_w'] = get_post_meta( $post->ID, 'pic_w', true );
    $values['pic_h'] = get_post_meta( $post->ID, 'pic_h', true );

    
    require get_template_directory() . '/custom_posts/awards/metaboxes/award.php';
}

function award_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['award_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['award_meta_box_nonce'], 'award_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('awardTitle', 'a_award'),
        array('awardCategory', 'a_category'),
        array('awardYear', 'a_year'),
        array('awardImage', 'a_image'),
        array('awardLink', 'a_link'),
        array('awardPicID', 'pic_id'),
        array('awardPicWidth', 'pic_w'),
        array('awardPicHeight', 'pic_h')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            $my_data = sanitize_text_field( $_POST[$field[0]] );
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'award_save_meta_box_data' );
?>
