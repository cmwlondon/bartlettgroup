<div class="customField imageSelector">
	<label for="awardImage">Award image</label>
	<input type="hidden" id="awardImage" name="awardImage" value="<?php echo $values['image']; ?>">

    <input type="hidden" id="awardPicID" name="awardPicID" value="<?php echo $values['pic_id']; ?>" class="ident">
    <input type="hidden" id="awardPicWidth" name="awardPicWidth" value="<?php echo $values['pic_w']; ?>" class="width">
    <input type="hidden" id="awardPicHeight" name="awardPicHeight" value="<?php echo $values['pic_h']; ?>" class="height">

	<a href="" class="mediaTrigger_award action">Add/Update image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['image']; ?>" <?php if ($values['image'] == '' || $values['image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['image'] == '' || $values['image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['image']; ?></p>
</div>

<div class="customField longtext">
    <label for="awardTitle">Award</label>
    <input type="text" id="awardTitle" name="awardTitle" value="<?php echo $values['award']; ?>">
</div>

<div class="customField longtext">
    <label for="awardCategory">Category</label>
    <input type="text" id="awardCategory" name="awardCategory" value="<?php echo $values['category']; ?>">
</div>

<div class="customField mediumtext">
    <label for="awardYear">Year</label>
    <input type="text" id="awardYear" name="awardYear" value="<?php echo $values['year']; ?>">
</div>

<div class="customField longtext">
    <label for="awardLink">Link</label>
    <input type="text" id="awardLink" name="awardLink" value="<?php echo $values['link']; ?>">
</div>

<script type="text/javascript">
var thisMediaPanel_award;

jQuery(document).ready(function(){

thisMediaPanel_award = new MediaPanel({
	"trigger" : jQuery('.mediaTrigger_award')
});

});
</script>
