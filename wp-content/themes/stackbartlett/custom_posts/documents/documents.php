<?php
/*
custom post type: documents
relates to second level product templates, products

cover image optional
title
link -> media
*/

function bartlett_document_init()
{
    $labels = array(
        'name'                => _x( 'Documents', 'post type general name' ),
        'singular_name'       => _x( 'Document', 'post type singular name' ),
        'add_new'             => _x( 'Add New Document', 'document' ),
        'add_new_item'        => __( 'Add New Document'),
        'edit_item'           => __( 'Edit Document' ),
        'new_item'            => __( 'New Document' ),
        'all_items'           => __( 'All Documents' ),
        'view_item'           => __( 'View Document' ),
        'search_items'        => __( 'Search Documents' ),
        'not_found'           => __( 'No Documents found' ),
        'not_found_in_trash'  => __( 'No Documents found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Documents'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => 'Documents',
        'hierarchical'       => false,
        'menu_position'      => null,
        // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt'),
	   'taxonomies'	=>	array('post_tag')
    );

    register_post_type( 'document', $args );
}
add_action( 'init', 'bartlett_document_init' );
?>