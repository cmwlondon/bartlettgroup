<?php
/*
custom post type: quotes

quote
person
*/

function bartlett_quote_init()
{
    $labels = array(
        'name'                => _x( 'Quotes', 'post type general name' ),
        'singular_name'       => _x( 'Quote', 'post type singular name' ),
        'add_new'             => _x( 'Add New Quote', 'quote' ),
        'add_new_item'        => __( 'Add New Quote'),
        'edit_item'           => __( 'Edit Quote' ),
        'new_item'            => __( 'New Quote' ),
        'all_items'           => __( 'All Quotes' ),
        'view_item'           => __( 'View Quote' ),
        'search_items'        => __( 'Search Quotes' ),
        'not_found'           => __( 'No Quotes found' ),
        'not_found_in_trash'  => __( 'No Quotes found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Quotes'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => 'Quotes',
        'hierarchical'       => false,
        'menu_position'      => null,
        // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
        // 'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt'),
        'supports'           => array( 'title', 'tags'),
        'taxonomies' => array('post_tag'),
	   'register_meta_box_cb' => 'add_quote_metaboxes'

    );

    register_post_type( 'quote', $args );
}
add_action( 'init', 'bartlett_quote_init' );

function add_quote_metaboxes(){

    $screens = array( 'quote' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'myplugin_qt',
            __( 'Quote', 'myplugin_textdomain' ),
            'quote_meta_box_callback',
            $screen,
            'normal'
        );

    }
}
// add_action( 'add_meta_boxes_quote', 'add_quote_metaboxes' );

function quote_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'quote_save_meta_box_data', 'quote_meta_box_nonce' );

    $values['text'] = get_post_meta( $post->ID, 'q_quote_text', true );
    $values['source'] = get_post_meta( $post->ID, 'q_quote_source', true );

    require get_template_directory() . '/custom_posts/quotes/metaboxes/quote.php';
}

function quote_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['quote_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['quote_meta_box_nonce'], 'quote_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    // if ( ! isset( $_POST['quoteText'] ) ) { return; }
    if ( isset( $_POST['quoteText'] )) {
        $my_data = sanitize_text_field( $_POST['quoteText'] );
        update_post_meta( $post_id, 'q_quote_text', $my_data );
    }

    if ( isset( $_POST['quoteSource'] )) {
        $my_data = sanitize_text_field( $_POST['quoteSource'] );
        update_post_meta( $post_id, 'q_quote_source', $my_data );
    }
}
add_action( 'save_post', 'quote_save_meta_box_data' );


?>
