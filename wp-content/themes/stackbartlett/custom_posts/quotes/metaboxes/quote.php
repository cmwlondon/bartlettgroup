<?php
?>
<div class="customField longtext">
<label for="quoteText">Quote text</label>
<input type="text" id="quoteText" name="quoteText" value="<?php echo esc_attr( $values['text'] ); ?>" size="25" />
</div>

<div class="customField longtext">
<label for="quoteSource">Quote source</label>
<input type="text" id="quoteSource" name="quoteSource" value="<?php echo esc_attr( $values['source'] ); ?>" size="25" />
</div>
