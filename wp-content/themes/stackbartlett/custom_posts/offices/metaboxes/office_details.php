<?php
/*
office details metabox template

custom post type: office

fields:
    address
    email
    phone
    map_image
*/
?>
    <div class="personDetails postbox">
        <div class="handlediv" title="Click to toggle"></div>
        <div class="inside">
            <div class="customField wpeditor">
                <p>Address</p>
                <?php
                // insert a wpeditor into a wpalchemy template
                $metabox->the_field('address');
                $settings = array(
                    'textarea_name' => $metabox->get_the_name(),
                    'editor_class'  => 'address_wpeditor',
                    'textarea_rows' => 6
                );
                wp_editor( $metabox->get_the_value(), 'address', $settings );
            ?>
            </div>
        	<?php
        	$metabox->the_field('email');
        	?>
        	<div class="customField mediumtext">
        		<label for="<?php $metabox->the_name(); ?>">Email</label>
        		<input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
        	</div>
            <?php
            $metabox->the_field('phone');
            ?>
            <div class="customField mediumtext">
                <label for="<?php $metabox->the_name(); ?>">Phone</label>
                <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
            </div>

        	<?php
        	$metabox->the_field('map_image');
        	$mainImage = $metabox->get_the_value();
			$mainImageName = $metabox->get_the_name();
        	$mainImageTarget = 'mapImage';
        	$mainImagePreview = $mainImageTarget . '_image';
        	?>
        	<div class="customField imageSelector">
        		<label for="<?php echo $mainImageTarget; ?>">Map image</label>
        		<input type="hidden" id="<?php echo $mainImageTarget; ?>" name="<?php echo $mainImageName; ?>" value="<?php echo $mainImage; ?>">
        		<a href="" class="mediaTrigger_map action">Add/Update image</a>
        		<img id="<?php echo $mainImagePreview; ?>" class="imageThumbnail" alt="" src="<?php echo $mainImage; ?>" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>>
        		<p class="imagepath" <?php if ($mainImage == '' || $mainImage == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $mainImage; ?></p>
        	</div>

            <div class="splitFieldWrapper">
                <?php
                $metabox->the_field('lat');
                ?>
                <div class="customField splitField">
                    <label for="<?php $metabox->the_name(); ?>">Latitude</label>
                    <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
                </div>

                <?php
                $metabox->the_field('long');
                ?>
                <div class="customField splitField">
                    <label for="<?php $metabox->the_name(); ?>">Longitude</label>
                    <input type="text" id="<?php $metabox->the_name(); ?>" name="<?php $metabox->the_name(); ?>" value="<?php $metabox->the_value(); ?>">
                </div>
                <br class="cb">
            </div>
        </div>
		<script type="text/javascript">
		var thisMediaPanel_map;

		jQuery(document).ready(function(){

			thisMediaPanel_map = new MediaPanel({
				"trigger" : jQuery('.mediaTrigger_map'),
				"target" : jQuery('#<?php echo $mainImageTarget; ?>'),
				"preview" : jQuery('#<?php echo $mainImagePreview; ?>'),
				"imagepath" : jQuery('.imagepath')
			});

		});
		</script>

    </div>
