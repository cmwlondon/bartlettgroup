<?php
/*
office map image select
*/
?>
<div class="customField longtext imageField">
<label for="officeMap" class="mediaTrigger"><?php echo _e( 'Map image', 'myplugin_textdomain' ); ?></label>
<input type="text" id="officeMap" name="officeMap" value="<?php echo esc_attr( $value ); ?>" />  
<img id="officeMap_image" alt="" src="<?php echo esc_attr( $value ); ?>">

<script type="text/javascript">
var file_frame,
	trigger = jQuery('.mediaTrigger'),
	target = jQuery('#officeMap'),
	preview = jQuery('#officeMap_image');

jQuery(document).ready(function(){
	console.log('media go');

	// "mca_tray_button" is the ID of my button that opens the Media window
	trigger.live('click', function( event ){

		event.preventDefault();

		if ( file_frame ) {
			file_frame.open();
			return;
		}

		file_frame = wp.media.frames.file_frame = wp.media({
			title: jQuery( this ).data( 'uploader_title' ),
			button: {
				text: jQuery( this ).data( 'uploader_button_text' ),
			},
			multiple: false  
		});

		file_frame.on( 'select', function() {

			attachment = file_frame.state().get('selection').first().toJSON();

			// "mca_features_tray" is the ID of my text field that will receive the image
			// I'm getting the ID rather than the URL:

			target.val(attachment.id);

			// but you could get the URL instead by doing something like this:
			/*
			attachment.sizes:
				full
				large
				medium
				thumbnail
			*/
			target.val(attachment.sizes.full.url);
			jQuery(preview)
			.animate({"opacity" : 0}, 500, function(){
				jQuery(this).load(function(){
					jQuery(this).animate({"opacity" : 1}, 500, function(){
					});
				}).attr({"src" : attachment.sizes.full.url});
			});

			// and you can change "thumbnail" to get other image sizes

		});

		file_frame.open();

	});
});
</script>


</div>
