<?php
?>
<div class="customField imageSelector">
	<label for="officeMap">Map</label>
	<input type="hidden" id="officeMap" name="officeMap" value="<?php echo $values['map']; ?>">
	<a href="" class="mediaTrigger_map action">Add/Update map image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['map']; ?>" <?php if ($values['map'] == '' || $values['map'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['map'] == '' || $values['map'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['map']; ?></p>
</div>

<div class="customField wpeditor">
	<p>Address</p>
	<?php
		$settings = array(
			'textarea_name' => 'officeAddress',
			'editor_class'  => 'address_wpeditor',
			'textarea_rows' => 6
		);
		wp_editor( html_entity_decode( $values['address'] ), 'address', $settings );
	?>
</div>

<div class="customField mediumtext">
	<label for="officeEmail">Email</label>
	<input type="text" id="officeEmail" name="officeEmail" value="<?php echo $values['email']; ?>">
</div>

<div class="customField mediumtext">
	<label for="officePhone">Phone</label>
	<input type="text" id="officePhone" name="officePhone" value="<?php echo $values['phone']; ?>">
</div>

<div class="customField mediumtext">
	<label for="officeFax">Fax</label>
	<input type="text" id="officeFax" name="officeFax" value="<?php echo $values['fax']; ?>">
</div>

<div class="splitFieldWrapper">
    <div class="customField splitField">
        <label for="officeLat">Latitude</label>
        <input type="text" id="officeLat" name="officeLat" value="<?php echo $values['lat']; ?>">
    </div>

    <div class="customField splitField">
        <label for="officeLong">Longitude</label>
        <input type="text" id="officeLong" name="officeLong" value="<?php echo $values['long']; ?>">
    </div>
    <br class="cb">
</div>

<div class="customField mediumtext">
	<label for="officeGmap">Google map link</label>
	<input type="text" id="officeGmap" name="officeGmap" value="<?php echo $values['gmap']; ?>">
</div>

<script type="text/javascript">
var thisMediaPanel_map;

jQuery(document).ready(function(){
	thisMediaPanel_map = new MediaPanel({
		"trigger" : jQuery('.mediaTrigger_map')
	});
});
</script>

