<?php
/*
custom post type: office
relates to: contact page office list

map image
address
email
phone
*/

function bartlett_office_init()
{
    $labels = array(
        'name'                => _x( 'Offices', 'post type general name' ),
        'singular_name'       => _x( 'Office', 'post type singular name' ),
        'add_new'             => _x( 'Add New Office', 'office' ),
        'add_new_item'        => __( 'Add New Office'),
        'edit_item'           => __( 'Edit Office' ),
        'new_item'            => __( 'New Office' ),
        'all_items'           => __( 'All Offices' ),
        'view_item'           => __( 'View Office' ),
        'search_items'        => __( 'Search Offices' ),
        'not_found'           => __( 'No Offices found' ),
        'not_found_in_trash'  => __( 'No Offices found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Offices'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => 'Offices',
        'hierarchical'       => false,
        'menu_position'      => null,
        // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
        'supports'           => array( 'title','thumbnail'),
        'register_meta_box_cb' => 'add_office_metaboxes'
    );

    register_post_type( 'office', $args );


}
add_action( 'init', 'bartlett_office_init' );

// create custom admin page for 'office' custom port type
// -> custum_posts/posts.php

/* add custom columns to the admin office 'browse' page and define column headings */
function office_columns_head($defaults) {
    $date = $defaults['date'];  // save the date column
    unset($defaults['date']);

    $defaults['office_map'] = 'Map';
    $defaults['office_address'] = 'Address';
    $defaults['office_email'] = 'Email';
    $defaults['office_phone'] = 'Phone';
    return $defaults;
}

/* define what is displayed in each column */ 
function office_columns_content($column_name, $post_ID) {
    $metadata = $metadata = get_post_meta($post_ID);

    /*
    $officeDetails = unserialize($metadata['office_details'][0]);

    $map_image = $officeDetails['map_image'];
    $address = $officeDetails['address'];
    $email = $officeDetails['email'];
    $phone = $officeDetails['phone'];
    */

    $map_image = $metadata['o_map'][0];
    $address = $metadata['o_address'][0];
    $email = $metadata['o_email'][0];
    $phone = $metadata['o_phone'][0];

    switch($column_name) {
        case "office_map" : {
            echo "<img class=\"listOfficeMap\" alt=\"\" src=\"$map_image\">";
        } break;
        case "office_address" : {
            echo nl2br(html_entity_decode($address));
        } break;
        case "office_email" : {
            echo $email;
        } break;
        case "office_phone" : {
            echo $phone;
        } break;
    }
}
add_filter('manage_office_posts_columns', 'office_columns_head');
add_action('manage_office_posts_custom_column', 'office_columns_content', 1, 2);

/* define custom metabox */
function add_office_metaboxes() {

    // $screens = array( 'office' );
    // foreach ( $screens as $screen ) {

        add_meta_box(
            'myplugin_of',
            __( 'Office', 'myplugin_textdomain' ),
            'office_meta_box_callback',
            'office',
            'normal'
        );

    // }
}
// add_action( 'add_meta_boxes_office', 'add_office_metaboxes' );

function office_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'office_save_meta_box_data', 'office_meta_box_nonce' );

    $values['map'] = get_post_meta( $post->ID, 'o_map', true );
    $values['address'] = get_post_meta( $post->ID, 'o_address', true );
    $values['email'] = get_post_meta( $post->ID, 'o_email', true );
    $values['phone'] = get_post_meta( $post->ID, 'o_phone', true );
    $values['fax'] = get_post_meta( $post->ID, 'o_fax', true );
    $values['lat'] = get_post_meta( $post->ID, 'o_lat', true );
    $values['long'] = get_post_meta( $post->ID, 'o_long', true );
    $values['gmap'] = get_post_meta( $post->ID, 'o_gmap', true );

    require get_template_directory() . '/custom_posts/offices/metaboxes/office.php';
}

function office_save_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['office_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['office_meta_box_nonce'], 'office_save_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('officeMap', 'o_map'),
        array('officeAddress', 'o_address'),
        array('officeEmail', 'o_email'),
        array('officePhone', 'o_phone'),
        array('officeFax', 'o_fax'),
        array('officeLat', 'o_lat'),
        array('officeLong', 'o_long'),
        array('officeGmap', 'o_gmap'),
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ($field[0] == 'officeAddress') {
                $my_data = htmlentities( $_POST[$field[0]] );
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'office_save_meta_box_data' );
?>