<?php
/*
custom post type: foundation
*/
function foundation_init()
{
    $labels = array(
        'name'                => _x( 'Bartlett foundation Item', 'post type general name' ),
        'singular_name'       => _x( 'Bartlett foundation Item', 'post type singular name' ),
        'add_new'             => _x( 'Add Bartlett foundation Item', 'foundation' ),
        'add_new_item'        => __( 'Add New Bartlett foundation Item'),
        'edit_item'           => __( 'Edit Bartlett foundation Item' ),
        'new_item'            => __( 'New Bartlett foundation Item' ),
        'all_items'           => __( 'All Bartlett foundation Items' ),
        'view_item'           => __( 'View Bartlett foundation Item' ),
        'search_items'        => __( 'Search Bartlett foundation Items' ),
        'not_found'           => __( 'No Bartlett foundation Items found' ),
        'not_found_in_trash'  => __( 'No Bartlett foundation Items found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Bartlett foundation Items'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
        'supports'           => array( 'title', 'thumbnail', 'tags'),
        'taxonomies' => array('post_tag'),
        'register_meta_box_cb' => 'add_foundation_metaboxes'
    );

    register_post_type( 'foundation', $args );
}
add_action( 'init', 'foundation_init' );

function add_foundation_metaboxes(){
    
    add_meta_box(
        'myplugin_foundation_item',
        __( 'Foundation Item details', 'myplugin_textdomain' ),
        'foundation_meta_box_callback',
        'foundation',
        'normal'
    );
}

// foundation_meta_box_callback /custom_posts/foundation/metaboxes/foundation_meta.php
function foundation_meta_box_callback( $post ) {
    $values = [];
    wp_nonce_field( 'foundation_meta_box_data', 'foundation_meta_box_nonce' );

    $values['foundation_image'] = get_post_meta( $post->ID, 'foundation_image', true );
    $values['foundation_title'] = get_post_meta( $post->ID, 'foundation_title', true );
    // $values['foundation_copy'] = get_post_meta( $post->ID, 'foundation_copy', true );

    require get_template_directory() . '/custom_posts/foundation/metaboxes/foundation_meta.php';
}

function foundation_meta_box_data( $post_id ) {
    if ( ! isset( $_POST['foundation_meta_box_nonce'] ) ) { return; }
    if ( ! wp_verify_nonce( $_POST['foundation_meta_box_nonce'], 'foundation_meta_box_data' ) ) { return; }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return; }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) { return; }
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) { return; }
    }

    $fields = array(
        array('foundationImage', 'foundation_image'),
        array('foundationTitle', 'foundation_title'),
        // array('foundationCopy', 'foundation_copy')
    );

    foreach($fields AS $field) {
        if ( isset( $_POST[$field[0]] )) {
            if ($field[1] == 'foundation_copy') {
                $my_data = htmlentities( $_POST[$field[0]] );
                
            } else {
                $my_data = sanitize_text_field( $_POST[$field[0]] );
            }
            update_post_meta( $post_id, $field[1], $my_data );
        }
    }
}
add_action( 'save_post', 'foundation_meta_box_data' );

?>