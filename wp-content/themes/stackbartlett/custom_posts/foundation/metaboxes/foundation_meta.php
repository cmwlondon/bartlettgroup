<div class="customField imageSelector">
	<label>Image</label>
	<input type="hidden" id="foundationImage" name="foundationImage" value="<?php echo $values['foundation_image']; ?>">
	<a href="" class="listPicker action">Add/Update image</a>
	<img class="imageThumbnail" alt="" src="<?php echo $values['foundation_image']; ?>" <?php if ($values['foundation_image'] == '' || $values['foundation_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>>
	<p class="imagepath" <?php if ($values['foundation_image'] == '' || $values['foundation_image'] == '-') : ?>style="opacity:0;"<?php endif; ?>><?php echo $values['foundation_image']; ?></p>
</div>

<div class="customField longtext">
    <label for="foundationTitle">image Caption</label>
    <input type="text" id="foundationTitle" name="foundationTitle" value="<?php echo $values['foundation_title']; ?>">
</div>

<!--
<div class="customField wpeditor">
    <p>Copy</p>
    <?php
        $settings = array(
            'textarea_name' => 'foundationCopy',
            'editor_class'  => 'lc_wpeditor',
            'textarea_rows' => 6
        );
        wp_editor( html_entity_decode($values['foundation_copy']), 'lc', $settings );
    ?>
</div>
-->

<script type="text/javascript">
var listImagePickers = [];

jQuery(document).ready(function(){
    listImagePickers.push( new MediaPanel({
    	"trigger" : jQuery('.listPicker')
    }) );
});
</script>
