                    <?php $mb->the_field('header1'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">header1</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <?php $mb->the_field('header2'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">header1</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <?php $mb->the_field('copy'); ?>
                    <div class="customField">
                        <label for="<?php $mb->the_name(); ?>">Copy</label>
                        <textarea id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" rows="8" cols="80"><?php $mb->the_value(); ?></textarea>
                    </div>

                    <?php $mb->the_field('source'); ?>
                    <div class="customField mediumtext">
                        <label for="<?php $mb->the_name(); ?>">Source</label>
                        <input type="text" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                    </div>

                    <?php $mb->the_field('image'); ?>
                    <div class="customField imageSelector">
                        <label for="<?php $mb->the_name(); ?>">image</label>
                        <input type="hidden" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>">
                        <a href="" class="mediaTrigger action">Add/Update image</a>
                        <img  class="imageThumbnail" alt="" src="<?php $mb->the_value(); ?>" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>>
                        <p class="imagepath" <?php if ($mb->get_the_value() == '' || $mb->get_the_value() == '-') : ?>style="opacity:0;"<?php endif; ?>><?php $mb->the_value(); ?></p>
                    </div>

        <script type="text/javascript">
        var caseStudyImagePickers = [];

        jQuery(document).ready(function(){
            caseStudyImagePickers.push(new MediaPanel({
                "trigger" : jQuery('.mediaTrigger')
            }));
        });
        </script>

