<?php
/*
custom post type: case_study
relates to templates:  business-product2

ID = $post->ID

header1
header2
copy
author
image

tags: insurance, commercial, speciality
*/

/* Register post type: business_product */

function case_study_init()
{
    $labels = array(
        'name'                => _x( 'Case Studies', 'post type general name' ),
        'singular_name'       => _x( 'Case Study', 'post type singular name' ),
        'add_new'             => _x( 'Add New Case Study', 'product' ),
        'add_new_item'        => __( 'Add New Case Study'),
        'edit_item'           => __( 'Edit Case Study' ),
        'new_item'            => __( 'New Case Study' ),
        'all_items'           => __( 'All Case Studies' ),
        'view_item'           => __( 'View Case Study' ),
        'search_items'        => __( 'Search Case Studiess' ),
        'not_found'           => __( 'No Case Studies found' ),
        'not_found_in_trash'  => __( 'No Case Studies found in Trash' ),
        'parent_item_colon'   => '',
        'menu_name'           => 'Case Studies'
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'has_archive'        => 'CaseStudy',
        'hierarchical'       => false,
        'menu_position'      => null,
        // 'menu_icon'          => get_stylesheet_directory_uri() . '/post_types/homebox/img/homebox.png',
        'supports'           => array( 'title', 'thumbnail'),
	   'taxonomies'	=>	array('post_tag')
    );

    register_post_type( 'case_study', $args );
}
add_action( 'init', 'case_study_init' );

/* Styling for the custom post type icon */
/*
add_action( 'admin_head', 'add_product_large_icon' );

function add_product_large_icon()
{
    ?>
    <style type="text/css" media="screen">
        #icon-edit.icon32-posts-campaign_element {background: url(<?php bloginfo('template_url') ?>/post_types/homebox/img/campaign_element32x32.png) no-repeat;}
    </style>
<?php
}
*/
?>