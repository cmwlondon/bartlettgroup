// console.log("ajax.js - %s %s", bartlett_ajax.ajaxUrl, bartlett_ajax.current_blog);
var mailform,
	thisForm,
	thisFormValidator,
	context,
	key,
	post;

jQuery(document).ready(function(){

	mailform = jQuery('article.mailform');

	jQuery('.ajaxEmail').on('click', function(e){
		e.preventDefault();

		context = jQuery(this).attr("data-context"),
		key = jQuery(this).attr("data-key"),
		post = jQuery(this).attr("data-post");

		mailform
		.addClass(context)
		.addClass('open');
	});

	jQuery('.ajaxEmailSubmit').on('click', function(e){
		e.preventDefault();

		var forename = jQuery('#forename').val(),
			surname = jQuery('#surname').val(),
			role = jQuery('#role').val(),
			company = jQuery('#company').val(),
			phone = jQuery('#phone').val(),
			mobile = jQuery('#mobile').val(),
			email1 = jQuery('#email1').val(),
			email2 = jQuery('#email2').val();

		thisForm = jQuery('#mailform');
		// form validation
		// mandatory fields: forename, surname, email1, email2
		// valid email
		// email1 = email2
		// https://jqueryvalidation.org/
		thisFormValidator = thisForm.validate({
			"rules" : {
				"forename" : "required",
				"surname" : "required",
				"email1" : {
					"required" : true,
					"email" : true
				},
				"email2" : {
					"required" : true,
					"email" : true,
					"equalTo" : "#email1"
				}
			}			
		});

		if ( thisForm.valid() ) {
			jQuery.ajax({
		        url: bartlett_ajax.ajaxUrl,
		        data: {
		            "action" : "send_email",
		            "post" : post,
		            "context" : context,
		            "key" : key,
					"forename" : forename,
					"surname" : surname,
					"role" : role,
					"company" : company,
					"phone" : phone,
					"mobile" : mobile,
					"email1" : email1,
					"email2" : email2	            
		        },
		        success:function(data) {
		        	if (data.status == 'ok') {
			        	// console.log(data.data);

			        	// reset form
			        	thisFormValidator.resetForm();
						jQuery('#forename, #surname, #role, #company, #phone, #mobile, #email1, #email2').val('');			        	

						// remove form overlay - slide upwards out of top of browser window
			        	mailform
			        	.removeClass('business')
			        	.removeClass('individual')
			        	.removeClass('open');
		        	} else {
		        		// form fails server-side validation
		        		// display errors in form

		        		// data.error
						/* errors
						'forname' - missing forename
						'surname' - missing surname
						'email1' - missing/invalid email1
						'email2' - missing/invalid email2
						'email1-email2' - email1 != email2
						*/

		        	}

		        },
		        error: function(errorThrown){
		            console.log(errorThrown);
		        }
		    });  
		}
	});

});