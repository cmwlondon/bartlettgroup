var fontsLoaded = false,
	videoLoaded = false,
	showPage;

function showPage() {
	// if ( fontsLoaded && videoLoaded) { jQuery('#page').removeClass('waitForFontsToLoad'); }
	if ( fontsLoaded ) { jQuery('#page').removeClass('waitForFontsToLoad'); }
}

WebFontConfig = {
	"google" : {
		families: ['Poppins:400,700,500,300,600', 'Open Sans']
	},
	"active" : function() {
		fontsLoaded = true;
		showPage();
	}
};

