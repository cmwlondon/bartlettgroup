(function () {
	"use strict";

	/* -----------------------------
	Spinner
	----------------------------- */

	/*
	horizontal carousel
	used:
	home: quotes
	*/

	function Spinner(parameters) {
		this.parameters = parameters;

		this.items = parameters.items;
		this.controls = parameters.controls;
		this.pause = parameters.pause;
		this.animation = parameters.animation;
		this.direction = parameters.direction;

		this.itemCount = 0;
		this.itemIndex = 0;
		this.timer = null;
		this.minHeight = 0;
		this.topPad;

		this.resizeThrottle;

		this.start();
	}
	
	Spinner.prototype = {
		"constructor" : Spinner,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;
	
			this.itemCount = this.items.length;

			this.normaliseHeight();
			this.buildControls();

			// start automatic switch
			this.timer = window.setInterval(function(){
				that.next();
			}, this.pause);

			jQuery(window).on('resize', function(e){
				this.resizeThrottle = window.setTimeout(function(){
					that.normaliseHeight();
				}, 100);
			});

			
		},
		"normaliseHeight" : function () {
			var that = this,
				offset;

			that.minHeight = 0;

			this.items.each(function(i){
				var itemHeight = jQuery(this).find('.singleColumn').height();
				if (itemHeight > that.minHeight) { that.minHeight = itemHeight ;} 
			});

			this.topPad = parseInt(this.items.eq(0).css('padding-top'), 10); 
			offset = this.minHeight + this.topPad + 148;
			this.items.parent().css({ "height" : offset + "px" });
		},
		"buildControls" : function () {
			var that = this,
				nodeIndex = 0,
				newNode,
				rt;

			this.controls.empty();

			do {
				newNode = jQuery("<a></a>")
				.append(jQuery('<span></span>').text((nodeIndex + 1)));

				if (nodeIndex == 0) {
				 	newNode.addClass('active');
				
				}

				newNode.on('click', function(e){
					e.preventDefault();
					that.jumpto(jQuery(this));
				});

				this.controls.append(newNode);				 	

				nodeIndex++;
			} while (nodeIndex < this.itemCount)

			rt = window.setTimeout(function(){
				that.controlsNodes = that.controls.find('a');	
			}, 10);
		},
		"jumpto" : function (controlNode) {
			var that = this,
				newItem = that.controlsNodes.index(controlNode);
				if (newItem != this.itemIndex) {
					window.clearInterval(that.timer);
					that.update(newItem);
				}
		},
		"next" : function () {
			var that = this,
				newIndex = this.indexLoop(this.itemIndex, this.direction);

			this.update(newIndex);
		},
		"previous" : function () {
			var that = this;
		},
		"update" : function (newIndex) {
			var that = this;

			// slide current panel off left hand
			this.items.eq(this.itemIndex).animate({"left" : "-100%"}, this.animation, function(){
				jQuery(this).css({"left" : "100%","z-index":"-1"});
			});

			// slide new panel in from right
			this.items.eq(newIndex).css({"z-index":"2"}).animate({"left" : "0%"}, this.animation, function(){
			});

			// update nav buttons
			this.controlsNodes.eq(this.itemIndex).removeClass('active');
			this.controlsNodes.eq(newIndex).addClass('active');
			
			this.itemIndex = newIndex;
		},
		"indexLoop" : function (n, forwards) {
			var that = this;

			n = (forwards) ? n + 1 : n - 1;

			n = ( n > this.itemCount - 1) ? 0 : n;
			n = ( n < 0) ? this.itemCount - 1 : n;
			return n;
		}
	}

	window.Spinner = Spinner;

	/* -----------------------------
	AwardSpinner
	----------------------------- */

	/*
	horizontal carousel, multiple item move
	used:
	about: awards
	*/

	function AwardSpinner(parameters) {
		this.parameters = parameters;

		/*
		"frame" : jQuery('article.awards'),
		"viewport" : jQuery('section.awardPages2'),
		"items" : jQuery('.awardPages2 div.award'),
		"controls" : jQuery('.awardPages2 > .controls'),
		*/

		this.frame = parameters.frame;
		this.viewport = parameters.viewport;
		this.items = parameters.items;
		this.controls = parameters.controls;
		this.pause = parameters.pause;
		this.animation = parameters.animation;
		this.direction = parameters.direction;

		this.itemWidth = 295; // display width of individual award item
		this.itemCount = 0;
		this.itemIndex = 0;
		this.timer = null;
		this.windowWidth;
		this.itemsInWindow;
		this.catalyst = -1;
		this.offsets;
		this.positions = [];

		// define horizontal positions of awards based on number of awards 1 - 4
		this.offsetList = [
			0,
			[-100,0,100],
			[-50,0,50,100],
			[-33,0,33,66,100],
			[-25,0,25,50,75,100]
		];

		this.widthlist = [0,295,590,885,1180]; // multiples of this.itemWidth 1 - 4

		this.start();
	}
	
	AwardSpinner.prototype = {
		"constructor" : AwardSpinner,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;
	
			this.itemCount = this.items.length;
			this.lower = -200;
			this.upper = 200;

			// this.alignImages();

			this.setStartPositions();

			jQuery(window).on('resize', function(e){
				that.setStartPositions();
			});
		},
		"alignImages" : function () {
			var that = this,
				thisImage,
				imageWidth,
				imageOffset;

			// iterate through all the awards, centering the images horizontally
			this.items.each(function(i){
				thisImage = jQuery(this).find('img');
				imageWidth = parseInt(thisImage.width(),10);
				imageOffset = Math.floor(imageWidth / 2);
				thisImage.css({"margin-left" : "-" + imageOffset + "px"});
			});
		},
		"setStartPositions" : function () {
			var that = this,
				itemIndex = 0,
				offsetIndex = 1,
				thisItem;

			this.windowWidth = parseInt( this.frame.width(), 10 );
			this.itemsInWindow = Math.floor(this.windowWidth / this.itemWidth);

			// has the width of the window changed enough to require changing the number of visible items?
			if (this.catalyst != this.itemsInWindow) {
				// yes, reset carousel
				this.catalyst = this.itemsInWindow;
				that.itemIndex = 0;
				clearInterval(this.timer);
				
				if (this.itemCount > this.itemsInWindow) {
					// too many items to display in one page, set up carousel

					// set offsets and page width based on items per page wodth
					this.offsets = this.offsetList[this.itemsInWindow];
					this.itemWindowWidth = this.itemsInWindow * this.itemWidth;

					// position items and start carousel
					this.positionAwards(true);
				} else {
					// items less than or equal to page width, just position items

					// set offsets and page width based on number of items
					this.offsets = this.offsetList[this.itemCount];
					this.itemWindowWidth = this.itemCount * this.itemWidth;

					// position items but don't start carousel
					this.positionAwards(false);
				}
			}
		},
		"positionAwards" : function (start) {
			var that = this,
				itemIndex = 0,
				offsetIndex = 1,
				thisItem;

			if (!start) {
				this.removeControls();
			}

			this.viewport.animate({"opacity" : 0}, 30, function(){
				
				that.viewport.css({"width" : that.itemWindowWidth + "px"});

				thisItem = that.items.each(function(i){
					jQuery(this).css({"left" : "0%", "display" : "block"});
				});

				do {
					thisItem = that.items.eq(itemIndex);
					that.positions[itemIndex] = that.offsets[offsetIndex];

					// thisItem.css({"left" : this.positions[itemIndex] + "%"});
					thisItem.css({"left" : that.positions[itemIndex] + "%"});

					if (offsetIndex < that.offsets.length -1) {
						offsetIndex++
					}

					itemIndex++;

				} while (itemIndex < that.itemCount)

				jQuery(this).animate({"opacity" : 1},300,function(){
					if ( start ) {
						that.resetControls();
						that.startCarousel();			
					}
				});
			});

		},
		"removeControls" : function () {
			var that = this;

			this.controls.empty();
			this.controlsNodes = false;
		},
		"resetControls" : function () {
			var that = this;

			if (this.controlsNodes) {
				this.controlsNodes.removeClass('active');
				this.controlsNodes.eq(this.itemIndex).addClass('active');
			} else {
				this.buildControls();
			}
		},
		"buildControls" : function () {
			var that = this,
				nodeIndex = 0,
				newNode,
				rt;

			this.removeControls();

			do {
				newNode = jQuery("<a></a>")
				.append(jQuery('<span></span>').text((nodeIndex + 1)));

				if (nodeIndex == 0) {
				 	newNode.addClass('active');
				
				}

				newNode.on('click', function(e){
					e.preventDefault();
					that.jumpto(jQuery(this));
				});

				this.controls.append(newNode);				 	

				nodeIndex++;
			} while (nodeIndex < this.itemCount)

			rt = window.setTimeout(function(){
				that.controlsNodes = that.controls.find('a');	
				that.controlsNodes.eq(that.itemIndex).addClass('active');
			}, 10);


		},
		"startCarousel" : function () {
			var that = this;

			// start automatic switch
				
			clearInterval(this.timer);
			this.timer = window.setInterval(function(){
				that.next();
			}, this.pause);
		},
		"jumpto" : function (node) {
			var that = this;
		},
		"next" : function () {
			var that = this;
			this.doLeft();
		},
		"doLeft" : function () {
			// move items to left (-)
			// this.itemIndex++

			var that = this,
				thisItem,
				itemIndex = 0,
				workingIndex = this.itemIndex,
				offset,
				offsetIndex = 0,
				p0 = [],
				p1 = [],
				map,
				trail;

			do {
				workingIndex = this.itemIndex + itemIndex;
				if (workingIndex >= this.itemCount) {
					workingIndex = workingIndex - this.itemCount;
				}

				p0.push(itemIndex);
				p1.push(workingIndex);

				itemIndex++;
			} while (itemIndex < this.itemCount)

			itemIndex = 0;
			do {
				map = p1[itemIndex];

				thisItem = this.items.eq(map);

				offset = (offsetIndex < this.offsets.length) ? this.offsets[offsetIndex] : this.upper;
				// thisItem.css({"left" : offset + "%"});
				if (itemIndex == 0) {
					thisItem.animate({"left" : offset + "%"}, this.animation, function(){
						jQuery(this)
						.css({"display" : "none", "left" : "100%"})
					});
				} else {
					thisItem
					.css({"display" : "block"})
					.animate({"left" : offset + "%"}, this.animation, function(){
					});
				}
				offsetIndex++;
				itemIndex++;
			} while (itemIndex < this.itemCount)

			this.controlsNodes.eq(this.itemIndex).removeClass('active');

			this.itemIndex++;
			if (this.itemIndex == this.itemCount) {
				this.itemIndex = 0;
			}

			this.controlsNodes.eq(this.itemIndex).addClass('active');
			
		},
		"doRight" : function () {
			// move items to right (+)
			// this.itemIndex--

			var that = this;

		}
	}

	window.AwardSpinner = AwardSpinner;

	/* -----------------------------
	FoundationSpinner
	----------------------------- */

	/*
	foundation horizontal carousel
	update images and captions pnel at same time
	*/

	function FoundationSpinner(parameters) {
		this.parameters = parameters;

		this.images = parameters.images;
		this.controls = parameters.controls;
		this.pause = parameters.pause;
		this.animation = parameters.animation;
		this.direction = parameters.direction;

		this.start();
	}
	
	FoundationSpinner.prototype = {
		"constructor" : FoundationSpinner,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;

			this.timer;
			this.itemIndex = 0;
			this.itemCount = this.images.length;

			if ( this.itemCount > 1 ) {
				this.buildControls();

				// start automatic switch
				this.timer = window.setInterval(function(){
					that.next();
				}, this.pause);
			}
		},	
		"buildControls" : function () {
			var that = this,
				newControl,
				index = 0,
				rt;

			// build controls
			do {
				newControl = jQuery("<a></a>");
				newControl.append(jQuery('<span></span>').text(index + 1));
				if (index == 0) {
					newControl.addClass('active');
				}
				this.controls.append(newControl);

				index++;
			} while( index < this.itemCount)

			rt = window.setTimeout(function(){
				that.controlsNodes = that.controls.find('a');	
				// that.controlsNodes.eq(that.itemIndex).addClass('active');
			}, 10);
		},	
		"next" : function () {
			var that = this,
				newIndex = this.indexLoop(this.itemIndex, this.direction);

			this.update(newIndex);
		},
		"indexLoop" : function (n, forwards) {
			var that = this;

			n = (forwards) ? n + 1 : n - 1;

			n = ( n > this.itemCount - 1) ? 0 : n;
			n = ( n < 0) ? this.itemCount - 1 : n;
			return n;
		},
		"update" : function(newIndex) {
			var that = this;

			// slide current panel off left hand
			this.images.eq(this.itemIndex).animate({"left" : "-100%"}, this.animation, function(){
				jQuery(this).css({"left" : "100%","z-index":"-1"});
			});

			// slide new panel in from right
			this.images.eq(newIndex).css({"z-index":"2"}).animate({"left" : "0%"}, this.animation, function(){
			});

			// update nav buttons
			that.controlsNodes.eq(this.itemIndex).removeClass('active');
			that.controlsNodes.eq(newIndex).addClass('active');
			
			this.itemIndex = newIndex;
		}
	}

	window.FoundationSpinner = FoundationSpinner;

	/* -----------------------------
	OfficeBoxSet
	----------------------------- */

	/*
	office expand/hide behaviour
	used:
	contactus
	*/

	function OfficeBoxSet(parameters) {
		this.parameters = parameters;

		this.boxNodes = parameters.boxNodes;

		this.boxes = [];
		this.boxcount = 0;
		this.currentbox = 999;

		this.start();
	}
	
	OfficeBoxSet.prototype = {
		"constructor" : OfficeBoxSet,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;
	
			this.boxcount = this.boxNodes.length;

			this.boxNodes.each(function(i){
				that.boxes.push(new OfficeBoxItem({
					"boxNode" : jQuery(this),
					"index" : i,
					"set" : that
				}));
			});
		},
		"setOpen" : function (index) {
			var that = this;

			this.currentbox = index;
		},
		"closeOpen" : function () {
			var that = this;

			this.boxes[this.currentbox].close();
		},
		"updateHeights" : function (index) {
			var that = this;
		}
	}

	window.OfficeBoxSet = OfficeBoxSet;

	/* -----------------------------
	OfficeBoxItem
	----------------------------- */

	/*
	office expand/hide behaviour
	used:
	contactus
	*/

	function OfficeBoxItem(parameters) {
		this.parameters = parameters;

		this.boxNode = parameters.boxNode;
		this.index = parameters.index;
		this.set = parameters.set; // reference to OfficeBoxSet

		this.header;
		this.window;
		this.info;
		this.height;

		this.isOpen = false;

		this.start();
	}
	
	OfficeBoxItem.prototype = {
		"constructor" : OfficeBoxItem,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;
	
			this.header = this.boxNode.find('header').eq(0);
			this.window = this.boxNode.find('.window').eq(0);
			this.info = this.window.find('.info').eq(0);
			// this.height = this.info.height() + 20;

			this.header.on('click', function(event) {
				event.preventDefault();

				// chekc to see if another OfficeBoxItem is open, if so, close it
				if (that.set.currentbox != 999 && !that.isOpen) {
					that.set.closeOpen();
				}

				if (that.isOpen) {
					// this box is open, close it
					that.close();
				} else {
					that.open();
				}
			});

			// detect offices which are open on page load
			if (this.boxNode.hasClass('open')) {
				this.open();
			}	
		},
		"open" : function () {
			var that = this;

			this.addMap();

			this.isOpen = true;
			this.set.setOpen(this.index);
			this.boxNode.addClass('open');
			// this.window.animate({"height" : this.height + "px"}, 300, function() { });
		},
		"close" : function () {
			var that = this;

			this.isOpen = false;
			this.boxNode.removeClass('open');
			// this.window.animate({"height" : "0px"}, 300, function() { });
		},
		"updateHeight" : function (index) {
			var that = this;
		},
		"addMap" : function () {
			var that = this,
				officePlace,
				office,
				myMapOptions,
				map;

			this.height = this.info.height() + 20;

			// bind google maps into 
			// <div class="map"><img alt="" src="<?php echo $map_image; ?>"></div>
			this.map = this.info.find('.map').eq(0);
			this.mapWidth = 0;
			
		    jQuery(window).on('resize', function(e){

		    	var workingWidth = that.map.width()	
		    	if ( that.mapWidth !== workingWidth ) {
		    		that.mapWidth = workingWidth;
		    		google.maps.event.trigger(map, 'resize');
		    	}
		    });

			if (!this.map.hasClass('googleized')) {
				this.map.addClass('googleized');	

				officePlace = {
					"lat" : this.map.attr('data-latitude'),
					"long" : this.map.attr('data-longitude'),
				}

				office = new google.maps.LatLng(officePlace.lat, officePlace.long);

			    myMapOptions = {
			        zoom: 17,
			        center: office,
			        disableDefaultUI: true,
			        mapTypeId: google.maps.MapTypeId.ROADMAP
			    };
				// document.getElementById("map164")
			    map = new google.maps.Map(this.map.get(0), myMapOptions);		

				var icon = {
				    url: "../wp-content/themes/stackbartlett/bartlett_mapmarker.png", // url
				    scaledSize: new google.maps.Size(50, 50), // scaled size
				    //origin: new google.maps.Point(0,0), // origin
				    //anchor: new google.maps.Point(0, 0) // anchor
				};

			    // show map once all tiles have loaded
			    map.addListener('tilesloaded', function() {
				    var marker = new google.maps.Marker({
				        position: office,
				        map: map,
				        icon: icon, 
				    });
				    map.setCenter(office);
			    	that.map.animate({"opacity" : 1}, 300, function(){});
			    });

			    map.addListener('resize', function() {
			    	var pause = setTimeout(function(){
			    		map.setCenter(office);	
			    		clearTimeout(pause);
			    	}, 100);
			    });

			}
		}
	}

	window.OfficeBoxSet = OfficeBoxSet;

	/* -----------------------------
	TellMeMoreBox
	----------------------------- */

	/*
	product info expand/hide behaviour
	used:
	individual product lvl1
	*/

	/*
	<div class="accordion open">
		<a href="" class="opener">tell me more</a>
		<div class="frame">
			<div class="window">
				<p>extra copy</p>
				<h4>H3 subhead</h4>
				<ul>
					<li>alpha</li>
					<li>beta</li>
					<li>gamma</li>
					<li>delta</li>
				</ul>
				<a href="" class="misc download">Download our motor insurance brochure (3 Mb PDF)</a>
				<a href="" class="misc caret">&gt; regulatory information</a>
			</div>
		</div>
		<a href="" class="closer">close</a>
	</div>
	*/

	function TellMeMoreBox(parameters) {
		this.parameters = parameters;

		this.boxNode = parameters.boxNode;
		this.index = parameters.index;

		this.opener;
		this.closer;
		this.window;
		this.info;
		this.height;

		this.isOpen = false;

		this.start();
	}
	
	TellMeMoreBox.prototype = {
		"constructor" : TellMeMoreBox,
		"template" : function () { var that = this; },
		
		"start" : function () {
			var that = this;

			this.opener = this.boxNode.find('.opener').eq(0);
			this.closer = this.boxNode.find('.closer').eq(0);

			this.window = this.boxNode.find('.window').eq(0);
			this.info = this.window.find('.info').eq(0);
			// this.height = this.info.height() + 20;

			this.opener.on('click', function(event) {
				event.preventDefault();
				that.open();
			});

			this.closer.on('click', function(event) {
				event.preventDefault();
				that.close();
			});
		},
		"open" : function () {
			var that = this;

			this.height = this.info.height() + 20;

			this.isOpen = true;
			this.boxNode.addClass('open');
			// this.window.animate({"height" : this.height + "px"}, 300, function() { });
		},
		"close" : function () {
			var that = this;

			this.isOpen = false;
			this.boxNode.removeClass('open');
			// this.window.animate({"height" : "0px"}, 300, function() { });
		},
		"updateHeight" : function (index) {
			var that = this;
		}
	}

	window.TellMeMoreBox = TellMeMoreBox;

	/* -----------------------------
	FlexBoxFix
	----------------------------- */

	var FlexBoxFix = function(parameters) {
		this.parameters = parameters;

		this.container = parameters.container;
		this.switchoff = parameters.switchoff;

		this.items;
		this.maxHeight = 0;
		this.windowThrottle;

		this.init();
	};

	FlexBoxFix.prototype = {
		"constructor" : FlexBoxFix,
		"template" : function () {var that = this;},
		"init" : function () {
			var that = this;

			this.prepareItems();

			var pause = window.setTimeout(function(){
				that.normalizeItems();

				jQuery(window).on('resize', function(){

					that.windowThrottle = window.setTimeout(function(){
						that.normalizeItems();
					}, 100);
				});
			}, 30);

		},
		"prepareItems" : function () {
			var that = this,
				itemContent,
				newGetter,
				newSetter;

			this.items = this.container.find('.flexboxItem');
			this.items.each(function(i){
				itemContent = jQuery(this).contents().clone();
				jQuery(this).empty();
				newGetter = jQuery('<div></div>').addClass('getter');
				newSetter = jQuery('<div></div>').addClass('setter');
				newGetter.append(itemContent);
				newSetter.append(newGetter);
				jQuery(this).append(newSetter);
			});
			
		},
		"normalizeItems" : function () {
			var that = this,
				itemHeight,
				windowWidth = parseInt(jQuery(window).width(), 10),
				set;

			that.maxHeight = 0;
			if ( windowWidth > this.switchoff) {
				this.items.each(function(i){
					itemHeight = parseInt(jQuery(this).find('.getter').height(), 10) + 64;
					if (that.maxHeight < itemHeight) {
						that.maxHeight = itemHeight;
					}
				});
				set = that.maxHeight + 'px';
			} else {
				set = 'auto';
			}
			this.items.find('.setter').css({"height" : set});
		}
	};

	window.FlexBoxFix = FlexBoxFix;

	/**
	 * Copyright (c) 2007-2015 Ariel Flesler - aflesler ○ gmail • com | http://flesler.blogspot.com
	 * Licensed under MIT
	 * @author Ariel Flesler
	 * @version 2.1.3
	 */
	;(function(f){"use strict";"function"===typeof define&&define.amd?define(["jquery"],f):"undefined"!==typeof module&&module.exports?module.exports=f(require("jquery")):f(jQuery)})(function($){"use strict";function n(a){return!a.nodeName||-1!==$.inArray(a.nodeName.toLowerCase(),["iframe","#document","html","body"])}function h(a){return $.isFunction(a)||$.isPlainObject(a)?a:{top:a,left:a}}var p=$.scrollTo=function(a,d,b){return $(window).scrollTo(a,d,b)};p.defaults={axis:"xy",duration:0,limit:!0};$.fn.scrollTo=function(a,d,b){"object"=== typeof d&&(b=d,d=0);"function"===typeof b&&(b={onAfter:b});"max"===a&&(a=9E9);b=$.extend({},p.defaults,b);d=d||b.duration;var u=b.queue&&1<b.axis.length;u&&(d/=2);b.offset=h(b.offset);b.over=h(b.over);return this.each(function(){function k(a){var k=$.extend({},b,{queue:!0,duration:d,complete:a&&function(){a.call(q,e,b)}});r.animate(f,k)}if(null!==a){var l=n(this),q=l?this.contentWindow||window:this,r=$(q),e=a,f={},t;switch(typeof e){case "number":case "string":if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(e)){e= h(e);break}e=l?$(e):$(e,q);case "object":if(e.length===0)return;if(e.is||e.style)t=(e=$(e)).offset()}var v=$.isFunction(b.offset)&&b.offset(q,e)||b.offset;$.each(b.axis.split(""),function(a,c){var d="x"===c?"Left":"Top",m=d.toLowerCase(),g="scroll"+d,h=r[g](),n=p.max(q,c);t?(f[g]=t[m]+(l?0:h-r.offset()[m]),b.margin&&(f[g]-=parseInt(e.css("margin"+d),10)||0,f[g]-=parseInt(e.css("border"+d+"Width"),10)||0),f[g]+=v[m]||0,b.over[m]&&(f[g]+=e["x"===c?"width":"height"]()*b.over[m])):(d=e[m],f[g]=d.slice&& "%"===d.slice(-1)?parseFloat(d)/100*n:d);b.limit&&/^\d+$/.test(f[g])&&(f[g]=0>=f[g]?0:Math.min(f[g],n));!a&&1<b.axis.length&&(h===f[g]?f={}:u&&(k(b.onAfterFirst),f={}))});k(b.onAfter)}})};p.max=function(a,d){var b="x"===d?"Width":"Height",h="scroll"+b;if(!n(a))return a[h]-$(a)[b.toLowerCase()]();var b="client"+b,k=a.ownerDocument||a.document,l=k.documentElement,k=k.body;return Math.max(l[h],k[h])-Math.min(l[b],k[b])};$.Tween.propHooks.scrollLeft=$.Tween.propHooks.scrollTop={get:function(a){return $(a.elem)[a.prop]()}, set:function(a){var d=this.get(a);if(a.options.interrupt&&a._last&&a._last!==d)return $(a.elem).stop();var b=Math.round(a.now);d!==b&&($(a.elem)[a.prop](b),a._last=this.get(a))}};return p});

})();
