
var pageheader,
	mobileIsOpen = false,
	supportsCSSTransitions,
	supportsFlexbox,
	ishighdpi,
	dpi,
	dpiSelectItems,
	inlinedpiSelectItems,
	dualdpiSelectItems,
	flexBoxFixers = [],
	videoDOM,
	officeBoxes,
	accordionBoxes = [],
	awardSpinner,
	foundationSpinner,
	map,
	maps,
	windowSize,
	docSize,
	aspectRatio,
	videoAspectRatio = 16 / 9,
	openMenu = -1,
	submenuLinks,
	menutimers = [],
	isIOS;

/*
need to wait till google maps library has loaded before adding the accordion behaviour for offices,
and to display google map element for pre-opened office
*/
/* -------------------------- */
function initMap(){
	if (jQuery('article.offices').length > 0) {
		officeBoxes = new OfficeBoxSet({
			"boxNodes" : jQuery('.officeItem')
		});
	}
}
/* -------------------------- */

function scaleVideo() {
	var videoNode = jQuery('#homevideo'),
		windowSize = {
			"width" : jQuery(window).width(),
			"height" : jQuery(window).height()
		},
		aspectRatio = windowSize.width / windowSize.height,
		offset,
		centre;

	// screen aspect ratio > 16/9
	if (aspectRatio >= videoAspectRatio ) {
		// >= 16/9
		// scale video to fit width, clip height and top align video
		offset = '0';
		centre = '0%';
	} else {
		// < 16/9
		// scale video to fit height, clip width and centre video
		offset = (windowSize.height * 8) / 9;
		offset = '-' + offset;
		centre = '50%';
	}
	videoNode.css({"margin-left" : offset + "px", "left" : centre});

}

Modernizr.addTest('isios', function() {
    return navigator.userAgent.match(/(iPad|iPhone|iPod)/g);
});

jQuery( document ).ready( function() {

	// creates dummy function for devices which don't support console.log natively
	if (!window.console) window.console = {};
	if (!window.console.log) window.console.log = function () { };

	supportsCSSTransitions = jQuery('html').hasClass('csstransitions');
	supportsFlexbox = jQuery('html').hasClass('flexbox');
	supportsTouch = jQuery('html').hasClass('touch');

	// supportsTouch = true;

	isIOS = Modernizr.isios;
	// id device supports devicePixelRatio check for retina display, otherwise assume standard res (IE 9/10) 
	if (typeof window.devicePixelRatio != 'undefined' ) {
		ishighdpi = ( window.devicePixelRatio > 1);	
		dpi = window.devicePixelRatio;
	} else {
		ishighdpi = false;
		dpi = 1;
	}

	windowSize = {
		"width" : jQuery(window).width(),
		"height" : jQuery(window).height()
	};
	aspectRatio = windowSize.width / windowSize.height,
	docSize = {
		"width" : jQuery('body').width(),
		"height" : jQuery('body').height()
	};

	// jQuery('.meettheteam .bg img').css({"height" : Math.floor(docSize.height / 2) + "px", "width" : "auto"})
	// ishighdpi = true;

	/*
	iPad 2: dpr:2 1024 x 768
	iPhone 6 plus: dpr:3 736 X 414
	*/

	// test for iOS devices - relevant to background image attachment/dedvice pixel ratio
	if ( isIOS ) { jQuery('html').addClass('ios'); } else { jQuery('html').addClass('not-ios'); } 

	// TEST mode - simulate iOS
	// jQuery('html').addClass('ios');

	jQuery('html').addClass('dpr' + dpi);

	submenuLinks = jQuery('ul#menu-main > li.menu-item-has-children');

	// navigation desktop/mobile	
	pageheader = jQuery('.pageHeader');
	submenuLinks.on('mouseenter', function(){
		var thisMenu = submenuLinks.index(jQuery(this));

		if ( openMenu != -1) {
			window.clearTimeout(menutimers[openMenu]);
			submenuLinks.eq(openMenu).removeClass('open');
			openMenu = -1;
		}		
		openMenu = thisMenu;
		submenuLinks.eq(openMenu).addClass('open');
	});

	submenuLinks.on('mouseleave', function(){
		if ( openMenu != -1) {
			menutimers[openMenu] = window.setTimeout(function(){
				submenuLinks.eq(openMenu).removeClass('open');
				openMenu = -1;
			}, 750);
		}		
	});

	jQuery('#hamburgermenu').on ('click', function(e){
		e.preventDefault();
		if ( !mobileIsOpen ) {
			pageheader.addClass('mobileOpen');
			mobileIsOpen = true;
		}
	});
	jQuery('.closeMenu').on ('click', function(e){
		e.preventDefault();
		if ( mobileIsOpen ) {
			pageheader.removeClass('mobileOpen');
			mobileIsOpen = false;
		}
	});

	jQuery(window).on('resize', function(e){
		var rw1Throttle = window.setTimeout(function(){

			// switch between mobile and desktop nav
			var windowWidth = jQuery(window).width();
			if ( mobileIsOpen && windowWidth > 880 ) {
				pageheader.removeClass('mobileOpen');
				mobileIsOpen = false;
			}
		},100);
	});

	// home page video element
	videoDOM = document.getElementById('homevideo');
	if (videoDOM != null) {

		videoDOM.addEventListener("playing", function() {
			videoLoaded = true;
			showPage();
		}, true);

		jQuery('.homeOverlay').on('click', function(e){
			videoDOM.play();
		});

		// loadeddata
		// loadedmetadata
		// canplay
	} else {
		videoLoaded = true;
	}

	// home page video - centre video if aspect ratio less than 16/9
	if (jQuery('.video').length > 0) {
		scaleVideo();

		jQuery(window).on('resize', function(e){
			var rw2Throttle = window.setTimeout(function(){
				scaleVideo();
			}, 75);
		});
	}

	// home page quote carousel element
	if (jQuery('.quotes').length > 0) {

		// 16 second interval for product page quotes, 8 second for home page quotes 
		var interval = (jQuery('.productQuotes').length > 0) ? 16000 : 8000;

		thisSpinner = new Spinner({
			"items" : jQuery('.quotes > article'),
			"controls" : jQuery('.quotes > .controls'),
			"pause" : interval,
			"animation" : 500,
			"direction" : true // true = forward, false = backwards
		});
	}

	// about page senior tem bio overlays
	if (jQuery('.seniorteam').length > 0 ) {
		var openPerson = -1,
		persons = jQuery('.person');

		jQuery('.person .trigger, .person .marker, .person .clipper img').on('click',function(e){
			e.preventDefault();
			var person = jQuery(this).parents('.person').eq(0),
				personIndex = persons.index(person);

			if (openPerson == personIndex) {
				person.removeClass('open');
				openPerson = -1;
			} else {
				persons.removeClass('open');
				person.addClass('open');
				openPerson = personIndex;
			}
		});
	}
	
	// about page foundation element
	if (jQuery('.foundation').length > 0 ) {

		foundationSpinner = new FoundationSpinner({
			"images" : jQuery('.images .frame .item'),
			"controls" : jQuery('.images .controls'),
			"pause" : 5000,
			"animation" : 500,
			"direction" : true // l-r
		});

	}

	// about page award carousel element
	if (jQuery('.awardPages2').length > 0 ) {
		awardSpinner = new AwardSpinner({
			"frame" : jQuery('article.awards .frame'),
			"viewport" : jQuery('section.awardPages2'),
			"items" : jQuery('.awardPages2 div.award'),
			"controls" : jQuery('.awardPages2 > .controls'),
			"pause" : 3000,
			"animation" : 500,
			"direction" : true // true = forward, false = backwards
		});
	}


	// open/close office boxes in contact page
	// if (jQuery('article.offices').length > 0) {
		/* refer to initMap() above
		officeBoxes = new OfficeBoxSet({
			"boxNodes" : jQuery('.officeItem')
		});
		*/
	// }

	if ( jQuery('.accordion').length > 0 ) {
		jQuery('.accordion').each(function(i){
			accordionBoxes.push(new TellMeMoreBox({
				"boxNode" : jQuery(this),
				"index" : i
			}));
		});
	}

	// flexbox items for IE */
	if ( !supportsFlexbox && jQuery('.flexboxContainer').length > 0 ) {
		flexBoxFixers.push( new FlexBoxFix({
			"container" : jQuery('.flexboxContainer'),
			"switchoff" : 768
		}));
	}

	/* home page 'down to content' button */
	if ( jQuery('#contentAnchor').length > 0 ) {
		jQuery('.down').on('click',function(e){
			e.preventDefault();
			jQuery(window).scrollTo(jQuery('#contentAnchor'), 500);
		});
		
	}

	/* home page 'tell me more' button */
	if ( jQuery('#contentCATUAnchor').length > 0 ) {
		jQuery('.catuTrigger').on('click',function(e){
			e.preventDefault();
			jQuery(window).scrollTo(jQuery('#contentCATUAnchor'), 500);
		});
		
	}

	/*
	select high res images for retina displays:
	device pixel ratio 2 (ipad air / imac retina)
	or 3 (iphone 6)
	*/
	var imageSource = ( ishighdpi ) ? 'data-retina' : 'data-normal';

	/* retina BACKGROUND images -> banner images on most pages + home/product page divider images */
	// <article class="module dpiSelect" data-normal="{normal_image}" data-retina="{retina_image}">
	dpiSelectItems = jQuery('.dpiSelect');
	dpiSelectItems.each(function(i){
		var bgImage = jQuery(this).attr(imageSource);
		if ( ishighdpi ) {
			jQuery(this).addClass('hidpi')
		}
		jQuery(this).css({"background-image" : "url('" + bgImage + "')"});
	});

	/* retina INLINE images */
	inlinedpiSelectItems = jQuery('.inlinedpiSelect');
	inlinedpiSelectItems.each(function(i){
		var bgImage = jQuery(this).attr(imageSource);
		if ( ishighdpi ) {
			jQuery(this).addClass('hidpi')
		}
		jQuery(this).find('img').attr({ "src" : bgImage });
	});

	/* inline and background images: about, contact and product header images, product divider images, home 'meet the team' and about history */
	dualdpiSelectItems = jQuery('.dualdpiSelect');
	dualdpiSelectItems.each(function(i){
		var image = jQuery(this).attr(imageSource);

		if ( ishighdpi ) {
			jQuery(this).addClass('hidpi')
		}

		jQuery(this).css({"background-image" : "url('" + image + "')"});
		jQuery(this).find('img').eq(0).attr({ "src" : image });
	});

});
