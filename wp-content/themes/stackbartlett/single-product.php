<?php
/*
custom post type: 'product'
single item template
single-product.php

@package WordPress
@subpackage stackbartlett
*/

$defaultFooter = true;

// get page header image
$thumbnailID = get_post_thumbnail_id($post->ID);
$imageData = wp_get_attachment_image_src( $thumbnailID, 'full');
$headerImage = $imageData[0];

$metadata = get_post_meta($post->ID);

$posttags = get_the_tags();
$taglist = [];
if ($posttags) {
    foreach($posttags AS $tag) {
        $taglist[] = $tag->name;
    }
}

// set background colour for case studies items
if (in_array('individual', $taglist)) {
	// individual (blue)
	$colourSchemeColor = 'individual02';
	$colourSchemebg = 'bgindividual02';
	$largeColor = 'iBlue';
} else {
	// business (purple)
	$colourSchemeColor = 'business01';
	$colourSchemebg = 'bgbusiness01';
	$largeColor = 'bPurple';
}

$bannerTitle = $metadata['banner_text'][0];
$bannerNormal = $metadata['banner_normal'][0];
$bannerRetina = $metadata['banner_retina'][0];
$bannerAlign = $metadata['banner_align'][0];

$dividerNormal = $metadata['divider_normal'][0];
$dividerRetina = $metadata['divider_retina'][0];
$dividerAlign = $metadata['divider_align'][0];

set_query_var( 'relatedTitle', $metadata['rld_heading'][0] );
set_query_var( 'taglist', $taglist );
set_query_var( 'colourScheme', $colourSchemebg );
set_query_var( 'metadata', $metadata );
set_query_var( 'postid', $post->ID );

set_query_var( 'postid', $post->ID );
set_query_var( 'quote_context', 'product' );
set_query_var( 'metakey', 'quotes_order' );
?>

<?php get_header();?>
	</div>

<div id="content" class="site-content <?php echo (in_array('individual', $taglist)) ? 'individual' : 'business'; ?> genericproduct">
		<div class="fsSubPage">
			<div class="headerBackground"></div>
			<div class="overlay">
				<div class="table"><h1><?php echo $bannerTitle; ?></h1></div>
			</div>
			<div class="clipper2 inlinedpiSelect <?php echo $bannerAlign; ?>" data-normal="<?php echo $bannerNormal; ?>" data-retina="<?php echo $bannerRetina; ?>"><div><img alt="<?php echo $bannerTitle; ?>"></div></div>
		<!-- div class="down"><span>scroll down for page content</span></div> -->
		<!-- <div class="clipper dpiSelect <?php echo $bannerAlign; ?>" data-normal="<?php echo $bannerNormal; ?>" data-retina="<?php echo $bannerRetina; ?>" style="xbackground-image:url('<?php echo $bannerNormal; ?>');"></div> -->
		</div>

<?php if ($metadata['show_iip'][0] != 'showiip') : ?>
<!-- product block 1: intro START -->		
		<?php if ($metadata['intro_copy'][0] != '') : ?>
		<article class="module intro" id="contentAnchor">
			<div class="singleColumn">
				<hr class="<?php echo $colourSchemebg; ?>">
				<div>
					<?php echo html_entity_decode($metadata['intro_copy'][0]); ?>
				</div>
			</div>
		</article>
		<?php endif; ?>
<!-- product block 1: intro END -->		
<?php else : ?>
<!-- product block 1 ALTERNATE: individual insurance products START -->
		<article class="module iip" id="contentAnchor">
			<div class="singleColumn">
				<hr class="<?php echo $colourSchemebg; ?>">
				<div>
					<h2>Our private client insurance broking service covers:</h2>
					<?php echo html_entity_decode($metadata['insurance_products_heading'][0]); ?>
				</div>
			</div>
			<div class="boxWrapper clearfix">
				<div class="box home">
					<h3>High value home insurance</h3>
					<p>(including second homes and homes abroad)</p>
				</div>
				<div class="box valuables">
					<h3>Specialist insurance for art, furniture, antiques and jewellery</h3>
				</div>
				<div class="box car">
					<h3>Car insurance</h3>
				</div>
				<div class="box life">
					<h3>Life Insurance and health insurance</h3>
				</div>
		</article>
<!-- product block 1 ALTERNATE: individual inusrance products END -->
<?php endif; ?>

<?php if ($metadata['extracopy_heading'][0] == '' && $metadata['extracopy_p1'][0] == '' && $metadata['extracopy_left'][0] == '' && $metadata['extracopy_right'][0] == '') : ?>
		<div class="module"><hr class="full"></div>
<?php endif; ?>

<!-- product block 2: extra copy START -->	
		<?php if ($metadata['extracopy_heading'][0] != '' || $metadata['extracopy_p1'][0] != '' || $metadata['extracopy_left'][0] != '' || $metadata['extracopy_right'][0] != '') : ?>
		<article class="module greybg extracopy <?php echo $largeColor; ?>">
			<?php if ($metadata['extracopy_heading'][0] != '' || $metadata['extracopy_p1'][0] != '') : ?>
			<div class="singleColumn">
			<?php if ($metadata['extracopy_heading'][0] != '') : ?>
				<h2><?php echo html_entity_decode($metadata['extracopy_heading'][0]); ?></h2>
			<?php endif; ?>

			<?php if ($metadata['extracopy_p1'][0] != '') : ?>
				<div>
					<?php echo html_entity_decode($metadata['extracopy_p1'][0]); ?>
				</div>
			<?php endif; ?>
			</div>
			<?php endif; ?>

			<?php if ($metadata['extracopy_left'][0] != '' || $metadata['extracopy_right'][0] != '') : ?>
				<?php if ($metadata['extracopy_left'][0] != '' && $metadata['extracopy_right'][0] != '') : ?>
			<div class="doubleColumn clearfix">
				<div class="column">
					<?php echo html_entity_decode($metadata['extracopy_left'][0]); ?>
				</div>
				<div class="column">
					<?php echo html_entity_decode($metadata['extracopy_right'][0]); ?>
				</div>
			</div>
				<?php else : ?>
					<?php if ($metadata['extracopy_left'][0] != '') : ?>
					<div class="singleColumn"><?php echo html_entity_decode($metadata['extracopy_left'][0]); ?></div>
					<?php endif; ?>	
					<?php if ($metadata['extracopy_right'][0] != '') : ?>
					<div class="singleColumn"><?php echo html_entity_decode($metadata['extracopy_right'][0]); ?></div>
					<?php endif; ?>	
				<?php endif; ?>
			<?php endif; ?>
		</article>
		<?php endif; ?>
<!-- product block 2: extra copy END -->		

<!-- product block 2: main copy START -->		
		<?php if ($metadata['maincopy_heading'][0] != '' || $metadata['maincopy_p1'][0] != '' || $metadata['maincopy_left'][0] != '' || $metadata['maincopy_right'][0] != '') : ?>
		<article class="module maincopy <?php echo $largeColor; ?>">
			<div class="singleColumn">
				<h2><?php echo html_entity_decode($metadata['maincopy_heading'][0]); ?></h2>
				<div>
					<?php echo html_entity_decode($metadata['maincopy_p1'][0]); ?>
				</div>
			</div>
			<?php if ($metadata['maincopy_left'][0] != '' && $metadata['maincopy_right'][0] != '') : ?>
			<div class="doubleColumn clearfix">
				<div class="column">
					<?php echo html_entity_decode($metadata['maincopy_left'][0]); ?>
				</div>
				<div class="column">
					<?php echo html_entity_decode($metadata['maincopy_right'][0]); ?>
				</div>
			</div>
			<?php
				else :
					if ($metadata['maincopy_left'][0] != '') : ?>
				<div class="singleColumn"><?php echo html_entity_decode($metadata['maincopy_left'][0]); ?></div>
				<?php
					endif;
					if ($metadata['maincopy_right'][0] != '') :
				?>
				<div class="singleColumn"><?php echo html_entity_decode($metadata['maincopy_right'][0]); ?></div>
				<?php endif; ?>
			?>

			<?php endif; ?>
		</article>
		<?php endif; ?>
<!-- product block 2: main copy END -->		

<!-- divider image -->
		<?php if ( $dividerNormal != '' ) : ?>
		<!-- <div class="module divider dpiSelect <?php echo $dividerAlign; ?>"  data-normal="<?php echo $dividerNormal; ?>" data-retina="<?php echo $dividerRetina; ?>"></div> -->
		<div class="module divider2 dualdpiSelect <?php echo $dividerAlign; ?>"  data-normal="<?php echo $dividerNormal; ?>" data-retina="<?php echo $dividerRetina; ?>"><div class="a"><div class="b"><img></div></div></div>

		<?php endif; ?>

<!-- product block 3: more copy START -->	
		<?php if ($metadata['moreleft_heading'][0] != '' || $metadata['moreleft_copy'][0] != '' || $metadata['moreright_heading'][0] != '' || $metadata['moreright_copy'][0] != '') : ?>
		<article class="module <?php echo $colourSchemebg; ?> morecopy">
			<?php if ( ($metadata['moreleft_heading'][0] != '' || $metadata['moreleft_copy'][0] != '') && ($metadata['moreright_heading'][0] != '' || $metadata['moreright_copy'][0] != '')) : ?>
			<div class="doubleColumn clearfix">
				<div class="column">
					<?php
						$mrheaderClass = ' class="mrOK"';
						if ( $metadata['moreleft_heading'][0] == '') {
							$mlheader = '&nbsp';
							$mlheaderClass = ' class="mr"';
						} else {
							$mlheader = html_entity_decode($metadata['moreleft_heading'][0]);
							$mlheaderClass = ' class="mrOK"';
						}
					?>
					<h3<?php echo $mlheaderClass; ?>><?php echo $mlheader; ?></h3>
					<div><?php echo html_entity_decode($metadata['moreleft_copy'][0]); ?></div>
				</div>
				<div class="column">
					<?php
						$mrheaderClass = ' class="mrOK"';
						if ( $metadata['moreright_heading'][0] == '') {
							$mrheader = '&nbsp';
							$mrheaderClass = ' class="mr"';
						} else {
							$mrheader = html_entity_decode($metadata['moreright_heading'][0]);
							$mrheaderClass = ' class="mrOK"';
						}
					?>
					<h3<?php echo $mrheaderClass; ?>><?php echo $mrheader; ?></h3>
					<div><?php echo html_entity_decode($metadata['moreright_copy'][0]); ?></div>
				</div>
			</div>
			<?php
				else :
				if ($metadata['moreleft_heading'][0] != '' || $metadata['moreleft_copy'][0] != '') :
					$mlheader = html_entity_decode($metadata['moreleft_heading'][0]);
					$mlheaderClass = '';
			?>
			<div class="singleColumn">
				<h3<?php echo $mlheaderClass; ?>><?php echo $mlheader; ?></h3>
				<div><?php echo html_entity_decode($metadata['moreleft_copy'][0]); ?></div>
			</div>
			<?php
				endif;
				if ($metadata['moreright_heading'][0] != '' || $metadata['moreright_copy'][0] != '') :
					$mrheader = html_entity_decode($metadata['moreleft_heading'][0]);
					$mrheaderClass = '';
			?>
			<div class="singleColumn">
				<h3<?php echo $mrheaderClass; ?>><?php echo $mrheader; ?></h3>
				<div><?php echo html_entity_decode($metadata['moreright_copy'][0]); ?></div>
			</div>
			<?php endif; ?>
			
			<?php endif; ?>
		</article>
		<?php endif; ?>
<!-- product block 3: more copy END -->		

<!-- product block 4: the bartlett process START -->
		<?php if ($metadata['process_heading'][0] != '' || $metadata['process_left'][0] != '' || $metadata['process_right'][0] != '') : ?>		
		<article class="module process <?php echo $largeColor; ?> greybg">
			<div class="singleColumn">
				<h2><?php echo html_entity_decode($metadata['process_heading'][0]); ?></h2>
				<hr class="<?php echo $colourSchemebg; ?>">
			</div>
			<div class="doubleColumn clearfix">
				<div class="column">
					<div><?php echo html_entity_decode($metadata['process_left'][0]); ?></div>
				</div>
				<div class="column">
					<div><?php echo html_entity_decode($metadata['process_right'][0]); ?></div>
				</div>
			</div>
		</article>
		<?php endif; ?>
<!-- product block 4: the bartlett process END -->		

<!-- product block 5: steps START -->	
		<?php if ($metadata['stepsleft_heading'][0] != '' && $metadata['steps_left'][0] != '' && $metadata['stepsright_heading'][0] != '' && $metadata['steps_right'][0] != '' ) : ?>		
		<article class="module steps greybg">
			<div class="flexboxContainer">
				<div class="flexboxItem step step1 left whitebg">
					<h3>Step 1:</h3>
					<h4><?php echo html_entity_decode($metadata['stepsleft_heading'][0]); ?></h4>
					<hr class="<?php echo $colourSchemebg; ?>">
					<div><?php echo html_entity_decode($metadata['steps_left'][0]); ?></div>
				</div>
				<div class="flexboxItem step step2 right whitebg">
					<h3>Step 2:</h3>
					<h4><?php echo html_entity_decode($metadata['stepsright_heading'][0]); ?></h4>
					<hr class="<?php echo $colourSchemebg; ?>">
					<div><?php echo html_entity_decode($metadata['steps_right'][0]); ?></div>
				</div>
			</div>
		</article>
		<?php endif; ?>
<!-- product block 5: steps END -->		

<!-- case study START-->
		<?php 
		get_template_part( 'modules/case_studies' );
		?>
<!-- case study START-->

<!-- product find out more downloads/contact START -->
		<?php
		if ( $metadata['dl_brochure'][0] != '' || $metadata['dl_contact'][0] != '' || $metadata['dl_heading'][0] != '' || $metadata['dl_copy'][0]  != '' ) :
			$data = explode(',', $metadata['dl_data'][0]);
		?>
		<article class="module downloads <?php echo $largeColor; ?>">
			<?php if ($metadata['dl_brochure'][0] != '' && $metadata['dl_image'][0] != '') : ?>
			<div class="doubleColumn clearfix">
				<div class="column image">
					<img alt="" src="<?php echo $metadata['dl_image'][0]; ?>">
				</div>
				<div class="column">
					<?php if ($metadata['dl_heading'][0] != '') : ?>
					<h2><?php echo html_entity_decode($metadata['dl_heading'][0]); ?></h2>
					<?php endif; ?>

					<?php if ($metadata['dl_copy'][0] != '') : ?>
					<div><?php echo html_entity_decode($metadata['dl_copy'][0]); ?></div>
					<?php endif; ?>

					<?php if ($metadata['dl_contact'][0] != '') : ?>
					<div>
						<p>
						<?php echo html_entity_decode($metadata['dl_conname'][0]); ?>
						<?php if ($metadata['dl_conemail'][0] != '') : ?>
						<br />
						Email: <a href="mailto:<?php echo $metadata['dl_conemail'][0]; ?>" class="email"><?php echo $metadata['dl_conemail'][0]; ?></a>
						<?php endif; ?>
						</p>
					</div>
					<?php endif; ?>

					<?php if ( $metadata['dl_brochure'][0] != '') : ?>
					<a href="<?php echo $metadata['dl_link'][0]; ?>" target="_blank" class="nuButton"><span class="caret"><span class="a"><?php echo html_entity_decode($metadata['dl_label'][0]); ?> <span class="b">(<?php echo $data[4].' '.$data[1]; ?>)</span></span></span></a>
					<?php endif; ?>
				</div>
			</div>
			<?php else : ?>
			<div class="singleColumn">
					<?php if ($metadata['dl_heading'][0] != '') : ?>
					<h2><?php echo html_entity_decode($metadata['dl_heading'][0]); ?></h2>
					<?php endif; ?>

					<?php if ($metadata['dl_copy'][0] != '') : ?>
					<div><?php echo html_entity_decode($metadata['dl_copy'][0]); ?></div>
					<?php endif; ?>

				<?php if ($metadata['dl_contact'][0] != '') : ?>
				<div>
					<p>
					<?php echo html_entity_decode($metadata['dl_conname'][0]); ?>
						<?php if ($metadata['dl_conemail'][0] != '') : ?>
						<br />
						Email: <a href="mailto:<?php echo $metadata['dl_conemail'][0]; ?>" class="email"><?php echo $metadata['dl_conemail'][0]; ?></a>
						<?php endif; ?>
						</p>
				</div>
				<?php endif; ?>

				<?php if ( $metadata['dl_brochure'][0] != '' && $metadata['dl_link'][0]) : ?>
				<a href="<?php echo $metadata['dl_link'][0]; ?>" target="_blank" class="nuButton"><span class="caret"><span class="a"><?php echo html_entity_decode($metadata['dl_label'][0]); ?> <span class="b">(<?php echo $data[4].' '.$data[1]; ?>)</span></span></span></a>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</article>
		<?php endif; ?>
<!-- product find out more downloads/contact END -->

<!-- quotes START -->	
		<?php if ($metadata['quotes_enabled'][0] == 'yes') : get_template_part( 'modules/quotes' ); endif; ?>
<!-- quotes END -->

<!-- related products list START -->
		<?php
		// parameters: $taglist, $postid
		get_template_part( 'modules/related_products' );
		?>
<!-- related products list END -->

<?php get_footer(); ?>