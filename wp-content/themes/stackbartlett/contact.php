<?php
/*
Template Name: contact

@package WordPress
@subpackage stackbartlett
*/
$defaultFooter = false;
$postid = $post->ID;
$thumbnailID = get_post_thumbnail_id($post->ID);
$imageData = wp_get_attachment_image_src( $thumbnailID, 'full');
$headerImage = $imageData[0];

// get metadata
$metadata = get_post_meta($post->ID);

$bannerTitle = $metadata['contact_banner_heading'][0];
$bannerNormal = $metadata['banner_normal'][0];
$bannerRetina = $metadata['banner_retina'][0];

$locations = $metadata['c_locations'][0];
set_query_var( 'locations', $locations );

/*
$team = array(
	$metadata['c_title'][0],
	$metadata['c_copy'][0],
	$metadata['c_team'][0]
);
set_query_var( 'team', $team );
*/
set_query_var( 'postid', $post->ID );

?>
<?php get_header();?>
	</div>

	<div id="content" class="site-content contactus">
		<div class="fsSubPage">
			<div class="headerBackground"></div>
			<div class="overlay">
				<div class="table"><h1><?php echo $bannerTitle; ?></h1></div>
			</div>
			<div class="clipper2 inlinedpiSelect" data-normal="<?php echo $bannerNormal; ?>" data-retina="<?php echo $bannerRetina; ?>"><div><img alt="<?php echo $bannerTitle; ?>"></div></div>
			<!-- div class="down"><span>scroll down for page content</span></div> -->
			<!-- <div class="clipper dpiSelect" data-normal="<?php echo $bannerNormal; ?>" data-retina="<?php echo $bannerRetina; ?>" style="xbackground-image:url('<?php echo $bannerNormal; ?>');"></div> -->
		</div>

		<!--  see an advisor block at start of content -->
		<?php // get_template_part( 'modules/advisor' ); ?>

		<!-- office list generator -->
		<!-- custom post type: office -->
		<?php get_template_part( 'modules/offices' ); ?>

		<!-- managemement team people:contact -->
		<!-- custom post type: person -->
		<?php // get_template_part( 'modules/people-contact' ); ?>
		

<!-- div#content closed on footer.php after 'see an advisor' section -->

<?php get_footer(); ?>
