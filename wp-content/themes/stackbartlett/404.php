<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	</div>

	<div id="content" class="site-content generic">
		<div class="fullscreen">
			<div class="headerBackground"></div>
		</div>
		<article class="module">
			<div class="singleColumn">
				<h1><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyfifteen' ); ?></h1>
				<p><?php _e( 'It looks like nothing was found at this location.', 'twentyfifteen' ); ?></p>
				<?php // get_search_form(); ?>
			</div>

		</article>


<?php get_footer(); ?>
