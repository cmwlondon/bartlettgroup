<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bartlett');

/** MySQL database username */
define('DB_USER', 'bartlett');

/** MySQL database password */
define('DB_PASSWORD', 're5pP@nd0Ver7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IonuY(Cf>&Sm+fJ8A&LAG&FA0GQBGTJ-^SfZ&gcXR}d^~s09mf)MDAL0yPHhXBG-');
define('SECURE_AUTH_KEY',  'XQ1ySTpMVe6Cm].mt2Bj.</hx#X4prj=9N,s36?d^IqeD#Rcy`(UjD~M`W(ZR@n/');
define('LOGGED_IN_KEY',    ' Q{+&$%}X4~l>K5W&ZVtm`Wz^Sc$lqMqnN(]mhKY*C]3:kkzf/P0=/<JZGmcesM?');
define('NONCE_KEY',        '/#>ggZ34JdzxQjn(UIb{ Bi</ Z4GeCSbNX5Ez;niUX[KM0l1-:er~iM}vD<.|/L');
define('AUTH_SALT',        'rg0}tyn<&go*xj#1XT>cB.*1^_}EY 3RNLiG)(x*g2_~Y{{_$MJCFSLNlxbb~>cH');
define('SECURE_AUTH_SALT', '=LV<CsI27Ch)`}rpIy0`}Q vVSu.ae.RDd~>i;w]6Ez0}v|2w*XvBxV(_<*I6}ac');
define('LOGGED_IN_SALT',   '+GtSZTtCgfO@cI/RfwDMMJv> e-nlxJjb0]5)I;D%-%c_WY_sa/dSE?R-vZ``g}X');
define('NONCE_SALT',       ')C]G~XDJPH/P8c|zoC}`_`A?[pv:jehJ{}/We*c_<j<+X>h4DrsJyTQ8!`oe]oLe');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'local.bartlett');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
